/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Bootstrap testing - gradlew test --rerun-tasks

package org.gint.test

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.junit.Assert.*

import org.gint.plugin.Constants
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class TestGintTask {

    protected final static String GREEN     = '\033[0;32m' // start green
    protected final static String RED       = '\033[0;31m' // start red
    protected final static String NO_COLOR  = '\033[0m'    // always end color change back to no color
    protected final static String RUN_SUCCESSFUL = GREEN + '    <<< RUN SUCCESSFUL' + NO_COLOR
    protected final static String RUN_FAILED     = RED   + '    <<< RUN FAILED'     + NO_COLOR

    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder() // creates/deletes before/after run

    private void createBuildFile(final String content = '') throws IOException {
        File buildFile = testProjectDir.newFile("build.gradle")
        TestUtils.writeFile(buildFile, "plugins { id '${Constants.PLUGIN_ID}' }" + '\n' + content, content != '')
    }

    @Test
    public void testAddTask() throws Exception {

        String content = "gint.taskHelper.add(name: 'z1', description: 'z1 desc', inline: { task -> true })"
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments('z1', '--stacktrace', '-DdbReport=')  // set dbReport to blank for build systems that may have it set globally
                        .build()


        String output = result.getOutput()
        result.println('output: ' + output)

        assertTrue(result.getOutput().contains("z1"))
        assertEquals(SUCCESS, result.task(":z1").getOutcome())

        assert output.contains('[info] Successful base tasks . . . . . . . . . : 1' + RUN_SUCCESSFUL)
        assert output.contains('[info] Successful base tasks . . . . . . . . . : 1')
        assert output.contains('[info] Total base tasks  . . . . . . . . . . . : 1')

        assert output.contains('BUILD SUCCESS')
    }

    @Test
    public void testAddTaskList() throws Exception {

        String content = """
            gint.taskHelper.add('myGroup', [
                [name: 'z1',
                    inline: { task -> true },
                ],
                [name: 'z2',
                    inline: { false },
                ],
                [name: 'z3',
                    inline: { true },
                ],
            ])
        """
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments('--stacktrace', '--continue', '-DdbReport=')
                        .buildAndFail() // NOTE: we expect it to fail

        String output = result.getOutput()
        result.println('output: ' + output)

        assertTrue(result.getOutput().contains("z1"))
        assertTrue(result.getOutput().contains("z2"))
        assertTrue(result.getOutput().contains("z3"))
        assertEquals(SUCCESS, result.task(":z1").getOutcome())
        assertEquals(FAILED,  result.task(":z2").getOutcome())
        assertEquals(SUCCESS, result.task(":z3").getOutcome())

        assert output.contains('[info] Failure summary:')
        assert output.contains('[failed] z2: Expected 0 but got -1')

        assert output.contains('[info] Failed base tasks . . . . . . . . . . . : 1' + RUN_FAILED)
        assert output.contains('[info] Successful base tasks . . . . . . . . . : 2')
        assert output.contains('[info] Total base tasks  . . . . . . . . . . . : 3')

        assert output.contains('BUILD FAILED')
    }

    @Test
    public void testCmdTask() throws Exception {

        // we use echo as something works on powershell, cmd, and linux/osx
        String content = """
                import org.gint.tasks.GintTask
                task cmd1(type: GintTask) {
                    description = 'cmd test'
                    cmd = 'echo xxx'
                }
            """
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments('gintAll', '--stacktrace', '-DdbReport=') // , '--info')
                        .build()

        assertTrue(result.getOutput().contains("cmd1"))
        assertEquals(SUCCESS, result.task(":cmd1").getOutcome())

        String output = result.getOutput()
        result.println('output: ' + output)

        assert output.contains('[info] Successful base tasks . . . . . . . . . : 1' + RUN_SUCCESSFUL)
        assert output.contains('[info] Successful base tasks . . . . . . . . . : 1')
        assert output.contains('[info] Total base tasks  . . . . . . . . . . . : 1')
        assert output.contains('[info] Elapsed run time  . . . . . . . . . . . : ')

        assert output.contains('BUILD SUCCESSFUL')
    }

    // Inline with default to not stop on fail and therefore, tasks end with outcome SUCCESS except for finalizer
    // No stacktrace since no task failures
    @Test
    public void testInlineTask() throws Exception {

        String content = """
            import org.gint.tasks.GintTask

            task inline1(type: GintTask) {
                 closures = [
                     inline: { task -> assert true }
                 ]
            }
            task inline2(type: GintTask) {
                 closures = [
                     inline: { assert false }
                 ]
            }
            task inline3(type: GintTask) {
                 closures = [
                     inline: { task -> throw new Exception('test throwing an exception') }
                 ]
            }
        """
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments('gintAll', '--continue')
                        //.withArguments('gintAll', '--continue', '--stacktrace',  '--info')
                        .buildAndFail()  // NOTE: we expect it to fail

        checkInlineResult(result)
    }

    // Inline with continue
    // With stack trace for failures since we specify --stacktrace and there are task failures
    @Test
    public void testInlineTaskContinue() throws Exception {

        String content = """
                import org.gint.tasks.GintTask

                task inline1(type: GintTask) {
                    closures = [
                        inline: { task -> assert true }
                    ]
                }
                task inline2(type: GintTask) {
                    closures = [
                        inline: { assert false }
                    ]
                }
                task inline3(type: GintTask) {
                    closures = [
                        inline: { task -> throw new Exception('test throwing an exception') }
                    ]
                }
            """
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments('gintAll', '--continue')
                        .buildAndFail()  // NOTE: we expect it to fail

        checkInlineResult(result, true)
    }

    protected void checkInlineResult(BuildResult result, final boolean doContinue = true) {

        String output = result.getOutput()
        result.println('output: ' + output)

        //result.println('outcome: ' + result.task(":inline2").getOutcome())
        //result.println('outcome: ' + result.task(":inline3").getOutcome())

        assertTrue(result.getOutput().contains("inline1"))
        assertEquals(SUCCESS, result.task(":inline1").getOutcome())
        assertEquals(FAILED, result.task(":inline2").getOutcome())
        assert output.contains('Task :inline2 FAILED')

        assert output.contains('Task :inline1' +  System.lineSeparator())
        if (doContinue) {
            assertEquals(FAILED, result.task(":inline3").getOutcome())
            assert output.contains('Task :inline3 FAILED')
        }

        assert output.contains('[info] Failure summary:')
        assert output.contains('[failed] inline2: assert false')
        assert output.contains('[failed] inline3: test throwing an exception')
        assert output.contains('[info] Failed base tasks . . . . . . . . . . . : 2' + RUN_FAILED)
        assert output.contains('[info] Successful base tasks . . . . . . . . . : 1')
        assert output.contains('[info] Total base tasks  . . . . . . . . . . . : 3')

        assert output.contains('BUILD FAILED')
    }
}
