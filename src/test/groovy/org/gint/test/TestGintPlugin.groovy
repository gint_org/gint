/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Bootstrap testing - gradlew test --rerun-tasks

package org.gint.test

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.junit.Assert.*

import org.gint.plugin.Constants
import org.gint.tasks.GintInternalTask
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class TestGintPlugin {

    private void createBuildFile(final String content = '') throws IOException {
        File buildFile = testProjectDir.newFile("build.gradle")
        TestUtils.writeFile(buildFile, "plugins { id '${Constants.PLUGIN_ID}' }" + '\n' + content, content != '')
    }

    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder() // creates/deletes before/after run

    @Test
    public void testDefinedTasks() {

        Project project = ProjectBuilder.builder().build()

        project.pluginManager.apply Constants.PLUGIN_ID

        //try {
        assertTrue(project.tasks[Constants.TASK_SET_UP] instanceof GintInternalTask)
        assertTrue(project.tasks[Constants.TASK_TEAR_DOWN] instanceof GintInternalTask)
        assertTrue(project.tasks[Constants.TASK_FAILED] instanceof GintInternalTask)
        assertTrue(project.tasks[Constants.TASK_ALL] instanceof GintInternalTask)
        assertTrue(project.tasks[Constants.TASK_INITIALIZE] instanceof GintInternalTask)
        assertTrue(project.tasks[Constants.TASK_FINALIZE] instanceof GintInternalTask)
        //} catch (Exception exception) {
        //    exception.printStackTrace()
        //}
    }

    @Test
    public void testNoGint() throws Exception {

        createBuildFile()  // No gint tests

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments(Constants.TASK_SET_UP, '--info', '--stacktrace', '-DdbReport=') // set dbReport to blank for build systems that may have it set globally
                        .build()

        String output = result.getOutput()
        result.println('output: ' + output)

        assert !output.contains('= = = = = = ') // no reporting shown
    }

    @Test
    public void testOutcomes() throws Exception {

        String content = "gint.taskHelper.add(name: 'z1', inline: { task -> true })"
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments(Constants.TASK_SET_UP, Constants.TASK_TEAR_DOWN, '--stacktrace', '-DdbReport=')
                        .build()

        result.println('outcome: ' + result.task(":" + Constants.TASK_SET_UP).getOutcome())
        result.println('outcome: ' + result.task(":" + Constants.TASK_TEAR_DOWN).getOutcome())

        assertTrue(result.task(":" + Constants.TASK_SET_UP).getOutcome() == SUCCESS)
        assertTrue(result.task(":" + Constants.TASK_TEAR_DOWN).getOutcome() == SUCCESS)
        // assertTrue(result.task(":" + Constants.TASK_TEAR_DOWN).getOutcome() == UP_TO_DATE)
    }

    @Test
    public void testGintReporting() throws Exception {

        String content = "gint.taskHelper.add(name: 'z1', inline: { task -> true })"
        createBuildFile(content)

        BuildResult result = GradleRunner.create()
                        .withProjectDir(testProjectDir.getRoot())
                        .withPluginClasspath()
                        .withArguments(Constants.TASK_ALL, '--stacktrace', '-DdbReport=')
                        .build()

        String output = result.getOutput()
        result.println('output: ' + output)
        //result.println('- - - - - - - - - - - - - - - - - ')
        //result.println 'success found: ' + output.contains('[info] Successful base tasks . . . . . . . . . . : ')

        // reporting
        assert output.contains('[info] Successful base tasks . . . . . . . . . : ')
        assert output.contains('[info] Total base tasks  . . . . . . . . . . . : ')
        assert output.contains('[info] Elapsed run time  . . . . . . . . . . . : ')

        // header and footer
        assert output.contains('= = = = = =   build started at')
        assert output.contains('= = = = = =   build completed at')
    }
}
