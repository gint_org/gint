/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin

import java.nio.charset.Charset

import org.gint.helpers.ClosureHelper
import org.gint.helpers.CmdGeneratorHelper
import org.gint.helpers.Helper
import org.gint.helpers.ParallelHelper
import org.gint.helpers.ReportHelper
import org.gint.helpers.ScriptRunner
import org.gint.helpers.SeleniumHelper
import org.gint.helpers.SqlHelper
import org.gint.helpers.TaskHelper
import org.gint.interfaces.CmdGenerator
import org.gint.plugin.Constants.TaskType
import org.gint.tasks.GintAbstractTask
import org.gint.tasks.GintGroupTask
import org.gint.tasks.GintTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.logging.Logger
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional

import java.nio.file.Files

/**
 * Gint is a Gradle plugin extension object
 */
//@CompileStatic
//@TypeChecked
public class Gint {

    Project project
    Logger logger
    Closure message = Helper.getStandardMessageClosure()

    @Input @Optional
    String name

    @Input @Optional
    String suite

    @Input @Optional
    String tearDownStrategy // valid values are after (default), before, both, none

    @Input @Optional
    String initializationStrategy // valid values are early (default), late, minimal

    protected File propertyFile // set in initialize
    protected Properties properties   // set in initialize

    @Input @Optional
    Boolean stopNow

    @Internal
    long startTime // run start time

    @Internal
    boolean doReporting = false // turned on when there are gint tasks, turned off when report done

    @Internal
    boolean failed = false
    @Internal
    boolean initializeDone = false  // when all properties loaded and directories set

    @Internal
    List failedMessageList = [] // list of failure messages
    @Input @Optional
    directories = [:]

    @Internal
    protected hasInputDirectoryBeenCreated = false
    @Internal
    protected hasOutputDirectoryBeenCreated = false
    @Internal
    protected hasTestReportsDirectoryBeenCreated = false
    @Internal
    protected hasCompareDirectoryBeenCreated = false


    // parameters are set during initialization from properties and command line parameters
    @Input @Optional
    boolean clean = false  // true to ignore previous results for default start, don't remember status
    @Input @Optional
    boolean verbose = false  // true to show more output from successful tasks, stack traces
    @Input @Optional
    boolean ignoreDependsResult = false // true to not skip task if one depends task failed or skipped
    @Input @Optional
    boolean ignoreDepends = false // true to not start depends tasks or targets when task starts
    @Input @Optional
    boolean cleanOutputDirectory = false  // true to clean the output directory before starting
    @Input @Optional
    boolean useSynchronizedClosures = false // run closures through a synchronized method
    @Input @Optional
    boolean showFailDetail = true // set to false to suppress fail detail

    @Input @Optional
    int timeout = 0 // timeout in milliseconds for running task, 0 or negative is no timeout
    @Input @Optional
    int autoRetry = 0 // global retry count applied for any task that doesn't have retry set, a way to globally apply a retry on all tasks
    @Input @Optional
    int level = 1 // run level, run all tasks <= this level, or if set to false

    @Internal
    int nameCounter = 1 // counter for auto generating names for tasks with no name specified

    @Input @Optional
    String prefix // prefix to enable uniqueness for concurrent runs
    @Input @Optional
    String constructName // defaulted during initialization for space name (Confluence), project name (JIRA, Bamboo), etc...
    @Input @Optional
    String label = '' // label for a run, usually a version indicator. Used in setting up input/output directory structure
    @Input @Optional
    String compareLabel = '' // label for a completed run to compare with
    @Input @Optional
    String encoding // file encoding for IO

    @Input @Optional
    Collection includeLevels
    @Input @Optional
    Collection excludeLevels

    @Internal
    protected Set skippedLevels = [] as Set

    @Input @Optional
    CmdGenerator cmdGenerator

    @Input @Optional
    Object xmlReport // boolean or String, xmlReport file if requested, use -DxmlReport to use defaults
    @Input @Optional
    Object dbReport     // GINT_DB_REPORT environment variable as example
    @Input @Optional
    Object dbUser       // GINT_DB_USER environment variable as example
    @Input @Optional
    Object dbPassword   // GINT_DB_PASSWORD environment variable as example
    @Input @Optional
    Object dbRunTable   // GINT_DB_RUN_TABLE environment variabld as example
    @Input @Optional
    Object dbLogTable   // GINT_DB_LOG_TABLE environment variable as

    // db run column values, needs to be set
    @Input @Optional
    Object runRepository
    @Input @Optional
    Object runBranch
    @Input @Optional
    Object runCommit
    @Input @Optional
    Object runBuild
    @Input @Optional
    Object runVersion
    @Input @Optional
    Object runEnvironment

    @Input @Optional
    Object cmdLog // log file

    @Input @Optional
    Closure cmdLogClosure  // customize how to cmd are logged to enable them to be cleaner for documentation purposes
    @Input @Optional
    Closure addTaskClosure // customize how to determine task name from other parameters

    @Internal
    protected List<GintTask> tasksSkipped = []

    @Internal
    protected boolean hasIncludedPropertiesBeenLoaded = false

    @Internal
    protected finalTaskList = [] // tasks that need to finalize run tasks, like seleniumClose
    // Remember task ordering information so they order can be set with mustRunAfter settings, 0 is last, 1 is first, 2 second and so on
    @Internal
    Map<String, Map<Integer, List<GintTask>>> orderMap = [base: [0: []], setUp: [0: []], tearDown: [0: []]]

    /*
     * Helpers
     */
    @Internal
    Helper helper
    @Internal
    TaskHelper taskHelper
    @Internal
    ScriptRunner scriptRunner
    @Internal
    ReportHelper reportHelper
    @Internal
    SeleniumHelper seleniumHelper
    @Internal
    ParallelHelper parallelHelper
    @Internal
    SqlHelper sqlHelper
    @Internal
    ClosureHelper closureHelper
    @Internal
    CmdGeneratorHelper cmdGeneratorHelper


    /**
     * Definition stage init done as soon as project is defined.
     * @param project
     * @return
     */
    public void init(final Project project) {

        this.project = project
        logger = project.logger

        helper = new Helper(this) // this is always needed
        //helper.log 'init start time:  ', System.currentTimeMillis() // performance debugging
        //def initStartTime = System.currentTimeMillis()

        reportHelper = new ReportHelper(this) // this is always needed

        if (name == null) {
            def script = helper.getParameterValue('script')
            if (helper.isNotBlank(script)) {  // new way to run scripts
                name = helper.getNameNoExtension(helper.getNameNoPath(script))
            } else {
                name = helper.getNameNoExtension(project.buildFile) // legacy way to run scripts
            }
        }

        // project.beforeEvaluate({ p -> p.logger.quiet("before evaluate")}) // multi-project
        project.afterEvaluate(afterEvaluateClosure) // after configuration stage

        // Set here so other initializations will log appropriately
        verbose = helper.getBooleanParameterValue('verbose', helper.isVerbose())

        setTearDownStrategy(helper.getParameterValue('tearDownStrategy', tearDownStrategy))

        initializationStrategy = helper.getParameterValue(Constants.INITIALIZATION_STRATEGY, Constants.INITIALIZATION_STRATEGY_EARLY)  // early, late, minimal (no gint properties)
        if (initializationStrategy == Constants.INITIALIZATION_STRATEGY_EARLY) { // this doesn't allow script to set initial values
            initialize()
        }
        //helper.log 'init end time:     ', System.currentTimeMillis() // performance debugging
        //helper.log 'init elapsed time: ', (System.currentTimeMillis() - initStartTime)
    }

    /**
     * Run after the evaluation stage so tasks are defined. Set default tasks based on save properties. Set dependencies for all and default tasks.
     */
    public Closure afterEvaluateClosure = { Project p ->

        if (!initializeDone) {  // load some properties
            initializeAndLoadPropertiesFirst() // no directories needed
            loadRunStatusProperties() // this needs to be done to set failed property list and needs at least target directory initialized
            //helper.log('directories1', directories)
        }

        Task baseTask = p.getTasks().getByName(Constants.TASK_BASE)
        Task allTask = p.getTasks().getByName(Constants.TASK_ALL)
        Task failedTask = p.getTasks().getByName(Constants.TASK_FAILED)
        Task initializeTask = p.getTasks().getByName(Constants.TASK_INITIALIZE)
        Task finalizeTask = p.getTasks().getByName(Constants.TASK_FINALIZE)
        Task setUpTask = p.getTasks().getByName(Constants.TASK_SET_UP)
        Task tearDownTask = p.getTasks().getByName(Constants.TASK_TEAR_DOWN)
        Task tearDownAfterTask = p.getTasks().getByName(Constants.TASK_TEAR_DOWN_AFTER)

        List<String> runTaskList = project.gradle.startParameter.taskNames
        //helper.log('run tasks', runTaskList)

        if (runTaskList.isEmpty()) { // defaults only valid if no specific tasks set on command line

            List<String> defaultTaskList = project.getDefaultTasks()  // get default tasks already set
            //helper.log('default tasks', defaultTaskList)

            // Never override user provided default task list
            if (defaultTaskList.isEmpty()) {
                def failedList = getFailedList() // always not null
                if (clean || (failedList == null) || failedList.isEmpty()) {
                    defaultTaskList.add(Constants.TASK_ALL) // add our default task
                } else {
                    defaultTaskList.add(Constants.TASK_FAILED) // add our default task to re-run failed tasks
                }
                p.setDefaultTasks(defaultTaskList)     // reset project default tasks

                runTaskList = defaultTaskList  // used for finalize processing - TODO whether to keep this
            }
        }

        List<GintTask> setUpList = []
        List<GintTask> tearDownList = []
        List<GintTask> tearDownAfterList = []
        List<GintTask> baseList = []

        // Conceptually, neededBy and mustRunBefore are opposites to dependsOn and mustRunAfter
        // Often in integration testing easier to add these to one item instead of adding opposite to many items
        Map<GintTask, Object> neededByMap = new LinkedHashMap<GintTask, Object>()      // track these to apply later
        Map<GintTask, Object> mustRunBeforeMap = new LinkedHashMap<GintTask, Object>() // track these to apply later

        // Lists may be empty and are specific to this gint script
        // Skip means task does not execute, quarantine means task executes but ignoreFailure is set
        List<String> skipList
        List<String> quarantineList

        if (suite == null) {  // use < 2 part names compatible with previous support
            skipList = helper.getListFromQualifiedStringList(stringList: System.getenv('GINT_SKIP_LIST'), qualifier1: getName())
            quarantineList = helper.getListFromQualifiedStringList(stringList: System.getenv('GINT_QUARANTINE_LIST'), qualifier1: getName())
        } else { // use < 3 part names
            skipList = helper.getListFromQualifiedStringList(stringList: System.getenv('GINT_SKIP_LIST'), qualifier1: suite, qualifier2: getName())
            quarantineList = helper.getListFromQualifiedStringList(stringList: System.getenv('GINT_QUARANTINE_LIST'), qualifier1: suite, qualifier2: getName())
        }

        int gintTaskCount = 0  // count actual Gint base, setUp or tearDown tasks

        //p.tasks.each { Task entry ->
        p.tasks.matching { it instanceof GintAbstractTask }.all { GintAbstractTask task ->

            if (task.name == Constants.TASK_INITIALIZE) {
                // avoid circular references
            } else {
                //task.validate() // disabled if not valid
                if (task.name != Constants.TASK_FINALIZE) {
                    task.dependsOn(initializeTask)
                }

                if (task instanceof GintTask) { // base, setUp and tearDown tasks

                    gintTaskCount++

                    // Old way that worked, but too much gradle overhead for this as seen with volume tests taking hours
                    //if ((task.name != Constants.TASK_FINALIZE)) {
                    //    task.finalizedBy(finalizeTask)
                    //}

                    switch (task.type) {
                        case TaskType.SET_UP:
                            setUpList << task
                            addToOrderMap(task, 'setUp')
                            if (skipList.contains(task.name)) {
                                task.enabled = false
                                helper.log(Constants.MESSAGE_TASK_SKIP, task.name)
                            } else if (quarantineList.contains(task.name)) {
                                task.ignoreFailure = true
                                helper.log(Constants.MESSAGE_TASK_QUARANTINE, task.name)
                            }
                            break

                        case TaskType.TEAR_DOWN:
                        case TaskType.TEAR_DOWN_AFTER: // TODO do we need a different order type for AFTER ??
                            if (task.type == TaskType.TEAR_DOWN) {
                                tearDownList << task
                            } else {
                                tearDownAfterList << task
                            }
                            task.ignoreFailure = true
                            addToOrderMap(task, 'tearDown')
                            if (skipList.contains(task.name)) {
                                task.enabled = false
                                helper.log(Constants.MESSAGE_TASK_SKIP, task.name)
                            }
                            // tear down tasks do not need to be quarantined as they already have ignoreFailure
                            break

                        default:
                            if (!finalTaskList.contains(task)) { // final tasks are different
                                baseList << task
                                addToOrderMap(task, 'base')
                            }
                            if (skipList.contains(task.name)) {
                                task.enabled = false
                                helper.log(Constants.MESSAGE_TASK_SKIP, task.name)
                            } else if (quarantineList.contains(task.name)) {
                                task.ignoreFailure = true
                                helper.log(Constants.MESSAGE_TASK_QUARANTINE, task.name)
                            }
                    }

                    if (task.map?.neededBy != null) {
                        neededByMap.put(task, task.map.neededBy)
                    }
                    if (task.map?.mustRunBefore != null) {
                        mustRunBeforeMap.put(task, task.map.mustRunBefore)
                    }

                    // Task specific tear down
                    handleTearDownConfig(task, task.map?.tearDown) // TODO: consider just making it a task field instead of the map

                    // Dependency predicate
                    setPredicateToSkipOnDependencyFailure(task)
                }

                if (task.name in failedList) {
                    failedTask.dependsOn(task)
                }
            }
            // helper.log('task.' + task.name + ' dependsOn list', task.dependsOn)
        }

        //helper.log('tear down list', tearDownList)
        //helper.log('tear down after list', tearDownAfterList)

        setUpTask.dependsOn(setUpList)
        baseTask.dependsOn(baseList)

        if (doTearDownBeforeOnly()) {
            tearDownTask.dependsOn(tearDownList)
            setUpTask.mustRunAfter(tearDownList)
            setUpList.each { it.mustRunAfter(tearDownList) }
        } else {
            if (doTearDownBoth()) {  // both before and after
                setUpTask.mustRunAfter(tearDownTask)
                setUpList.each { it.mustRunAfter(tearDownList) }

                tearDownAfterTask.dependsOn(tearDownAfterList)
                tearDownAfterTask.mustRunAfter(setUpList + baseList + [baseTask])
                tearDownAfterList.each { it.mustRunAfter(baseList + [baseTask]) }
                // finalizeTask.mustRunAfter(tearDownAfterTask)
                // tearDownAfterList.each { finalizeTask.mustRunAfter(it) }

            } else if (doTearDownAfter()) { // after only
                tearDownTask.dependsOn(tearDownList)
                tearDownTask.mustRunAfter(setUpList + baseList + [baseTask])
                tearDownList.each { it.mustRunAfter(setUpList) }
                //  finalizeTask.mustRunAfter(tearDownTask)
                //  tearDownList.each { finalizeTask.mustRunAfter(it) }
            }
        }

        if (clean) {
            def list1 = setUpList + [setUpTask]+ (doTearDownBefore() ? tearDownList + [tearDownTask]: [])
            baseList.each { it.dependsOn(list1) }
        } else {
            def list1 = setUpList + [setUpTask]
            baseList.each { it.mustRunAfter(list1) }
        }

        // Disable Gint reporting since there are no user defined gint tasks in this run
        if (gintTaskCount == 0) {
            logger.info('No Gint tasks defined for this project.')
            initializeTask.enabled = false
            finalizeTask.enabled = false
            baseTask.enabled = false
            setUpTask.enabled = false
            tearDownTask.enabled = false
            tearDownAfterTask.enabled = false
            allTask.enabled = false
            failedTask.enabled = false

        } else {
            // only add finalize if we already have at least one gint task defined and limited to Gint tasks in run list
            // this was because of severe runtime performance problems (volume test) having a finalize task associated with every task
            p.tasks.matching { Task it ->
                (it instanceof GintAbstractTask) && (it.name != Constants.TASK_FINALIZE) && !runTaskList.contains(it.name)
            }.all { Task it ->
                finalizeTask.mustRunAfter(it)

                // TODO: Stop using this for now as final task list has marginal value as there is no guarantee it will run
                // since it is not a finalizedBy on every task
                // Use cases for clean-up like selenium driver is now specific
                finalTaskList.each { Task finalTask ->
                    if (it != finalTask) {
                        it.finalizedBy(finalTask)
                    }
                }
            }
        }
        handleBeforeRequest(neededByMap, "neededBy")
        handleBeforeRequest(mustRunBeforeMap, "mustRunBefore")

        // handleOrderConfig()
    }

    /**
     * Add task to appropriate order list for later processing. Ordering is done based on type (base, set up, tear down) groupings
     * @param task
     */
    protected void addToOrderMap(final GintTask task, final String type) {
        if (task.order >= 0) {
            if (orderMap[type][task.order] == null) {
                orderMap[type].put(task.order, [])
            }
            orderMap[type][task.order] << task
            if (task.order > 0) {
                // TODO
            }
        }
    }

    /**
     * Based on orderMap, set appropriate mustRunAfter relationships. 0 is last
     * @param task
     */
    protected void handleOrderConfig() {
        orderMap.each { String key, Map<Integer, List<GintTask>> map ->
            if (map != null) {
                List<Integer> list = new ArrayList<Integer>(map.keySet()) // list of order numbers
                list.remove(0) // 0 is ordered last, so don't need in list
                Collections.sort(list)
                list.reverse() // need in descending order
                List<GintTask> lastList = map[0]

                // Each last with the same order number must be after the collection of all tasks with the lower order number
                // This does nothing if there are no specifically ordered tasks
                list.each { int entry ->
                    lastList.each { task ->
                        task.mustRunAfter(map[entry])
                        taskHelper.addTaskToGroup(task, Constants.ORDER_TASK_PREFIX + entry)  // gintOrder group
                    }
                    lastList = map[entry]
                }
            }
        }
    }

    /**
     * Add depends on for tearDown specified in the map.
     * @param task
     */
    protected void handleTearDownConfig(final Task task, final Object value) {
        if (value instanceof String) {
            task.dependsOn(value)
        } else if (value instanceof List) {
            ((List) value).each { entry ->
                handleTearDownConfig(task, entry)
            }
        }
    }

    /**
     * All dependencies should be successful from a Gint perspective. Set predicate that calculates this.
     * Predicate result == false means skip the task.
     * @param task
     */
    protected void setPredicateToSkipOnDependencyFailure(final Task task) {
        task.onlyIf { Task t ->
            boolean result = true // means to run task
            for (Task entry in t.getTaskDependencies().getDependencies(t)) {
                //helper.log('dependency entry', entry.name)
                result = !(entry instanceof GintTask)
                if (!result) {
                    GintTask gintTask = (GintTask) entry
                    result = !gintTask.failed
                    if (!result && !task.isTearDown) { // respect teardown failure dependency failure for teardown tasks
                        result = gintTask.isTearDown == true
                    }
                    // println('\ntask name: ' + entry.name + ', result: ' + result)
                }
                if (!result) {
                    break
                }
            }
            return result
        }
    }

    /**
     * Handle needed by or must run after request from the map of requests. These are BEFORE requests - opposite of AFTER requests
     * @param map of request collected
     * @param type = neededBy or mustRunBefore
     */
    protected void handleBeforeRequest(final Map<GintTask, Object> map, final String type) {
        map.each { GintTask entry, value ->
            handleBeforeRequest(entry, value, type) // handle each request from the map
        }
    }

    /**
     * Set needed by or must run after for a task and if it is a group task, for all members of the group.
     * @param beforeTask
     * @param object that is some way identifies an after task or set of after tasks
     * @param type = neededBy or mustRunBefore
     */
    protected void handleBeforeRequest(final GintTask beforeTask, final Object object, final String type) {

        if (object instanceof String || object instanceof GString) {
            Task afterTask = project.getTasks().getByName(object)  // this will fail if not found
            handleBeforeRequest(beforeTask, afterTask, type)
        } else if (object instanceof Task) {
            handleBeforeRequest(beforeTask, (Task) object, type)
        } else if (object instanceof Closure) {
            handleBeforeRequest(beforeTask, ((Closure) object).call(), type)
        } else if (object instanceof Collection) {
            ((Collection) object).each {
                handleBeforeRequest(beforeTask, it, type)
            }
        } else {
            //helper.log 'ignore invalid before request', object.class.name
        }
    }

    protected void handleBeforeRequest(final GintTask beforeTask, final Task afterTask, final String type) {

        if (afterTask != null) {
            applyBeforeRequest(beforeTask, afterTask, type)

            def beforeTaskDependencies = beforeTask.taskDependencies.getDependencies()
            def beforeTaskMustRunAfterDependencies = beforeTask.mustRunAfter.getDependencies()
            //(beforeTaskDependencies + beforeTaskMustRunAfterDependencies).each {
            //    helper.log beforeTask.name + 'before', it.name
            //}

            if (afterTask instanceof GintGroupTask) {
                GintUtils.transitiveTaskDependencies(afterTask).each { Task entry ->

                    // Be careful to avoid circular references where possible, at least some of the obvious use cases
                    if ((entry instanceof GintTask) && !entry.equals(beforeTask)) {
                        if (((GintTask)entry).type == beforeTask.type) { // types match
                            if (!beforeTaskDependencies.contains(entry) && !beforeTaskMustRunAfterDependencies.contains(entry)) {
                                applyBeforeRequest(beforeTask, entry, type)
                                //println beforeTask.name + ' before request applied for ' + entry.name
                            }
                        }
                    }
                }
            }
        }
    }

    protected void applyBeforeRequest(final GintTask beforeTask, final Task afterTask, final String type) {

        //helper.log(beforeTask.name + ' ' + type, afterTask.name)
        if (type == 'neededBy') {
            afterTask.dependsOn(beforeTask)
        } else if (type == 'mustRunBefore'){
            afterTask.mustRunAfter(beforeTask)
        } else {
            throw new Exception('Unexpected exception. Contant support. Invalid after task type ' + type) // should never happen
        }
    }

    protected void initializeOtherDirectories() {

        cleanOutputDirectory = helper.getBooleanParameterValue('cleanOutputDirectory', cleanOutputDirectory) // do before touching outputDirectory ??

        if (directories.output == null) {
            directories.output = getDefaultOutputDirectory()
        }

        if (directories.compare == null) {
            directories.compare = getDefaultCompareDirectory()
        }
        //setDiffCli(helper.getParameterValue('diffCli', getDiffCli()))  // TODO
    }

    protected String getDefaultOutputDirectory() {
        label = helper.getParameterValue('label', label)
        if (helper.isNotBlank(label)) {
            return helper.getParameterValue('outputDirectory', Constants.COMPARE_DIRECTORY + GintUtils2.getFileSeparator() + name.toLowerCase() + GintUtils2.getFileSeparator() + label)
        } else {
            return helper.getParameterValue('outputDirectory', getTargetDirectory() + GintUtils2.getFileSeparator() + Constants.OUTPUT_DIRECTORY + GintUtils2.getFileSeparator() + name.toLowerCase())
        }
    }

    protected String getDefaultCompareDirectory() {
        compareLabel = helper.getParameterValue('compareLabel', compareLabel)
        if (helper.isNotBlank(compareLabel)) {
            return helper.getParameterValue('compareDirectory', Constants.COMPARE_DIRECTORY + GintUtils2.getFileSeparator() + name.toLowerCase() + GintUtils2.getFileSeparator() + compareLabel)
        } else {
            return outputDirectory
        }
    }

    /**
     * User can ask for early initialization of properties and directory fields at a specific time in the script.
     * Otherwise it could be done as late as possible depending on initializationStrategy
     */
    public void initialize() {

        if (!initializeDone) {
            clean = helper.getBooleanParameterValue('clean', getClean()) // needed before status properties are loaded

            // Things normally done after evaluation
            initializeAndLoadPropertiesFirst() // no directories needed
            loadRunStatusProperties() // this needs to be done to set failed property list and needs at least target directory initialized
            //helper.log('directories1', directories)

            // Things normally done initializeTask
            initializeAndLoadPropertiesNeedingTargetDirectory()
            // Load remaining properties before doing any further initialization
            // Source and resource directories will be initialized on first use after this
            initializeAndLoadPropertiesLast() // Stage 3 files, needs resource directory set
            initializeOtherDirectories()

            initializeDone = true
        }
    }

    /**
     * Initialize task runs before any other Gint task
     * @param project
     * @return
     */
    public void initializeTask() {

        startTime = System.currentTimeMillis()  // start time is run start time AFTER evaluation is completed
        logger.quiet(getReportHelper().getOutputHeader())

        if (!initializeDone) {
            initializeAndLoadPropertiesNeedingTargetDirectory()
            // Load remaining properties before doing any further initialization
            initializeAndLoadPropertiesLast() // Stage 3 files, needs resource directory set
            initializeOtherDirectories()
        }

        // See if verbose has been set later in properties resolution
        verbose = helper.getBooleanParameterValue('verbose', getVerbose() || helper.isVerbose())
        if (encoding == null) {
            encoding = helper.getParameterValue('encoding', Charset.defaultCharset().toString()) // set if available
        }

        // do after status properties are loaded before accessing any other properties
        ignoreDependsResult = helper.getBooleanParameterValue('ignoreDependsResult', getIgnoreDependsResult())
        ignoreDepends = helper.getBooleanParameterValue('ignoreDepends', getIgnoreDepends())

        level = helper.getParameterValue('level')?.equalsIgnoreCase(Constants.INCLUDE_ALL) ? Integer.MAX_VALUE : helper.getIntegerParameterValue('level', level)

        if (includeLevels == null) {  // not already set by user code, defaults to include
            String temp = helper.getParameterValue('includeLevels')
            if (temp instanceof Collection) {
                includeLevels = temp
            } else if (temp == null || temp instanceof String || temp instanceof GString) {
                includeLevels = helper.separatedStringToList(temp ?: 'true', ',', true)
            }
        }
        if (excludeLevels == null) {  // not already set by user code
            excludeLevels = helper.separatedStringToList(helper.getParameterValue('excludeLevels') ?: 'false', ',', true)
        }

        timeout = helper.getIntegerParameterValue('timeout', getTimeout())
        autoRetry = helper.getIntegerParameterValue('autoRetry', getAutoRetry())

        helper.setLogFilter(helper.getParameterValue('logFilter', helper.getLogFilter()))
        reportHelper.setReportFilter(helper.getParameterValue('reportFilter', reportHelper.getReportFilter()))

        setCmdGenerator(getCmdGenerator()) // this provides an opportunity for subclasses to override and set a default cmd generator (after initialization has been done)

        reportHelper.prepareForXmlReport()
        reportHelper.prepareForCmdLog()

        doReporting = true // turn on reporting

        if (getVerbose()) {
            logVerboseHeaderInfo()
        }

        stopNow = getStopNow() || getFailed()  // handles profile load failures
    }

    /**
     * First stage of loading properties. No directory parameters needed.
     * These file locations are generic (do not depend on any directories that can be specified by user defined parameters), so can be loaded early.
     * An exception is gintUserProperties which can only be provided by parameter or environment variable.
     * This allows for all subsequent values to be customized in these locations. Specifically target directory.
     * Properties are loaded as Ant properties
     */
    protected void initializeAndLoadPropertiesFirst() {

        if (initializationStrategy != Constants.INITIALIZATION_STRATEGY_MINIMAL) {
            // Get the user's gint.properties file
            loadAntProperties(helper.getParameterValue('gintUserProperties', GintUtils2.getUserHome() + GintUtils2.getFileSeparator() + Constants.PROPERTY_FILENAME))
            loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

            // Get gint.properties in the current directory
            loadAntProperties(Constants.PROPERTY_FILENAME)
            loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded
        }
    }

    /**
     * Second stage of loading properties where we need the target directory set.
     */
    protected void initializeAndLoadPropertiesNeedingTargetDirectory() {

        if (initializationStrategy != Constants.INITIALIZATION_STRATEGY_MINIMAL) {
            // Get properties from the target directory which includes the failedList
            def propertyPath = getTargetDirectory() + GintUtils2.getFileSeparator() + Constants.PROPERTY_FILENAME

            // Target gint properties in case project is using filtering to modify values when resources -> target
            if (GintUtils2.getAbsoluteFile(new File(propertyPath)).exists()) {
                loadAntProperties(helper.getParameterValue('gintProperties', propertyPath))
                loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded
            }
        }
    }

    /**
     * Third stage of loading properties where we need resource directory set.
     * These file locations are based on directory parameters.
     * Properties are loaded as Ant properties
     */
    protected void initializeAndLoadPropertiesLast() {

        // Get project specific properties from the resource directory after the target directory settings
        loadAntProperties(helper.getParameterValue('gintProperties', resourceDirectory + GintUtils2.getFileSeparator() + Constants.PROPERTY_FILENAME))
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

        // Profiles - yet another type of property support
        def profileList = helper.separatedStringToList(helper.getParameterValue('profiles'))
        def directoriesParameter = helper.getParameterValue('directories')  // TODO - conflict with directories ???
        def directoryList = (directoriesParameter != null) ?
                        helper.separatedStringToList(directoriesParameter) :
                        [
                            GintUtils2.getUserHome(),
                            getTargetDirectory(),
                            getResourceDirectory(),
                            helper.getCurrentDirectory()
                        ]
        if (!helper.loadProfile(profileList, directoryList)) {
            addFailedMessage("Failed loading profiles.")
        }
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded
    }

    protected String getUserHome() {
        //assert project.ant.properties.'user.home' == System.getProperty('user.home')
        return project.ant.properties.'user.home'
    }

    // leave in for compatibility
    protected String getFileSeparator() {
        return GintUtils2.getFileSeparator()
    }
    /**
     * Get all tasks of type GintTask. This is a replacement for gint2: getTestacases()
     * @return Set of tasks
     */
    public Set<GintTask> getGintTasks() {
        return project.tasks.withType(GintTask)
    }

    protected void loadAntProperties(String fileName) {
        //helper.log('Gint properties loaded from', fileName)
        fileName = GintUtils2.getAbsoluteFile(fileName).getAbsolutePath()
        helper.dLog('Gint properties loaded from', fileName)
        //helper.log('Gint properties loaded from', fileName)
        try {
            project.ant.property(file: fileName)
            //helper.log('get properties', project.ant.getProperties())
        } catch (Exception e) {
            message 'warning', 'Problem loading properties from ' + fileName + '. ' + e.toString()
        }
    }

    /**
     * Run status is stored in a properties file named after the test and stored in either
     * the output directory (if exists) or current directory, don't create the output directory!!!!
     */
    protected void loadRunStatusProperties() {

        if (initializationStrategy != Constants.INITIALIZATION_STRATEGY_MINIMAL) {
            def propertyDirectory = (new File(getTargetDirectory()).exists()) ? getTargetDirectory() : helper.getCurrentDirectory()
            propertyFile = new File(propertyDirectory, getName().toLowerCase() + '.properties')  // script specific property file
            //helper.log('load property path', propertyFile.getAbsolutePath())

            if (propertyFile.exists()) {  // if already exists, then load it
                properties = helper.loadProperties(propertyFile)
                //helper.log('loaded properties', properties)
            } else {
                properties = new Properties()
            }
        }
    }

    /**
     * Load property files indicated by the includePropertyFiles property or parameter
     */
    protected void loadIncludedProperties() {

        //helper.log('hasIncludedPropertiesBeenLoaded', hasIncludedPropertiesBeenLoaded)

        // Ant properties are immutable, so only the first include file will work, that is why we only load once
        if (!hasIncludedPropertiesBeenLoaded) {
            def includes = helper.getParameterValue('includePropertyFiles')
            // helper.log('includes', includes)
            if (helper.isNotBlank(includes)) {
                def propertyFileList = helper.csvDataAsList(data: includes) // split comma separated list
                propertyFileList.each { name ->
                    if (name.startsWith('~' + GintUtils2.getFileSeparator())) {  // handle user home replacement
                        name = getUserHome() + GintUtils2.getFileSeparator() + name.substring(2)
                    }
                    loadAntProperties(name)
                }
                hasIncludedPropertiesBeenLoaded = true
            }
        }
    }

    protected List<String> getFailedList() {
        def failedString = clean || (properties == null) ? null : properties.getProperty('failed')
        //helper.log('failedString', failedString)
        return helper.isNotBlank(failedString) ? helper.separatedStringToList(failedString) : null
    }

    protected void saveFailedList(final List failedList) {
        //helper.log('properties', properties)
        if (properties != null) {
            properties.setProperty('failed', helper.listToSeparatedString(failedList))
            if (!propertyFile.getParentFile().exists()) {
                Files.createDirectories(propertyFile.getParentFile().toPath()) // make sure parent exists
            }
            helper.storeProperties(propertyFile, properties)
        }
    }

    public void addFailedMessage(final String string, boolean markFailed = true) {
        if (markFailed) {
            failed = true
        }
        message 'error', string
        this.failedMessageList << string
    }

    public void logFailedMessages() {
        failedMessageList.each { string -> message('error', string) }
    }

    protected logVerboseHeaderInfo() {
        helper.log('Gint version', getVersion()) // Gint plugin version
        helper.log('clean', clean)
        helper.log('tearDownStrategy', tearDownStrategy)
        helper.log('level', level)
        helper.log('includeLevels', includeLevels)
        helper.log('excludeLevels', excludeLevels)

        if (autoRetry > 0) {
            helper.log('autoRetry', autoRetry)
        }
        if (helper.isNotBlank(label)) {
            helper.log('label', label)
        }
        if (helper.isNotBlank(compareLabel)) {
            helper.log('compareLabel', compareLabel)
        }
        // Initialize lazy initialized directories not already accessed
        inputDirectory
        testReportsDirectory
        helper.log('directories', directories)
    }

    /**
     * Print stack trace if --stacktrace flag is set
     * @param exception
     */
    public void printStackTrace(final Exception exception) {
        //helper.log('show stack trace', project.gradle.startParameter.showStacktrace)
        if (project.gradle.startParameter.showStacktrace == project.gradle.startParameter.showStacktrace.ALWAYS || project.gradle.startParameter.showStacktrace == project.gradle.startParameter.showStacktrace.ALWAYS_FULL) {
            exception.printStackTrace()
        }
    }

    public void printStackTrace(final Error exception) {
        if (project.gradle.startParameter.showStacktrace == project.gradle.startParameter.showStacktrace.ALWAYS || project.gradle.startParameter.showStacktrace == project.gradle.startParameter.showStacktrace.ALWAYS_FULL) {
            exception.printStackTrace()
        }
    }

    /**
     * Best if we automatically clean up as we can do so as part of final things to ensure it always gets done. Much harder for
     * script to do so easily, but, they can also do their own if they really want
     */
    public void closeSqlConnections() {
        if (sqlHelper != null) {
            sqlHelper.closeSqlConnections()
        }
    }

    /**
     * Best if we automatically clean up as we can do so as part of final things to ensure it always gets done. Much harder for
     * script to do so easily, but, they can also do their own if they really want
     */
    public void closeSeleniumDrivers() {
        if (seleniumHelper != null) {
            seleniumHelper.closeDrivers()
        }
    }

    // - - - - - - Helpers - - - - - -

    public Helper getHelper() {
        return helper
    }

    public TaskHelper getTaskHelper() {
        if (taskHelper == null) {
            taskHelper = new TaskHelper(this)
        }
        return taskHelper
    }

    public ScriptRunner getScriptRunner() {
        if (scriptRunner == null) {
            scriptRunner = new ScriptRunner(this)
        }
        return scriptRunner
    }

    public ReportHelper getReportHelper() {
        return reportHelper
    }

    public SeleniumHelper getSeleniumHelper() {
        if (seleniumHelper == null) {
            seleniumHelper = new SeleniumHelper(this)
        }
        return seleniumHelper
    }

    public ParallelHelper getParallelHelper() {
        if (parallelHelper == null) {
            parallelHelper = new ParallelHelper(this)
        }
        return parallelHelper
    }

    public SqlHelper getSqlHelper() {
        if (sqlHelper == null) {
            sqlHelper = new SqlHelper(this)
        }
        return sqlHelper
    }

    public ClosureHelper getClosureHelper() {
        if (closureHelper == null) {
            closureHelper = new ClosureHelper(this)
        }
        return closureHelper
    }

    public CmdGeneratorHelper getCmdGeneratorHelper() {
        if (cmdGeneratorHelper == null) {
            cmdGeneratorHelper = new CmdGeneratorHelper(this)
        }
        return cmdGeneratorHelper
    }

    // Directories
    public Map<String, String> getDirectories() {
        return directories
    }
    public String getInputDirectory() {
        if (directories.input == null) {
            directories.input = helper.getFileParameterValue('inputDirectory', resourceDirectory, helper.getCurrentDirectory())
        }
        if (!hasInputDirectoryBeenCreated) {
            GintUtils2.getAbsoluteFile(directories.input).mkdirs() // creates all directories needed for the path
            this.hasInputDirectoryBeenCreated = true
        }
        return directories.input
    }
    public String setInputDirectory(final dir) {
        directories.input = dir
    }
    public String getOutputDirectory() {
        if (!hasOutputDirectoryBeenCreated) {
            if (directories.output == null) { // fails if initialization not done
                directories.output = getDefaultOutputDirectory()
            } else if (getCleanOutputDirectory()) {
                GintUtils2.getAbsoluteFile(directories.output).deleteDir() // clean out everything
            }
            GintUtils2.getAbsoluteFile(directories.output).mkdirs() // creates all directories needed for the path
            this.hasOutputDirectoryBeenCreated = true
        }
        return directories.output
    }
    public String setOutputDirectory(final dir) {
        directories.output = dir
    }
    public String getResourceDirectory() {
        if (directories.resource == null) {
            directories.resource = helper.getFileParameterValue('resourceDirectory',
                getSourceDirectory() + GintUtils2.getFileSeparator() + Constants.RESOURCE_DIRECTORY,
                helper.getCurrentDirectory()
            )
        }
        return directories.resource
    }
    public String setResourceDirectory(final dir) {
        directories.resource = dir
    }
    public String getSourceDirectory() {
        if (directories.source == null) {
            directories.source = helper.getFileParameterValue('sourceDirectory',
                new File(project.projectDir, Constants.SOURCE_DIRECTORY).getAbsolutePath(),
                new File(helper.getCurrentDirectory(), Constants.SOURCE_DIRECTORY).getAbsolutePath(),
                helper.getCurrentDirectory())
        }
        return directories.source
    }
    public String setSourceDirectory(final dir) {
        directories.source = dir
    }
    public String getTargetDirectory() {
        if (directories.target == null) {
            directories.target = helper.getFileParameterValue('targetDirectory',
                project.buildDir.toString(),
                new File(helper.getCurrentDirectory(), Constants.BUILD_DIRECTORY).getAbsolutePath(),
                new File(helper.getCurrentDirectory(), Constants.TARGET_DIRECTORY).getAbsolutePath(),
                helper.getCurrentDirectory() // always need something that does exist
            )
            GintUtils2.getAbsoluteFile(directories.target).mkdirs() // make sure target directory exists
        }
        return directories.target
    }
    public String setTargetDirectory(final dir) {
        directories.target = dir
    }
    protected String getImageDirectory() { // internal only at least for now
        return getOutputDirectory() // same as output for now
    }
    public String getTestReportsDirectory() {
        if (directories.testReports == null) {
            directories.testReports = helper.getParameterValue('testReportsDirectory', getTargetDirectory() + GintUtils2.getFileSeparator() + Constants.TEST_REPORTS_DIRECTORY)
        }
        if (!hasTestReportsDirectoryBeenCreated) {
            GintUtils2.getAbsoluteFile(directories.testReports).mkdirs() // creates all directories needed for the path
            hasTestReportsDirectoryBeenCreated = true
        }
        return directories.testReports
    }
    public void setTestReportsDirectory(final dir) {
        directories.testReports = dir
    }
    public String getCompareDirectory() {
        if (!hasCompareDirectoryBeenCreated) {
            GintUtils2.getAbsoluteFile(directories.compare).mkdirs() // creates all directories needed for the path
            this.hasCompareDirectoryBeenCreated = true
        }
        return directories.compare
    }
    public void setCompareDirectory(final dir) {
        directories.compare = dir
    }
    protected String getCompareImageDirectory() { // internal only
        return directories.compare  // same as compare for now
    }

    public boolean getCleanOutputDirectory() {
        return this.cleanOutputDirectory
    }
    public void setCleanOutputDirectory(final boolean cleanOutputDirectory = true) {
        this.cleanOutputDirectory = cleanOutputDirectory
    }

    /**
     * get an input file name, add .txt if no extension is provided
     */
    public String getInputFile(final name, final whenNoExtension = '.txt') {
        return getInputDirectory() + GintUtils2.getFileSeparator() + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * get an output file name, add .txt if no extension is provided
     */
    public String getOutputFile(final name, final whenNoExtension = '.txt') {
        return getOutputDirectory() + GintUtils2.getFileSeparator() + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * get an resource file name, add .txt if no extension is provided
     */
    public String getResourceFile(final name, final whenNoExtension = '.txt') {
        return getResourceDirectory() + GintUtils2.getFileSeparator() + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * get compare file name, add .txt if no extension is provided
     */
    public String getCompareFile(final name, final whenNoExtension = '.txt') {
        return getCompareDirectory() + GintUtils2.getFileSeparator() + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * Get construct name with default if not set
     */
    public String getConstructName() {
        if (constructName == null) {
            constructName = getPrefix() + name
        }
        return constructName
    }

    /**
     * Subclass can override this to set a default cmd generator automatically
     * @return current command generator
     */
    public void setCmdGenerator(final CmdGenerator cmdGenerator) {
        this.cmdGenerator = cmdGenerator
    }

    public void setCmdGenerator(final String name, final Map parameters = null) {
        setCmdGenerator(getCmdGeneratorHelper().getCmdGenerator(name, parameters))
    }

    // Delay prefix setting until first use so it can be set in gint properties if necessary
    public String getPrefix() {
        if (prefix == null) {
            prefix = helper.getParameterValue('prefix', Constants.PREFIX)
        }
        return prefix
    }

    public String getNextName() {
        return getPrefix() + nameCounter++
    }

    public setTearDownStrategy(final String value) {
        tearDownStrategy = value == null ? Constants.TEAR_DOWN_STRATEGY_DEFAULT : value.toLowerCase()
        if (!Constants.TEAR_DOWN_STRATEGY_LIST.contains(tearDownStrategy)) {
            throw new GradleException('Invalid tearDownStrategy specified. Valid values are: ' + Constants.TEAR_DOWN_STRATEGY_LIST)
        }
    }

    public void addExcludeLevels(final List newLevels) {
        if (this.excludeLevels != null) {
            this.excludeLevels += newLevels
        } else {
            this.excludeLevels = newLevels
        }
    }

    public void addExcludeLevels(final String newLevels) {
        addExcludeLevels(helper.separatedStringToList(newLevels))
    }

    public boolean doTearDownBeforeOnly() {
        return tearDownStrategy == 'before'
    }

    public boolean doTearDownBefore() {
        return ['before', 'both'].contains(tearDownStrategy)
    }

    public boolean doTearDownAfter() {
        return ['after', 'both'].contains(tearDownStrategy)
    }

    public boolean doTearDownBoth() {
        return tearDownStrategy == 'both'
    }

    /**
     * Get gint plugin version from the manifest file from the jar
     */
    public String getVersion() {
        return this.class.getPackage().implementationVersion
    }

    /**
     * We don't want subclasses to replace an existing helper, only set it to a subclass as part of initial setup of a script.
     * This sets the gint seleniumHelper variable
     * @param inSeleniumHelper
     */
    public void setSeleniumHelperIfNotDefined(final inSeleniumHelper) {
        //helper.log 'before selenium class', this.@seleniumHelper
        if ((this.@seleniumHelper == null) && (inSeleniumHelper instanceof SeleniumHelper)) {
            this.@seleniumHelper = inSeleniumHelper
        }
    }

    /**
     * Provide an easy way for scripts to access jdbc and especially easily enable dbReport capability.
     * More over, automatically handle this for postgres as the default database access type.
     * This updates the project configurations including adding a jdbc configuration.
     * @param dependency - specification acceptable to dependencies definition like: org.postgresql:postgresql:4.2.5
     */
    public void addJdbcConfiguration(String dependency = null) {
        if (dependency == null) {
            dependency = 'org.postgresql:postgresql:' + (project.findProperty('postgresJdbcVersion') ?: Constants.DEFAULT_POSTGRES_JDBC_VERSION)
        }
        project.repositories { jcenter() }  // Access postgres or any publicly available JDBC drivers
        project.configurations { jdbc }
        project.dependencies { jdbc dependency }
        project.configurations.jdbc.each { groovy.sql.Sql.classLoader.addURL it.toURI().toURL() }
    }
}
