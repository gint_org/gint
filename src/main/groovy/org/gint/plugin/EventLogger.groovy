/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin

import org.gradle.BuildAdapter
import org.gradle.api.Task
import org.gradle.api.execution.TaskActionListener
import org.gradle.api.execution.TaskExecutionListener
import org.gradle.api.tasks.TaskState

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

/**
 * Customize the output messaging for a script run
 */
@CompileStatic
@TypeChecked
class EventLogger extends BuildAdapter implements TaskExecutionListener, TaskActionListener {

    @Override
    public void beforeExecute(Task task) {
        //println "[$task.name]"
    }

    @Override
    public void afterExecute(Task task, TaskState state) {
        //println()
        //        if (task instanceof GintTask) {
        //            println(task.getName() + ' after execute')
        //            println('The output is: ' + ((GintTask)task).standardOutput.toString())
        //
        //        }
    }

    //  @Override
    //    public void buildFinished(BuildResult result) {
    //
    //        if (result.getFailure() == null) {
    //            println 'Gint test completed'
    //        } else {
    //            result.rethrowFailure()
    //        }
    //        //if (result.failure != null) {
    //        //    result.failure.printStackTrace()
    //        //}
    //    }

    // Task action listener
    @Override
    public void beforeActions(Task task) {
        //println('xxx before')
    }

    @Override
    public void afterActions(Task task) {

        //        if (task instanceof GintTask) {
        //            println(task.getName() + ' after actions')
        //            println('The output is: ' + task.st
        //        }
    }
}
