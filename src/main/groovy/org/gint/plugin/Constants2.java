/*
 * Copyright (c) 2009, 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin;

public class Constants2 {

    public static final String PLUGIN_ID = "org.gint";

    public static final String EOL = "\n";
    public static final String GROOVY_CODE_ENDING = "_code";
    public static final String FIELD_NAME_USER_MAP = "map"; // user data map field name

    // GintTask types for processing tasks
    enum TaskType {
        BASE,
        SET_UP,
        TEAR_DOWN,
        TEAR_DOWN_AFTER
    }

    // Gradle task grouping
    public static final String GROUP_BASE = "Gint Base";
    public static final String GROUP_SET_UP = "Gint Set Up";
    public static final String GROUP_TEAR_DOWN = "Gint Tear Down";

    // Task names
    public static final String TASK_FAILED = "gintFailed";
    public static final String TASK_BASE = "gintBase";
    public static final String TASK_SET_UP = "gintSetUp";
    public static final String TASK_TEAR_DOWN = "gintTearDown";
    public static final String TASK_TEAR_DOWN_AFTER = "gintTearDownAfter";
    public static final String TASK_ALL = "gintAll";
    // public static final String TASK_GRAPH = "gintGraph"
    public static final String TASK_INITIALIZE = "gintInitialize";
    public static final String TASK_FINALIZE = "gintFinalize";
    // Do we still need this, I don't think so - user can run another cleanup task
    // after our finalizer task
    // public static final String TASK_FINAL = "gintFinal" // special named task
    // that user can define that will be included in our finalize processing after
    // reporting is completed

    // Task descriptions
    public static final String TASK_DESCRIPTION_FAILED = "Run last failed tasks";
    public static final String TASK_DESCRIPTION_BASE = "Run base tasks";
    public static final String TASK_DESCRIPTION_SET_UP = "Run set up tasks";
    public static final String TASK_DESCRIPTION_TEAR_DOWN = "Run tear down tasks";
    public static final String TASK_DESCRIPTION_TEAR_DOWN_AFTER = "Run tear down after tasks";
    public static final String TASK_DESCRIPTION_ALL = "Run all tasks including base, set up, and tear down tasks";
    public static final String TASK_DESCRIPTION_GRAPH = "List Gint tasks with their depends on tasks";
    public static final String TASK_DESCRIPTION_INITIALIZE = "Initialize is an automatically run task that completes Gint initialization including loading property files";
    public static final String TASK_DESCRIPTION_FINALIZE = "Finalize is an automatically run task that reports on results";
    public static final String TASK_DESCRIPTION_GROUP = "Task group";

    // Task log messages
    public static final String MESSAGE_TASK_SKIP = "Task marked skipped by GINT_SKIP_LIST environment variable";
    public static final String MESSAGE_TASK_QUARANTINE = "Task marked quarantined by GINT_QUARANTINE_LIST environment variable";

    // Fail exit codes
    public static final int FAIL_TIMEOUT = -96; // task was abandoned due to a timeout
    public static final int FAIL_ERROR = -97; // return code for a unknown, non-assert error, example - a Java error
    public static final int FAIL_ASSERTION = -98; // return code for a failed assertion
    public static final int FAIL_EXCEPTION = -99; // return code for an exception raised

    public static final String PROPERTY_FILENAME = "gint.properties";
    public static final String SOURCE_DIRECTORY = "src/itest";
    public static final String TARGET_DIRECTORY = "target"; // normal for Maven projects
    public static final String BUILD_DIRECTORY = "build"; // normal for Gradle projects
    public static final String COMPARE_DIRECTORY = "compare";
    public static final String OUTPUT_DIRECTORY = "output"; // relative (default to target directory)
    public static final String RESOURCE_DIRECTORY = "resources"; // relative (default to source directory)
    public static final String TEST_REPORTS_DIRECTORY = "test-reports"; // relative (default to target directory)
    public static final String PREFIX = "z"; // prefix for construct name
    public static final int DISPLAY_LIMIT_SUMMARY = 300; // limit some potentially long logging requests for summaries
    public static final int DISPLAY_LIMIT = 3000; // limit some potentially long logging requests, use verbose for more
    public static final String XML_ENCODING = "UTF-8"; // XML report encoding
    public static final String INCLUDE_ALL = "all"; // include level all
    public static final String DEFAULT_REPORT_PROPERTY_EXCLUDES = "password"; // filter out properties in reports that
                                                                              // contain password in the name

    public static final String ORDER_TASK_PREFIX = "gintOrder";

    public static final String TEAR_DOWN_STRATEGY_DEFAULT = "before";

    public static final String INITIALIZATION_STRATEGY = "initializationStrategy";
    public static final String INITIALIZATION_STRATEGY_MINIMAL = "minimal"; // no gint properties loaed/saved
    public static final String INITIALIZATION_STRATEGY_EARLY = "early";
    public static final String INITIALIZATION_STRATEGY_LATE = "late";

    // Reporting - ascii color codes
    // http://jafrog.com/2013/11/23/colors-in-terminal.html
    // http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
    public final static String GREEN = "\033[0;32m"; // start green
    public final static String RED = "\033[0;31m"; // start red
    public final static String NO_COLOR = "\033[0m"; // always end color change back to no color

    // SQL
    public final static String DEFAULT_POSTGRES_JDBC_VERSION = "42.7.3";

    public final static String SUCCESSFUL = "SUCCESSFUL";
    public final static String FAILED = "FAILED";
    public final static String FAILED_PADDED = FAILED + "    ";
}
