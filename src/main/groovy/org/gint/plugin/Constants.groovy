/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

@CompileStatic
@TypeChecked
class Constants extends Constants2 {

    public static final Set<String> RESERVED_NAME_LIST = ["tasks", "help"] as Set

    // GintTask types for processing tasks
    enum TaskType {
        BASE,
        SET_UP,
        TEAR_DOWN,
        TEAR_DOWN_AFTER
    }

    public static final TEAR_DOWN_STRATEGY_LIST = [
        'after',
        'before',
        'both',
        'none'
    ]

    // Ignore some fields when converting a map into a task
    public static final List<String> IGNORE_TASK_KEY_LIST = [
        'ext',
        // ext is used in a map as a name extension
        'project',
        // project is a gradle value
        'timeout',  // gradle field for timeout which is different than our at least for now - todO:
    ]
}
