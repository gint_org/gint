/*
 * Copyright (c) 2009, 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

import org.gint.plugin.Constants.TaskType
import org.gint.tasks.GintTask
import org.gradle.api.Task
import org.gradle.internal.nativeintegration.console.ConsoleDetector
import org.gradle.internal.nativeintegration.console.ConsoleMetaData
import org.gradle.internal.nativeintegration.services.NativeServices
import org.gint.helpers.Helper

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Groovy static utilities. See also the Java version in GintUtils2
 */
@CompileStatic
@TypeChecked
class GintUtils extends GintUtils2 {

    /**
     * Gradle only provides first level dependencies, this method gives transitive dependencies as well
     * @param task
     * @param includeTask - true if you want to include the original task in the set
     * @return set of tasks
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Set<? extends Task> transitiveTaskDependencies(Task task, boolean includeTask = false) {

        Set<? extends Task> set = new HashSet<? extends Task>()

        task.getTaskDependencies().getDependencies().each {

            if (it instanceof Task) {
                set.add((Task) it)
                if (!set.contains(it)) {
                    set.addAll(transitiveTaskDependencies((Task) it))
                }
            }
        }
        if (includeTask) {
            set.add(task)
        }
        return set
    }


    @CompileStatic(TypeCheckingMode.SKIP)
    static public Set<? extends Task> transitiveTaskDependencies(Set<? extends Task> tasks) {

        Set<? extends Task> set = new HashSet<? extends Task>()

        tasks.each {
            if (!set.contains(it)) {
                transitiveTaskDependencies(it, false).each { set.add(it) }
            }
        }
        return set
    }

    static String getGroup(GintTask task) {
        switch (task.type) {
            case TaskType.SET_UP: return Constants.GROUP_SET_UP

            case TaskType.TEAR_DOWN:
                case TaskType.TEAR_DOWN_AFTER: return Constants.GROUP_TEAR_DOWN

            default: return Constants.GROUP_BASE
        }
    }
}
