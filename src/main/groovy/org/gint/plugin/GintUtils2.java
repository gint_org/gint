/*
 * Copyright (c) 2009, 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.gradle.internal.nativeintegration.console.ConsoleDetector;
import org.gradle.internal.nativeintegration.console.ConsoleMetaData;
import org.gradle.internal.nativeintegration.services.NativeServices;

/**
 * Java static utilities
 */
public class GintUtils2 {

    static public void debugStackTrace() {
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            System.out.println(ste);
        }
    }

    /**
     * This doesn't seem to work as consoleMetaData seems to always be null
     *
     * @return
     */
    static public boolean hasConsole() {
        ConsoleDetector consoleDetector = NativeServices.getInstance().get(ConsoleDetector.class);
        ConsoleMetaData consoleMetaData = consoleDetector.getConsole();
        return consoleMetaData != null;
        // return NativeServices.getInstance().get(ConsoleDetector.class).getConsole()
        // != null
    }

    static public void deleteFileQuietly(final String name) {
        try {
            Path path = FileSystems.getDefault().getPath(name);
            Files.deleteIfExists(path);
        } catch (Exception ignore) {
        }
    }

    static public String getCurrentDirectory() {
        return System.getProperty("user.dir");
    }

    static public String getUserHome() {
        return System.getProperty("user.home");
    }

    static public String getFileSeparator() {
        return System.getProperty("file.separator");
    }

    static public File getAbsoluteFile(String file) {
        return getAbsoluteFile(new File(file), true);
    }

    static public File getAbsoluteFile(String file, final boolean createParents) {
        return getAbsoluteFile(new File(file), createParents);
    }

    static public File getAbsoluteFile(File file) {
        return getAbsoluteFile(file, true);
    }

    static public File getAbsoluteFile(File file, final boolean createParents) {
        if (!file.isAbsolute()) {
            file = new File(getCurrentDirectory(), file.getPath());
        }
        // System.out.println("absolute file: " + file.getAbsolutePath())
        if (createParents) { // make sure parents exist
            try {
                Files.createDirectories(file.getParentFile().toPath());
                // System.out.println("parents created")
            } catch (IOException e) {
                // Ignore - let something else fail later
            }
        }
        return file;
    }

    static public boolean isWindows() {
        return System.getProperty("os.name").contains("Windows");
    }

    /**
     * Copied from CliUtils
     * Quote a string by escaping or doubling up internal quotes - defaults to
     * single quote
     *
     * @param string      to quote, if null returns blank
     * @param quote       character (usually ' or ")
     * @param beforeQuote character added before any quote character found within
     *                    the string (usually the same as quote, but could be an
     *                    escape character)
     * @result string starting and ending with quote character with internal quote
     *         characters handled
     */
    static public String quoteString(final String string, final char quote, final char beforeQuote) {
        StringBuilder result = new StringBuilder();
        if (string != null) {
            result.append(quote);
            for (int i = 0; i < string.length(); i++) {
                char aChar = string.charAt(i);
                if (aChar == quote) {
                    result.append(beforeQuote);
                }
                result.append(aChar);
            }
            result.append(quote);
        }
        return result.toString();
    }

    /**
     * Single quote string with standard doubling
     *
     * @param string
     * @return single quoted string
     */
    static public String quoteString(final String string) {
        return quoteString(string, '\'', '\'');
    }

    /**
     * Single quote string with standard escaping
     *
     * @param string
     * @return single quoted string
     */
    static public String quoteStringWithEscape(final String string) {
        return quoteString(string, '\'', '\\');
    }

    /**
     * Double quote string with standard doubling
     *
     * @param string
     * @return double quoted string
     */
    static public String doubleQuoteString(final String string) {
        return quoteString(string, '"', '"');
    }

    /**
     * Simple thread sleep ignoring exception
     *
     * @param milliseconds
     */
    static public void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ignore) {
        }
    }

    /**
     * Safe way to convert a string to a int value
     *
     * @param value
     * @param defaultValue
     * @return
     */
    static public Integer getInteger(final String value, Integer defaultValue) {
        if (value != null && !"".equals(value)) {
            try {
                return Integer.parseInt(value);
            } catch (Exception exception) {
            }
        }
        return defaultValue;
    }

    static public String getFormattedElapsedTime(long elapsedTime) {
        return elapsedTime + " secs "
                + ((elapsedTime < 60) ? "" : "(" + round(Double.valueOf(((double) elapsedTime) / 60), 1) + " mins) ");
    }

    static public String getFormattedElapsedTime(double elapsedTime) {
        return elapsedTime + " secs "
                + ((elapsedTime < 60) ? "" : "(" + round(Double.valueOf(elapsedTime / 60), 1) + " mins) ");
    }

    // https://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
    static double round(double value, int places) {
        return new BigDecimal(value).setScale(places, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Get an arg list based on various runner parameters to run either a new or old
     * style gradlew command to run test scripts
     *
     * @param parameters
     * @param providedArgs
     * @param moreArgs
     * @param scriptDirName
     * @param relativeFile
     * @return arg list suitable for running using Exec task or project.exec
     */
    static public List<String> getArgs(final Map<String, Object> parameters, final List<String> providedArgs,
            final List<String> moreArgs, final String scriptDirName, final File relativeFile) {

        List<String> args = providedArgs == null ? new ArrayList<String>() : new ArrayList<String>(providedArgs);

        // Handle case of a build file being used to run the scripts with apply instead
        // of indirectly
        // Use the projectDirectory parameter to point to the directory with the itest
        // build file
        // This covers Gradle 8.0 removal -b option for running arbitrary scripts
        // https://discuss.gradle.org/t/deprecation-of-build-file-for-specifying-a-custom-build-file-location-in-8-0/41244
        Object dirParameter = parameters == null ? null : parameters.get("projectDirectory");
        String projectDirectory = dirParameter instanceof String ? (String) dirParameter : null;
        if (projectDirectory != null) {
            if (!projectDirectory.endsWith(GintUtils2.getFileSeparator())) {
                projectDirectory += GintUtils2.getFileSeparator();
            }
            // modify the script location to be relative to the project directory
            dirParameter = parameters.get("directory");
            String scriptDirectory = dirParameter == null ? "." : dirParameter.toString().replace(projectDirectory, "");

            String script = (scriptDirectory + GintUtils2.getFileSeparator() + relativeFile.getPath());
            script = script.replace(GintUtils2.getFileSeparator() + GintUtils2.getFileSeparator(),
                    GintUtils2.getFileSeparator()); // remove any duplicated separators

            char quote = isWindows() ? '"' : '\''; // single quote on linux to avoid any shell interpretations, windows
                                                   // needs double quote

            args.add("-p");
            args.add(projectDirectory); // --project-dir - directory of the build.script that will apply the scrip,
            args.add("-Dscript=" + (isWindows() ? quoteString(script, quote, quote) : script));
        } else { // old way
            args.add("-b");
            args.add(scriptDirName + GintUtils2.getFileSeparator() + relativeFile.getPath());
        }
        if (moreArgs != null) { // args coming from command line
            args.addAll(moreArgs);
        }
        return args;
    }

    static public String argsToString(final Collection<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String entry : list) {
            builder.append(entry).append(" ");
        }
        return builder.toString();
    }
}
