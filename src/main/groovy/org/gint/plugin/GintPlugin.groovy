/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin

import org.gint.tasks.GintGroupTask
import org.gint.tasks.GintInternalTask
import org.gradle.BuildResult
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.TaskExecutionException

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

/**
 * Gradle plugin definition
 */
@CompileStatic
@TypeChecked
class GintPlugin implements Plugin<Project> {

    @Override
    public void apply(Project project) {

        Gint gint = project.extensions.create('gint', Gint)
        gint.init(project)

        project.task(Constants.TASK_FAILED, type: GintInternalTask, group: Constants.GROUP_BASE, description: Constants.TASK_DESCRIPTION_FAILED)
        Task baseTask = project.task(Constants.TASK_BASE, type: GintGroupTask, group: Constants.GROUP_BASE, description: Constants.TASK_DESCRIPTION_BASE)
        Task allTask = project.task(Constants.TASK_ALL, type: GintGroupTask, group: Constants.GROUP_BASE, description: Constants.TASK_DESCRIPTION_ALL)
        Task setUpTask = project.task(Constants.TASK_SET_UP, type: GintGroupTask, group: Constants.GROUP_SET_UP, description: Constants.TASK_DESCRIPTION_SET_UP)
        Task tearDownTask = project.task(Constants.TASK_TEAR_DOWN, type: GintGroupTask, group: Constants.GROUP_TEAR_DOWN, description: Constants.TASK_DESCRIPTION_TEAR_DOWN)
        Task tearDownAfterTask = project.task(Constants.TASK_TEAR_DOWN_AFTER, type: GintInternalTask, group: Constants.GROUP_TEAR_DOWN, description: Constants.TASK_DESCRIPTION_TEAR_DOWN_AFTER)
        Task initializeTask = project.task(Constants.TASK_INITIALIZE, type: GintInternalTask, group: Constants.GROUP_BASE, description: Constants.TASK_DESCRIPTION_INITIALIZE)
        Task finalizeTask = project.task(Constants.TASK_FINALIZE, type: GintInternalTask, group: Constants.GROUP_BASE, description: Constants.TASK_DESCRIPTION_FINALIZE)

        initializeTask.doFirst({ Task task ->
            //gint.helper.log 'initialize task start time: ', System.currentTimeMillis() // performance debugging
            gint.initializeTask()
            //gint.helper.log 'initialize task end time:   ', System.currentTimeMillis() // performance debugging
        })

        setUpTask.doFirst({ Task task ->
            //gint.logger.info('\n\nxxx  failed: {}', gint.failed )
            //println('\n\nxxx  failed 2: ' + gint.failed )
            //if (gint.setUpFailed) {
            //    throw new TaskExecutionException(task, new Exception('Gint run has failed due to setUp task failures.'))
            //}
        })

        // Try to issue report as soon as all Gint requested tasks are done at least if we get the chance
        finalizeTask.doLast({ Task task ->
            finalThings(gint, task)
        })

        allTask.dependsOn(setUpTask)
        allTask.dependsOn(baseTask)
        allTask.dependsOn(tearDownTask)
        allTask.dependsOn(tearDownAfterTask)

        // Make sure reporting has been completed and if not, complete the reporting
        // This turned out to be a better solution that using finalizedBy which had severe runtime performance problems when associated with each task
        // This happens after BUILD SUCCESSFUL or BUILD FAILED message
        project.gradle.buildFinished { BuildResult buildResult ->
            // println "GINT FINISHED"  // TODO temporary for debugging
            finalThings(gint)
        }

        // this messes up  regular error reporting, so do not use.
        // another approach to manage output is https://stackoverflow.com/questions/36802181/control-logging-output-from-third-party-libraries-used-in-a-gradle-plugin
        // project.getGradle().useLogger(new EventLogger())
    }

    protected finalThings(final Gint gint, final Task task = null) {
        try {
            if (gint.doReporting) {  // reporting not already done by finalizeTask
                boolean result = gint.reportHelper.finalizeReport() // reporting
                if (!result) {
                    gint.logFailedMessages()
                    Exception exception = new Exception('Gint run failed or was incomplete due to initialization, reporting, or other problems logged above.')

                    throw (task == null ? exception : new TaskExecutionException(task, exception))

                    if (task != null) {
                        throw new TaskExecutionException(task, exception)
                    }
                    throw exception
                }
            }
            //gint.reportHelper.logGintTaskGraph()
        } finally {
            try {
                gint.closeSqlConnections()
            } catch (Exception ignore) {
            }
            try {
                gint.closeSeleniumDrivers()
            } catch (Exception ignore) {
            }
        }
    }
}
