/*
 * Copyright (c) 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;

import org.gint.objects.ScriptEntry;
import org.gint.tasks.GintRunnerTask;

public class RunScript extends RecursiveAction {

    protected final GintRunnerTask task; // originating task
    protected final String name;
    protected final ScriptEntry scriptEntry;
    protected final File logDir;
    protected final File workDir;
    protected final Map<String, String> environment;
    protected final String executable;
    protected final List<String> args;
    protected Integer parallelTimeout;

    protected Double elapsedTime; // secs
    protected Integer exitValue;
    protected File tempFile;

    protected boolean isRetry;

    public RunScript(final GintRunnerTask task, final String name, final ScriptEntry scriptEntry, final File logDir,
            final File workDir,
            final Map<String, String> environment, final String executable, final List<String> args,
            final Integer parallelTimeout) {
        this.task = task;
        this.name = name;
        this.scriptEntry = scriptEntry;
        this.logDir = logDir;
        this.workDir = workDir;
        this.environment = environment;
        this.executable = executable;
        this.args = args;
        this.parallelTimeout = parallelTimeout;
    }

    public RunScript cloneForRetry() {
        RunScript entry = new RunScript(task, name, scriptEntry, logDir, workDir, environment, executable, args,
                parallelTimeout);
        isRetry = true;
        return entry;
    }

    @Override
    protected void compute() {

        task.runStarted(isRetry, name, logDir == null ? null : getLogFile(), executable, args);
        try {
            long startTime = System.currentTimeMillis();

            List<String> command = new ArrayList<>();
            command.add(executable);
            command.addAll(args);

            ProcessBuilder builder = new ProcessBuilder(command);
            File output = getOutputFile();
            builder.redirectError(Redirect.appendTo(output));
            builder.redirectOutput(Redirect.appendTo(output));
            builder.redirectErrorStream(true);
            if (workDir != null) {
                builder.directory(workDir);
            }
            if (environment != null) {
                builder.environment().putAll(environment); // add to existing environment
            }
            // System.out.println("command: " + builder.command());
            // System.out.println("workDir: " + builder.directory());

            task.sleep(); // coordinate with other process starts to avoid conflicts
            Process process = builder.start();
            // System.out.println("process: " + process.info().commandLine());
            // System.out.println("process: " + process.info().arguments());
            // System.out.println("process start for " + name);

            if (parallelTimeout == null) {
                parallelTimeout = 60; // TODO: 60 minutes - is this the right limit???
            }
            if (parallelTimeout != null && parallelTimeout > 0) {
                process.waitFor(parallelTimeout, TimeUnit.MINUTES);
                process.destroy();
                process.waitFor(10, TimeUnit.SECONDS); // give it a chance to stop on its own
                process.destroyForcibly();
            }
            process.waitFor();
            exitValue = process.exitValue();
            task.runResult(name, exitValue);
            elapsedTime = GintUtils2.round((double) ((System.currentTimeMillis() - startTime)) / 1000, 1); // in seconds

            // Add each process output to task output BUT only when there is a data
            // comparison requested on the task
            if ((task.getData() != null) || (task.getFailData() != null)) {
                task.getOutData().addAll(org.gint.helpers.Helper.readFileToList(output));
            }

        } catch (Exception e) {
            // Exception exception = new TaskExecutionException(task, e);
            // new Exception(failedCount + " scripts failed out of " + runnerList.size() + "
            // scripts run."));
            if (exitValue == null) {
                exitValue = -1; // added when testing windows
            }
            throw new RuntimeException(e);
        } finally {
            if (tempFile != null) {
                task.joinOutput(tempFile);
                tempFile.delete();
            }
        }
    }

    protected File getOutputFile() throws IOException {
        if (logDir == null) {
            tempFile = Files.createTempFile(name, null).toFile();
            tempFile.deleteOnExit();
            return tempFile;
        } else {
            return getLogFile();
        }
    }

    public boolean hasFailed() {
        return exitValue != null && exitValue != 0;
    }

    public File getLogFile() {
        return new File(logDir, name + ".log");
    }

    public String getName() {
        return name;
    }

    public Double getElapsedTime() {
        return elapsedTime == null ? 0 : elapsedTime;
    }

    public Integer getExitValue() {
        return exitValue;
    }

    public ScriptEntry getScriptEntry() {
        return scriptEntry;
    }
}
