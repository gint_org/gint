/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.plugin

import org.gradle.api.logging.StandardOutputListener

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

/**
 * Customize the output messaging for a script run
 */
@CompileStatic
@TypeChecked
class OutputListener implements StandardOutputListener {

    //protected File file
    protected OutputStream outputStream
    protected List<String> list

    OutputListener(final File file) {
        //this.file = file;
        assert file != null
        outputStream = file.newOutputStream()
    }

    OutputListener(final List<String> list) {
        assert list != null
        this.list = list
    }

    public close() {
        if (outputStream) {
            outputStream.close()
        }
    }

    @Override
    public void onOutput(CharSequence text) {
        if (outputStream) {
            outputStream << text.toString()
        } else {
            list << text.toString()
        }
    }
}
