/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.tasks

import org.gint.helpers.Helper
import org.gint.helpers.ParallelHelper
import org.gint.interfaces.CmdGenerator
import org.gint.plugin.Constants
import org.gint.plugin.GintUtils
import org.gint.plugin.OutputListener
import org.gint.plugin.Constants.TaskType
import org.gradle.api.Task
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode

//import org.gradle.api.tasks.Optional  // not automatically included

/**
 * Gint task. This should be a java bean meaning most fields are private with getters and setters - the default for Groovy code is undeclared are private with getters/setters automatically defined
 */
//@CompileStatic
//@TypeChecked
class GintTask extends GintAbstractTask {

    public TaskType type = TaskType.BASE // default to base task and set later if different

    protected Helper helper
    protected Closure message

    @Input @Optional
    String description
    @Input @Optional
    String gintGroup // different from a gradle group

    @Input @Optional
    Object cmd

    @Input @Optional
    Map closures = new HashMap() // use map to prevent Closures from running during evaluation

    @Input @Optional
    Map map // user defined data

    @Input @Optional
    Object file
    @Input @Optional
    Object expected
    @Input @Optional
    Object level
    @Input @Optional
    Object depends
    @Input @Optional
    Object end
    @Input @Optional
    Object neededBy
    @Input @Optional
    Object ignoreDependsResult
    @Input @Optional
    Object retry
    @Input @Optional
    Object retryData
    @Input @Optional
    Object retryCancelData
    @Input @Optional
    Object data
    @Input @Optional
    Object failData
    @Input @Optional
    Object im
    @Input @Optional
    Object standardInput
    @Input @Optional
    Object setUp
    @Input @Optional
    Object tearDown
    @Input @Optional
    Object compare

    @Input @Optional
    String startNext
    @Input @Optional
    String encoding

    @Internal
    String failReason
    @Internal
    String failDetail

    @Internal
    List<String> outData

    @Internal
    Object output  // TODO - maybe this is an object - for output configuration

    // TODO - compare support

    @Internal
    Object result

    @Internal
    int restartCount = 0

    @Input
    int order = 0 // represents LAST or not set ordering - previously Integer.MAX_VALUE

    @Input
    int autoRetry = 0

    @Input @Optional
    Integer retrySleep
    //@Input @Optional
    //private Integer timeout - gradle 5 supports this
    @Input @Optional
    Integer sleep
    @Input @Optional
    Integer sleepAfter
    @Input @Optional
    Integer patternFlag  // null is significant as indicator to use default of Literal

    @Input
    boolean ignoreFailure = false
    @Input
    boolean complete = false
    @Input
    boolean shell = true
    @Input
    boolean cmdLog = true   //  default to log commands unless specifically requested to not log
    @Input
    boolean isSetUp = false
    @Input
    boolean isTearDown = false

    @Internal
    Boolean restartRequested = false
    @Internal
    boolean failed = false
    @Internal
    boolean skipped = false

    @Internal
    Boolean success
    @Internal
    Boolean compareSuccess
    @Internal
    Boolean found
    @Internal
    Boolean notFound

    @Internal
    ParallelHelper parallel

    protected long startTime
    protected long endTime

    @Input @Optional
    OutputStream standardOutput

    /**
     * Constructor
     */
    GintTask() {
        super()
        helper = gint.helper
        message = gint.message
    }

    /**
     * Set outData for data compare. Override in sub class if necessary.
     * @param task
     */
    protected void resetOutData() {
        if (parallel) {
            outData = parallel.getOutList()
            outData += parallel.getErrorList() // always include error data even if success
        }
    }

    List getOutData() {
        if (outData == null) {
            outData = []
        }
        return outData
    }

    /**
     * Get encoding for reading/writing files.
     * @return encoding from task has a specific one specified, otherwise use default setting. Could be null!
     */
    public String getEncoding() {
        return encoding ?: gint.encoding // use global setting if task specific not set
    }

    @Internal
    public boolean isTearDownTask() {
        return type == TaskType.TEAR_DOWN || type == TaskType.TEAR_DOWN_AFTER
    }

    /**
     * Check any parameter requirements and if not met, disable the task
     * @return true if ok
     */
    //protected boolean validate() {

    // TODO: a gint task can do comparisons without inline or cmd settings
    //        if ((closures.inline == null) || !(closures.inline instanceof Closure)) {
    //            if (helper.isBlank(cmd) && (gint.cmdGenerator == null) && (closures.cmdGenerator == null)) {
    //                gint.addFailedMessage('Task ' + helper.quoteString(name) + ((closures.inline == null) ? //
    //                                ' does not define cmd or inline code' //
    //                                : ('defines inline but it is not a closure (' + closures.inline.class.name) + ')') //
    //                                )
    //                enabled = false
    //            }
    //        }
    //    return enabled
    //}

    @TaskAction
    def action() {

        try {
            startProcessing(true)
        } catch (Exception exception) {   // don't let exceptions get out of our control
            message 'error', "Unexpected exception starting task ${name}. Use --stacktrace to get debug information. " + exception
            throw new TaskExecutionException(this, exception)
        }
        if (!skipped) {  // Gint determined skip like due to level calculation
            try {
                endProcessing(false)

                gint.failed = gint.failed || treatAsFailed()
                // helper.log('gint failed', gint.failed)

            } catch (Exception exception) {   // don't let exceptions get out of our control
                endTime = System.currentTimeMillis()
                gint.printStackTrace(exception) // if requested
                gint.addFailedMessage("Exception running task ${name}. ${exception}" )
                failed = true
                success = false
                return true   // no restart
            }
            if (treatAsFailed()) {
                def message = failReason ?: 'Gint determined the task was not successful.'
                throw new TaskExecutionException(this, new Exception(message))
            }
        }
        // debug('After completion')
    }

    public boolean treatAsFailed() {
        return !ignoreFailure && (failed == true || success == false)
    }

    /**
     * Start the task process or thread running.  Make sure any depends tasks have completed.
     * Note that all depends tasks/targets should have at least been started due to the GANT dependency mechanism,
     * we just need to worry about them being completed.
     * @param task
     * @param startMessage - true to log a start message
     * @param isTearDown - is to be considered a tearDown task
     */
    protected void startProcessing(final boolean startMessage, final boolean isTearDown = false) {
        // must wait until last minute to determine whether task is intended to be skipped to allow dependencies to finish

        skipped = gint.stopNow || isSkipped(level) || dependsOnTaskSkipped()

        if (skipped) {
            handleSkipTask()
        } else {
            if (sleep instanceof Integer) {
                sleepRequest(sleep, name)
            }
            if (closures.inline == null) {
                // handles conversion of cmd generator and closures to cmd string, do this as late as possible for variable resolution from other task closures
                updateCmdToString() // sets task.cmd to string value if needed
            }

            if (startMessage) {
                gint.reportHelper.taskMessage 'start', name + (description ? ' (' + description + ')' : '') + (isTearDown ? ' as tearDown' : '')
            }

            if ((cmd != null) && (cmdLog != false) && gint.reportHelper.cmdLog && (isExpectedResult(0, expected))) { // only log commands expected to be successful
                gint.reportHelper.addCmdLog(cmd)
            }

            // TODO: im processing removed
            //if ((im == null) || getGintIm().getResult(task)) { // before IM
            startTime = System.currentTimeMillis()

            boolean shouldRun = true

            if (closures.start) {
                def result = gint.closureHelper.runClosure(closures.start, this, 'startResult', 'startResult') // stop run only if start closure returns false
                shouldRun = result != false
                // helper.logWithFormat('task', task)
                if ([
                    Constants.FAIL_TIMEOUT,
                    Constants.FAIL_ERROR,
                    Constants.FAIL_ASSERTION,
                    Constants.FAIL_EXCEPTION,
                ].contains(closures.startResult)) {
                    //gint.failed = true  // error message already reported, this will error out the entire test
                    failed = true
                }
            }
            if (shouldRun) {

                // Run inline code or start the command
                if (this instanceof GintRunnerTask) {
                    ((GintRunnerTask)this).startParallel()
                } else if (closures.inline instanceof Closure) {
                    def outputListener = new OutputListener(getOutData()) // capture inline closure output to outData for later comparison
                    logging.addStandardOutputListener(outputListener)     // start capture
                    logging.addStandardErrorListener(outputListener)
                    gint.closureHelper.handleInline(this)
                    logging.removeStandardOutputListener(outputListener)  // end capture
                    logging.removeStandardErrorListener(outputListener)
                    //gint.helper.log('outData', outData)  // note output data gets deleted after processing
                } else {
                    def parameters = [
                        standardInput: gint.helper.getValueHandleClosure(standardInput),
                    ] + (map?.parameters instanceof Map ? map.parameters : [:])
                    parallel = (shell == false)
                        ? gint.parallelHelper.executable(parameters, cmd)
                        : gint.parallelHelper.shell(parameters, cmd)
                }
                //if (complete == true) {  // need to end this one before starting others
                //    endTaskHandleRestart(task)
                //}
            } else {
                level = false
                handleSkipTask(true, true)
            }

            // TODO - more IM processing
            //} else if (task.imResult?.skip == true) {
            //    handleSkiptask(task, false, true)
            //} else {  // im before says it should be considered failed
            //gettasksRun() << task   // marks this as completed for summary
            //success = false
            //result = -1
            //if (!isTearDown) { //  || (task.ignoreFailure == false)) {
            //    handleResultMessage(task)
            //} else if (!task.success && getVerbose()) {
            //    message 'ignore', "${task.name} failed - ignored for tearDown or if ignoreFailure requested"
            //}
            //}
        }
    }

    /**
     * Get the command to use for a task. Use explicit task cmd if provided, otherwise generate it from the command generator.
     * Only evaluate once, so save result in task.cmd for use later.
     * @param task
     * @return cmd string or null
     */
    protected void updateCmdToString() {
        if (cmd == null) {
            cmd = ((closures.inline != null) ? null : // inline always takes precedence
                            // ((closures.cmdGenerator) ? ((CmdGenerator)closures.cmdGenerator).callback(this) :
                            ((closures.cmdGenerator instanceof CmdGenerator) ? ((CmdGenerator) closures.cmdGenerator).callback(this) :
                            (gint.cmdGenerator ? gint.cmdGenerator.callback(this) : null)))
        } else if (cmd instanceof Closure) {
            Closure cmdAsClosure = (Closure) cmd
            cmd = (cmdAsClosure.getMaximumNumberOfParameters() > 0) ? cmdAsClosure(this) : cmdAsClosure()
        }
    }

    /**
     * Handle skipping a task - for reporting later
     */
    protected void handleSkipTask(final reportSkipLevel = true, final showSkipMessage = false) {
        def skipLevel = convertLevel(level)
        gint.tasksSkipped << this

        if (reportSkipLevel) {
            gint.skippedLevels << skipLevel
        }
        //}
        if (showSkipMessage || gint.getVerbose()) {
            gint.reportHelper.taskMessage 'info', ("Skip task: " + name //
                            + (reportSkipLevel ? ", level: " + skipLevel : '') //
                            + (closures.startResult == null ? '' : ', start closure result: ' + closures.startResult))
        }
    }

    /**
     * Test to see if this task is to be skipped (not run)
     * <pre>
     * - excludeLevels takes precedence over other values, ie if match exclude, then skip
     * - includeLevels takes precedence over integer levels except for all case, then integer level is still used
     * - for collections, if all entries are to be skipped, then skip otherwise include
     * </pre>
     * @param inLevel - level to check, usually task.level
     * @return true if task is intended to be skipped
     */
    public boolean isSkipped(final inLevel) {

        boolean result = null
        def level = convertLevel(inLevel)

        if (level instanceof Collection) {
            result = true
            for (entry in level) {  // use for so we can do quick return
                if (!isSkipped(entry)) {
                    result = false  // return on first none skipped entry
                    break
                }
            }
        } else if (gint.excludeLevels?.contains(level)) {
            result = true
        } else if (gint.includeLevels instanceof Collection) {
            if (level instanceof Integer) {
                result = ((int)level > gint.level) && !gint.includeLevels?.contains(level)
            } else {
                result = !(gint.includeLevels?.contains(level) || ((gint.includeLevels?.size() == 1) && gint.includeLevels?.contains(Constants.INCLUDE_ALL)))
            }
        } else if (level instanceof Integer) {
            result = ((int)level > gint.level)
        } else {
            result = false // null include levels list means include ALL
        }
        //helper.log 'inLevel', inLevel
        //helper.log 'level', level
        //helper.log 'level class', level.class.name
        //helper.log 'isSkipped', result
        return result
    }

    /**
     * dependsOn tasks should be run before this task and they may determine themselves to be skipped.
     * This checks that and skips this task if at least one depends on task is skipped according to Gint processing
     * @return true if at least one depends on task was skipped by Gint processing
     */
    protected boolean dependsOnTaskSkipped() {

        boolean result = false
        for (Task entry : GintUtils.transitiveTaskDependencies(this)) {
            if (entry instanceof GintTask) {
                def task = (GintTask) entry
                //task.debug('Determine skipped for gint task: ' + name)

                result = !task.isTearDownTask() && (task.skipped || task.state.skipped)
                //helper.log('dependsOnTaskSkipped ' + task.name, result)
                if (result) {
                    // TODO improve this message
                    message 'info', this.name + ' skipped because it depends on skipped task ' + task.name
                    break
                }
            }
        }
        //helper.log 'dependsOnTaskSkipped', result
        return result
    }

    /**
     * Calculate the actual level from the input level. In particular convert null to 1 and evaluate closures.
     * @param level to convert, default the task level, separate parameter needed for recursion
     * @return
     */
    protected convertLevel(inLevel) {
        try {
            //helper.log('level', level)
            if ((inLevel == null) && (closures.level instanceof Closure)) {
                inLevel = closures.level
            }
            return (inLevel == null) ? 1 :
                            (inLevel instanceof Closure ?
                            ((inLevel.getMaximumNumberOfParameters() > 0) ? inLevel(this) : inLevel()) : inLevel)
        } catch (Exception exception) {
            gint.reportHelper.taskMessage 'warning', "Exception in level closure ignored. " + exception.toString()  // TODO log with level warn ?
            return 1  //  default level TODO make constant
        }
    }

    /**
     * End the task processing and check all provided conditions:
     * - command result matches expected
     * - data is found in output
     * - failData is NOT found in output
     * - resultClosure returns true
     * @param request ignore failure - force ignore failure on call, used for task specific tearDown actions
     * @return true if task was ended or false if task had to be restarted
     */
    protected boolean endProcessing(boolean requestIgnoreFailure) {

        if (parallel != null) {
            result = parallel.end(false)
            //logger.quiet('result: {}', this.result)
            parallel.printOutList()
            parallel.printErrorList()
        } else if (this instanceof GintRunnerTask) {
            boolean success = ((GintRunnerTask)this).endParallel()
            result = success ? 0 : -1
        }

        resetOutData()  // make sure this is set before the result closure is called!

        // Now a chance for task specific processing to update/change the result
        if (closures.result != null) {
            gint.closureHelper.runClosure(closures.result, this, "resultResult", "resultResult")  // task result is updated with closure result or closure exception info
            result = closures.resultResult ?: 0  // map null or false result to 0
        }

        // handle both windows and non-windows result codes- windows uses -1, -2, etc... that gets converted to positive number on unix via 256 + number.
        //success = ((result == expected) || (result == (256 + expected)))
        success = isExpectedResult(result, expected, name)

        def restartNeeded = false
        //if (!ignoreFailure) { // skip this for tearDown operations - don't mess up data for valid runs of the task
        if (!requestIgnoreFailure) {  // determine outcome for all runs except task specific teardown requests
            determineOutcome()
            restartNeeded = restartRequested
        }
        endTime = System.currentTimeMillis()

        boolean ignoreFailure = requestIgnoreFailure || (isTearDown == true)
        boolean ignoreFailure2 = ignoreFailure || (ignoreFailure == true)

        if (restartNeeded) {
            // Log error information before attempting restart processing
            logOutputAfterEnd(ignoreFailure) // opportunity to log task output based on parameters or other factors
        }

        if (!requestIgnoreFailure || (ignoreFailure != true) || (isTearDown != true)) {
            gint.reportHelper.handleResultMessage(this, ignoreFailure2 || restartNeeded)
        } else if (!success && gint.getVerbose()) {
            gint.reportHelper.taskMessage 'ignore', "${name} failed - ignored for tearDown or if ignoreFailure requested"
        }

        if (restartNeeded == true) {  // restart and don't do any more processing here
            restartTask()
        } else {
            // TODO:
            //            logOutputAfterEnd(task, ignoreFailure) // opportunity to log task output based on parameters or other factors
            //            if (!requestIgnoreFailure || (ignoreFailure != true) || (isTearDown != true)) {
            //                handleResultMessage(task, ignoreFailure2)
            //            } else if (!success && getVerbose()) {
            //                message 'ignore', "${name} failed - ignored for tearDown or if ignoreFailure requested"
            //            }
            // final closure is run after the task results have been calculated and reported
            // Note that final closure may run for tearDown, but no task result data is available
            if ((closures.final != null) && (!gint.closureHelper.runClosure(closures.final, this))) {
                gint.failed = true // problems with finalClosures failing must be reported as global test failures as they occur after task has completed
                return true // final closure experienced an exception - end no matter what
            }
            failed = !success  // indicator that remembers failure status, even though we may be ignoring that

            if (failed && this.ignoreFailure) {  // Don't use local ignoreFailure variable! TODO: rename that variable
                if (type == TaskType.BASE) {
                    gint.reportHelper.quarantinedBaseTasks << this.name
                } else if (type == TaskType.SET_UP) {
                    gint.reportHelper.quarantinedSetUpTasks << this.name
                }
                success = true  // mark as success even if it failed - needed at least for dependency handling
            }
            gint.reportHelper.taskMessage 'complete', name + ' (' + gint.name + ') - ' + (endTime - startTime)/1000 + ' secs' + Constants.EOL // leave space after
            parallel = null // free memory
            outData = null // free memory

            if (sleepAfter instanceof Integer) {
                sleepRequest(sleepAfter, name)
            }

            // TODO - probably not needed - gradle probably has a method for doing this
            //            if (startNext != null) { // TODO just handle simple case to start
            //                def nextTask = findTask(startNext)
            //                if (nextTask != null) { // found
            //                    if (okToRun(nextTask)) {
            //                        // only start if not already run or started
            //                        runtask(nexttask)
            //                    }
            //                }
            //            }
        }
        return !restartNeeded
    }

    /**
     * Restart a task - handle restart requested TODO - check looping conditions and other data that needs to be cleared
     */
    protected void restartTask() {
        //if (getVerbose()) {
        message 'info', "Restarting task ${name}."
        //}
        def resetList = [
            'restartRequested',
            'success',
            'failReason',
            //'imResult',
            //'output', // this is user provided data and should NOT be cleared - see GINT-40
            'outData',
            'parallel',
            //'thread' // not used in gint3
        ]
        resetList.each { key ->
            //println 'reset key: ' + key
            this[key] = null
        }
        // sleep before running it again, default to 1 sec for non im tasks
        sleepRequest(retrySleep instanceof Integer ? retrySleep : im ? 0 : 1000, name)

        startProcessing(true)
        endProcessing(false)
    }

    /**
     * Sleep for specified length and handle logging.
     * @param length
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected void sleepRequest(final int sleepTime, final name) {
        if (sleepTime > 0) {
            if (gint.getVerbose()) {
                message 'info', sleepTime + ' milliseconds sleep requested by task ' + name
            }
            org.codehaus.groovy.runtime.DefaultGroovyStaticMethods.sleep(sleepTime) // avoid conflict with task.sleep field
        }
    }

    /**
     * Task has ended and is about to report outcome. This is an opportunity to decide to log additional information.
     * Default is to log output from parallel task if verbose is set
     * @param ignoreFailure - ignore failure setting
     */
    protected void logOutputAfterEnd(final boolean ignoreFailure = false) {
        if (gint.getVerbose() || !(success || ignoreFailure)) {
            if (parallel) {
                parallel.printOutList()
                parallel.printErrorList()
            }
        }
    }

    /**
     * Does the result match the expected task result(s)?  Support recursion for collections, strings.
     * - Handle both windows and non-windows result codes- windows uses -1, -2, etc... that gets converted to positive number on unix via 256 + number.
     * - expected null matches to 0
     * - boolean expected: true maps to 0, false to -1
     * - strings are converted to ints
     * @param expected (usually task.expected)
     * @param result
     * @param name - task name, used for diagnostic message only
     * @return true if result match based on standard behavior
     */
    protected boolean isExpectedResult(final result, expected, final name = null) {
        try {
            if ((expected == null) && (closures.expected instanceof Closure)) {
                expected = closures.expected
            }
            //helper.log('result', result)
            //helper.log('expected', expected)

            return (result == expected) ||
                            ((expected == null) ? (result == 0)
                            : ((expected instanceof Boolean) ? ((expected && (result == 0)) || (!expected && ((result == -1) || (result == 255))))
                            : ((expected instanceof Integer) ? (result == (256 + (int) expected))
                            : ((expected instanceof Collection) ? isExpectedResultWithCollection(result, expected, name)
                            : ((expected instanceof String) || (expected instanceof GString)) ? isExpectedResultWithString(result, expected, name)
                            : ((expected instanceof Closure) ? expected(result)
                            : false
                            )))))
        } catch (Exception exception) {
            if (gint.getVerbose()) {
                gint.reportHelper.taskMessage 'warning', "Exception in expected closure ignored. 0 will be used. " + exception.toString()
            }
            return (result == 0)
        }
    }

    /**
     * Expected value is a string or GString, need to convert to integer
     * @param result
     * @param expected
     * @return true if result match the converted value or matches 0 if expected is invalid
     */
    protected boolean isExpectedResultWithString(final Integer result, final expected, final name = null) {
        try {
            def expectedAsInt = Integer.parseInt(expected)
            return isExpectedResult(result, expectedAsInt)
        } catch (NumberFormatException exception) {
            if (name != null) {
                gint.reportHelper.taskMessage 'warning', "Invalid expected value of '${expected}' for task ${name}. 0 will be used."
            }
            return (result == 0)
        }
    }

    /**
     * Expected value is a collection, need to match on ANY element of collection - support recursion
     * @param result
     * @param expected
     * @return true if result match the converted value or matches 0 if expected is invalid
     */
    protected boolean isExpectedResultWithCollection(final Integer result, final Collection list, final name = null) {
        for (expected in list) {  // use for loop for early return capability
            if (isExpectedResult(result, expected, name)) {
                return true
            }
        }
        return false
    }

    /**
     * Make sure expected value is converted to an integer
     * - boolean false to -1
     * - string value to integer it represents, -1 if not valid
     * @param expected - current expected value
     * @param name - task name if available for diagnostic message
     * @return converted expect value
     */
    protected int convertExpectedToInteger(final expected, final name = null) {

        def int result = 0  // default if nothing else is valid
        if (expected == null) {
            // default 0
        } else if (expected instanceof Integer) {
            result = expected
        } else if ((expected instanceof String) || (expected instanceof GString)) {
            try {
                result = Integer.parseInt(expected)
            } catch (NumberFormatException exception) {
                message 'warning', "Invalid expected value of '${expected}' for task ${name}. 0 will be used."
            }
        } else if (expected instanceof Boolean) {
            result = expected ? 0 : -1  // convert false to -1 (fail)
        } else {
            gint.reportHelper.taskMessage 'warning', "Invalid expected value of '${expected}'" + (name ? " for task ${name}" : '') + ". 0 will be used."
        }
        return result
    }


    /**
     * For a task that has been ended, do all the calculation to determine the outcome.
     * Set appropriate global and task fields
     * @param task
     */
    //@CompileStatic(TypeCheckingMode.SKIP)
    protected void determineOutcome() {

        // look for expected data
        String outDataAsString = null
        def patternFlag = ((patternFlag != null) ? patternFlag : getPatternFlag())
        if (success && data) {
            // enable multi-line pattern matching
            outDataAsString = helper.listToSeparatedString(outData, System.properties.'line.separator')
            found = helper.verifySearchInTarget(search: data, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: this)
            success = found
        }

        // look for fail data
        if (success && failData) {
            if (outDataAsString == null) {
                outDataAsString = helper.listToSeparatedString(outData, System.properties.'line.separator')
            }
            notFound = helper.verifySearchNotInTarget(search: failData, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: this)
            success = notFound
        }

        // look in generated output for data or fail data, if a file name is not specified, use the default file name based on task name
        if (success) {
            if (output instanceof Map) {
                handleOutputMap(output)
            } else if (output instanceof List) { // check each file contains the correct content
                output.each { map ->
                    if (success && (map instanceof Map)) {
                        handleOutputMap(map)
                    }
                }
            }
        }

        // TODO
        // compare requested?
        //        if (success && ((compare instanceof Map) || (compare == true))) {
        //            compareSuccess = runDiff(task)   // run file compares
        //            success = compareSuccess
        //        }
        //        // image compare requested?
        //        if (success && ((imageCompare instanceof Map) || (imageCompare == true))) {
        //            compareSuccess = runImageCompare(task)   // run image compares
        //            success = compareSuccess
        //        }

        boolean doRetry = false
        // im response? TODO
        //        if (im) {
        //            def result = getGintIm().getResult(task, false)  // after running task
        //            if (restartRequested == true) {
        //                return // don't do anything more here!!!
        //            }
        //            success = success && result
        //
        //            // retry processing only if not im - just to simplify it a bit
        //        } else
        if (isRetryConfigured()) {
            // task was not successful or it is successful but the expected result is not correct
            if (!success || !isExpectedResult(result, expected)) {
                doRetry = shouldDoRetry(outDataAsString)
            }
        }
        if (!doRetry) {
            // if successful so far, run the successClosure if it has one
            if (success && (closures.success != null)) {
                gint.closureHelper.runClosure(closures.success, this, "successResult")
                closures.successResult = closures.successResult ?: false // map null to false so it will be reported properly
                success = closures.successResult

                // Check if we need a retry on success closure failure
                if (!success && isRetryConfigured()) {
                    doRetry = shouldDoRetry(outDataAsString)
                }
            }
        }
        //        if (!doRetry) {
        //            // test fails if any non-ignored test fails for whatever reason
        //            if (!success) {
        //                checkStopConditions()
        //            }
        //            //message 'debug', "complete determineResults for task: ${name}"
        //        }
    }

    protected boolean handleOutputMap(final map) {
        assert map instanceof Map
        if (map.file == null) {
            map.file = (file instanceof String || file instanceof GString) ? file : gint.getOutputFile(name)
        }
        if ((map.file instanceof String) || (map.file instanceof GString)) {
            map.exists = new File(map.file).exists()
            success = map.exists
        }
        if (success) {
            map.found = true  // initialize to make later result message comparison simpler
            map.notFound = true
            def patternFlag2 = ((map.patternFlag != null) ? map.patternFlag : patternFlag)
            def fileAsString = null
            if (map.data) {
                if (fileAsString == null) {
                    fileAsString = readFile(map.file)
                }
                map.found = helper.verifySearchInTarget(search: map.data, target: fileAsString, patternFlag: patternFlag2, logFailure: true, firstParameter: this)
                success = map.found
            }
            if (success && map.failData) {
                if (fileAsString == null) {
                    fileAsString = readFile(map.file)
                }
                map.notFound = helper.verifySearchNotInTarget(search: map.failData, target: fileAsString, patternFlag: patternFlag2, logFailure: true, firstParameter: this)
                success = map.notFound
            }
        }
    }

    /**
     * See if retry has been configured for the  Set retry count if not set
     * @return true if task indicates a retry
     */
    @Internal
    protected boolean isRetryConfigured() {
        if (retry instanceof Integer) {
            // retry already set
        } else if (retry instanceof Boolean) {
            retry = retry == true ? 1 : 0
        } else if ((closures.retry instanceof Closure) || (retrySleep != null) || (retryData != null) || (retryCancelData != null)) {
            retry = 1
        } else if (autoRetry > 0) {
            retry = autoRetry
        } else {
            return false // do nothing if none of the above were specified
        }
        return (int)retry > 0
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    protected shouldDoRetry(String outDataAsString) {
        boolean doRetry = retryData == null
        if (!doRetry) {
            // check to see if we should do the retry based on retryData (just like matching on data for result)
            // all retryData must be found
            if (outDataAsString == null) {
                outDataAsString = helper.listToSeparatedString(outData, System.properties.'line.separator')
            }
            doRetry = helper.verifySearchInTarget(search: retryData, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: this)
            if (!doRetry && gint.getVerbose()) {
                message 'info', 'Retry not done since retryData was not found in output.'
                helper.log('retryData', retryData)
                //helper.logWithFormat('outDataAsString', outDataAsString)
            }
        }
        if (doRetry && (retryCancelData != null)) {
            // check to see if we should do cancel retry based on retryCancelData (just like matching on failData for result)
            // any retryCancelData found will cancel the retry
            if (outDataAsString == null) {
                outDataAsString = helper.listToSeparatedString(outData, System.properties.'line.separator')
            }
            doRetry = helper.verifySearchNotInTarget(search: retryCancelData, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: this)
            if (!doRetry && gint.getVerbose()) {
                message 'info', 'Retry not done since at least one retryCancelData entry was found in output.'
                helper.log('retryCancelData', retryCancelData)
            }
        }
        if (doRetry) {  // still doing retry, give retry closure a chance to change things before retry
            if (closures.retry instanceof Closure) {
                gint.closureHelper.runClosure(closures.retry, this, "retryResult") // task closures.retryResult is updated with closure result
                closures.retryResult = closures.retryResult ?: false // map null to false so it will be reported properly
                doRetry = closures.retryResult == true // only continue retry processing if it returns true
                if (!doRetry && gint.getVerbose()) {
                    message 'info', 'Retry not done since retry closure did not return true.'
                }
            }
        }
        restartRequested = false // make sure this is set correctly
        if (doRetry) { // match on data condition
            restartRequested = retry instanceof Integer && (restartCount < (Integer) retry)
            restartCount++
        }
        return restartRequested == true
    }

    /**
     * Read a file respecting task and global encoding setting
     * @param file
     * @param task
     * @return file contents or output.file contents or ''
     */
    public String readFile(final file = null) {
        if (file == null) {  // default to the output file if it exists
            return (output?.file == null) ? '' : helper.readFile(output.file, gint.getEncoding())
        }
        return helper.readFile(file, gint.getEncoding())
    }

    /**
     * Read a file as list of lines respecting task and global encoding setting
     * @return list of lines
     */
    public List<String> readFileToList(final file = null) {
        if (file == null) {  // default to the output file if it exists
            return (output.file == null) ? []: helper.readFileToList(output.file, getEncoding())
        }
        return helper.readFileToList(file, getEncoding())
    }

    public synchronized void joinOutput(final File file) {
        try (FileInputStream stream = new FileInputStream(file)) {
            byte[] buffer = new byte[4096];
            while (true) {
                int bytesRead = stream.read(buffer);
                if (bytesRead == -1) {
                    break;
                }
                System.out.write(buffer, 0, bytesRead);
            }
            System.out.flush();
        } catch (Exception e) {
            System.out.println("Unexpected exception joining data from parallel activity. " + e);
        }
    }

    public void debug(text = '') {
        if (text) {
            helper.log text, ''
        }
        helper.log name, [
            map:     map,
            success: success,
            failed:  failed,
            state:   [skipped: state.skipped, didWork: state.didWork, failure: state.failure]
        ]
    }

    public void debugFull() {
        helper.log('task.' + name, this)
    }
}
