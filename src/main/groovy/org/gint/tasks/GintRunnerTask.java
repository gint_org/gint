/*
 * Copyright (c) 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.tasks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import org.gint.objects.ScriptEntry;
import org.gint.plugin.GintUtils2;
import org.gint.plugin.RunScript;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.Optional;

// Note on synchronizing output for this task - task message output and log output are synchronized
// - see https://stackoverflow.com/questions/15438727/if-i-synchronized-two-methods-on-the-same-class-can-they-run-simultaneously#:~:text=First%2C%20it%20is%20not%20possible,is%20done%20with%20the%20object.
//   First,it is not possible for two invocations of synchronized methods on the same object to interleave.
//   When one thread is executing a synchronized method for an object,all
//   until the first thread is done with the object.

// Following tutorial from https://docs.gradle.org/current/userguide/worker_api.html
abstract public class GintRunnerTask extends GintTask { // editor only flags this error as GintTask is groovy class

    @Input
    @Optional
    public Map<String, Object> parameters;

    @Input
    @Optional
    public List<String> providedArgs;

    @InputDirectory
    @Optional
    public File scriptDir;

    @Input
    @Optional
    public String scriptDirName;

    @Input
    @Optional
    public List<String> moreArgs;

    @InputDirectory
    @Optional
    public File logDir;

    @Input
    public List<ScriptEntry> scriptList;

    @Input
    @Optional
    public Integer workerCount; // null to let us determine best value

    @Input
    @Optional
    public Integer parallelTimeout; // maximum time in minutes a single test is allowed to run, default 60 minutes

    // Note: decided to not relate pool size to processors as memory is a more
    // important factor in our case because of gradle daemon start overhead and
    // memory use. Need to minimize daemons used and still get parallel activity.
    // If there is a memory problem daemons will shutdown ending the script run.
    // https://www.baeldung.com/java-fork-join
    protected int DEFAULT_MIN_WORKERS = 2;
    protected int DEFAULT_MAX_WORKERS = 6; // customizable by GINT_MAX_WORKERS environment variable

    public void startParallel() {
    }

    public boolean endParallel() {

        determineWorkerCount(); // based on input and defaulting
        taskMessage("info", "Starting parallel script running with worker pool size " + workerCount);
        ForkJoinPool pool = new ForkJoinPool(workerCount);

        List<RunScript> runnerList = new ArrayList<>();

        Object value = parameters == null ? null : parameters.get("executable");
        String executable = value instanceof String ? (String) value
                : (GintUtils2.isWindows() ? ".\\gradlew.bat" : "./gradlew");
        value = parameters == null ? null : parameters.get("workingDir");
        File workingDir = value instanceof File ? (File) value : null;
        value = parameters == null ? null : parameters.get("environment");
        @SuppressWarnings("unchecked")
        Map<String, String> environment = (value instanceof Map ? (Map) value : null);

        // System.out.println("current dir: " + GintUtils2.getCurrentDirectory());
        // System.out.println("working dir: " + workingDir);
        // System.out.println("\n\nlist size: " + scriptList.size());
        // System.out.println("max parallel: " + pool.getParallelism());

        // Start runners
        for (ScriptEntry entry : scriptList) {

            File relativeFile;
            try { // worked ok without the script list addition, but, probably was an exposure
                relativeFile = getScriptDir().toPath().relativize(entry.getFile().toPath()).toFile(); // nio path
            } catch (IllegalArgumentException exception) {
                relativeFile = entry.getFile();
            }
            String name = entry.getName().replace(".gradle", "");

            List<String> args = GintUtils2.getArgs(parameters, providedArgs, moreArgs, getScriptDirName(),
                    relativeFile);
            // Add the list entry specific arguments
            if (entry.getArgs() != null) {
                args.addAll(entry.getArgs());
            }
            // taskMessage("debug", "debug args: " + GintUtils2.argsToString(args));

            RunScript runner = new RunScript(this, name, entry, logDir, workingDir, environment, executable, args,
                    parallelTimeout);
            runnerList.add(runner);
            pool.execute(runner);
        }

        boolean doRetry = System.getenv("GINT_RETRY_FAILED") != null;
        List<RunScript> failedList = new ArrayList<>();

        // Collect results
        boolean success = collectResults(pool, runnerList, failedList, doRetry);

        if (doRetry && !failedList.isEmpty()) {
            taskMessage("info",
                    "Retrying " + failedList.size() + " failed scripts out of " + runnerList.size() + " scripts");

            // Collect results from retries
            List<RunScript> stillFailedList = new ArrayList<>();
            success = collectResults(pool, failedList, stillFailedList, false); // no retry this time

            taskMessage("info", "Retry summary:");
            for (RunScript entry : failedList) {
                if (entry.hasFailed()) {
                    taskMessage("error", "Failed retry for " + entry.getName() + " in "
                            + GintUtils2.getFormattedElapsedTime(entry.getElapsedTime()));
                } else {
                    taskMessage("info", "Successful retry for " + entry.getName() + " in "
                            + GintUtils2.getFormattedElapsedTime(entry.getElapsedTime()));
                }
            }
            failedList = stillFailedList;
        }
        for (RunScript entry : failedList) {
            taskMessage("error",
                    entry.getName() + " failed in " + GintUtils2.getFormattedElapsedTime(entry.getElapsedTime()));
        }

        String message;
        if (success) {
            message = runnerList.size() + " scripts completed successfully";
            taskMessage("complete", message);
        } else {
            message = failedList.size() + " scripts failed out of " + runnerList.size() + " scripts run";
            setFailReason(message);
        }
        return success;
    }

    /**
     * If workerCount not specifically set, then try to get a good value based on
     * scriptList size
     */
    protected void determineWorkerCount() {
        if ((workerCount == null) || (workerCount < 1)) { // if not set or incorrectly set
            workerCount = DEFAULT_MIN_WORKERS;
            int increment = scriptList.size() / 10; // increment 1 for every 10 scripts over 10
            workerCount += increment;
            int maxWorkerCount = GintUtils2.getInteger(System.getenv("GINT_MAX_PARALLEL"), DEFAULT_MAX_WORKERS);
            if (workerCount > maxWorkerCount) {
                workerCount = maxWorkerCount;
            }
        }
    }

    /**
     * Collect results from the run list.
     */
    protected boolean collectResults(final ForkJoinPool pool, final List<RunScript> list,
            final List<RunScript> failedList, final boolean doRetry) {
        // System.out.println("\nRetry failed: " + retryFailed);
        boolean success = true;
        for (RunScript entry : list) {
            entry.quietlyJoin(); // blocks until done

            success = success && !entry.hasFailed();

            String message = entry.getName();
            if (logDir != null) {
                message += " with log: " + entry.getLogFile().getAbsolutePath();
            }
            message += " in " + GintUtils2.getFormattedElapsedTime(entry.getElapsedTime());

            taskMessage(entry.hasFailed() ? "error" : "complete", message);

            // check for failure and run it again as soon as we discover a failure
            // not ideal if it is one of the last ones in list, but logic is simple this way
            if (entry.hasFailed()) {
                if (doRetry) {
                    RunScript clone = entry.cloneForRetry();
                    failedList.add(clone);
                    pool.execute(clone);
                } else {
                    failedList.add(entry);
                }
            }
        }
        return success;
    }

    public synchronized void taskMessage(final String tag, final String message) {
        reportHelper.taskMessage(tag, message); // editor shows error but it is ok
    }

    /**
     * Only do message for first run
     */
    public synchronized void runStarted(final boolean isRetry, final String name, final File logFile,
            final String executable, final List<String> args) {
        taskMessage("info", (isRetry ? "Retry" : "Start") + " running " + name
                + (logFile != null ? " with logging to " + logFile.getAbsolutePath() : ""));
        taskMessage("info", "Args: " + executable + " " + GintUtils2.argsToString(args));
    }

    public synchronized void runResult(final String name, final Integer exitValue) {
        taskMessage(exitValue != null && exitValue == 0 ? "info" : "error",
                "Run script " + name + " completed with exit code " + exitValue);
    }

    /**
     * Attempt to control the timing of Gradle daemons starting to avoid:
     * - Gradle build daemon disappeared unexpectedly
     * By synchronizing the starting of new processes with a 1 second delay.
     * A 1 second delay plus added memory helped but did not eliminate the error.
     * Likely host memory problems.
     */
    public synchronized void sleep() {
        GintUtils2.sleep(1000);
    }

    public List<ScriptEntry> getScriptList() {
        return scriptList;
    }

    public void setScriptList(final List<ScriptEntry> scriptList) {
        this.scriptList = scriptList;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public List<String> getProvidedArgs() {
        return providedArgs;
    }

    public void setProvidedArgs(List<String> providedArgs) {
        this.providedArgs = providedArgs;
    }

    public File getScriptDir() {
        return scriptDir == null ? new File(getScriptDirName()) : scriptDir;
    }

    public void setScriptDir(File scriptDir) {
        this.scriptDir = scriptDir;
    }

    public String getScriptDirName() {
        return scriptDirName != null ? scriptDirName : scriptDir != null ? scriptDir.getName() : ".";
    }

    public void setScriptDirName(String scriptDirName) {
        this.scriptDirName = scriptDirName;
    }

    public File getLogDir() {
        return logDir;
    }

    public void setLogDir(File logDir) {
        this.logDir = logDir;
    }

    public List<String> getMoreArgs() {
        return moreArgs;
    }

    public void setMoreArgs(List<String> moreArgs) {
        this.moreArgs = moreArgs;
    }

    public Integer getWorkerCount() {
        return workerCount;
    }

    public void setWorkerCount(Integer workerCount) {
        this.workerCount = workerCount;
    }

    public Integer getParallelTimeout() {
        return parallelTimeout;
    }

    public void setParallelTimeout(Integer parallelTimeout) {
        this.parallelTimeout = parallelTimeout;
    }

}
