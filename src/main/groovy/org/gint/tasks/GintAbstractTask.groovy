/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.tasks

import org.gint.helpers.ReportHelper
import org.gint.plugin.Gint
import org.gradle.api.DefaultTask

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked


/**
 * Gint task.
 *
 * Class hierarchy
 * - GintAbstractTask
 *   - GintTask - base, setUp, tearDown
 *   - GintInternalTask
 *     - GintGroupTask
 */
@CompileStatic
@TypeChecked
public abstract class GintAbstractTask extends DefaultTask {

    protected Gint gint

    public ReportHelper reportHelper

    /**
     * Constructor
     */
    public GintAbstractTask() {
        super()
        gint = (Gint) this.getProject().getExtensions().getByName('gint')
        reportHelper = gint.reportHelper
    }
}
