/*
 * Copyright (c) 2009, 2020 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.objects

import java.util.regex.Pattern

import org.gint.plugin.Gint
import org.gint.plugin.GintUtils2

import groovy.io.FileType
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import org.gint.plugin.Constants

@CompileStatic
@TypeChecked
public class Runner {

    protected Gint gint

    protected boolean recursive // recurse on script directory
    protected String taskNamePrefix

    protected List<String> providedArgs // args provided at definition time
    protected String scriptDirName
    protected File scriptDir
    protected Pattern includePattern
    protected Pattern excludePattern

    protected Collection<String> includeList // null means not used
    protected Collection<String> excludeList
    protected Collection<String> includeExtensionList

    // Input directories and files that participate in out-of-date processing so changes will force scripts to be run again
    protected Collection<String> inputDirList
    protected Collection<String> inputFileList

    protected Closure outputFileClosure

    public Runner(final Gint gint, Map<String, Object> parameters) {

        this.gint = gint

        //gint.helper.log('runner parameters', parameters)
        recursive = parameters?.recursive == true

        taskNamePrefix = parameters?.containsKey('taskNamePrefix') ? parameters.taskNamePrefix : 'itest' // allow taskNamePrefix to be blank or null as special case
        if (gint.helper.isBlank(taskNamePrefix)) {
            taskNamePrefix = null
        }

        providedArgs = (List) (parameters.args instanceof List ? parameters.args : new ArrayList<String>())

        scriptDirName = parameters?.directory ?: 'src/itest/gradle'
        scriptDir = gint.project.file(scriptDirName)

        if (!scriptDir.exists()) {
            throw new Exception("Script directory '${scriptDir.getAbsolutePath()}' does not exist.")
        }

        includePattern = (parameters?.includePattern instanceof Pattern ? parameters?.includePattern : null) // null means not used
        excludePattern = (parameters?.excludePattern instanceof Pattern ? parameters?.excludePattern : null) // null means not used

        includeList = (Collection) (parameters?.includeList instanceof Collection ? parameters?.includeList : null) // null means not used
        excludeList = (Collection) (parameters?.excludeList instanceof Collection ? parameters?.excludeList : null)
        includeExtensionList = (Collection) (parameters?.includeExtensionList instanceof Collection ? parameters?.includeExtensionList : ['gradle'])

        // Input directories and files that participate in out-of-date processing so changes will force scripts to be run again
        inputDirList = (Collection) (parameters?.inputDirList instanceof Collection ? parameters?.inputDirList : [])
        if (parameters?.inputFileList instanceof Collection) {
            inputFileList = (Collection) (parameters?.inputFileList)
        } else {
            inputFileList = [] // add default entries if they are available in the script directory, this covers many standard use cases
            if (!recursive) { // no defaults for recursive choice, not a standard use case and check is not sub-directory specific
                [
                    'build.gradle',
                    'gradle.properties',
                ].each {
                    if ((new File(scriptDir, it)).exists()) {
                        inputFileList << it
                    }
                }
            }
        }

        // Output file(s) identified by closure that participate in out-of-date processing, defaulting to the standard gint xmlReport file
        outputFileClosure = (Closure) (parameters?.outputFileClosure instanceof Closure ? parameters.outputFileClosure : { simpleName ->
            gint.project.buildDir.toString() + GintUtils2.getFileSeparator() + Constants.TEST_REPORTS_DIRECTORY + GintUtils2.getFileSeparator() + simpleName + '.xml'
        })
    }

    public List<File> getSelectedFileList() {
        List<File> list = new ArrayList<File>()
        if (recursive) {
            scriptDir.eachFileRecurse FileType.FILES, {
                if (selectFile(it)) {
                    list.add(it)
                }
            }
        } else {
            scriptDir.eachFile FileType.FILES, {
                if (selectFile(it)) {
                    list.add(it)
                }
            }
        }
        // gint.helper.log('file list', list)
        return list
    }

    /**
     * Initial file selection based on patterns
     */
    protected boolean selectFile(File entry) {
        boolean select = !(entry.name in ['settings.gradle', 'gradle.properties'])
        select = select && ((includePattern == null) ||  includePattern.matcher(entry.name)) // patterns are more general, just match on full entry name
        select = select && ((excludePattern == null) || !excludePattern.matcher(entry.name)) // patterns are more general, just match on full entry name
        return select
    }

    /**
     * Secondary filtering based on entry information
     */
    public boolean selectFile(String name, String simpleName, String extension) {
        boolean select = (includeList == null) || (includeList.contains(name) || includeList.contains(simpleName))
        select = select && ((excludeList == null) || (!excludeList.contains(name) && !excludeList.contains(simpleName)))
        select = select && includeExtensionList?.contains(extension)
        return select
    }

    public File getScriptDir() {
        return scriptDir
    }

    public Pattern getIncludePattern() {
        return includePattern
    }

    public Pattern getExcludePattern() {
        return excludePattern
    }

    public Collection<String> getIncludeList() {
        return includeList
    }

    public Collection<String> getExcludeList() {
        return excludeList
    }

    public Collection<String> getIncludeExtensionList() {
        return includeExtensionList
    }

    public Collection<String> getInputDirList() {
        return inputDirList
    }

    public Collection<String> getInputFileList() {
        return inputFileList
    }
}
