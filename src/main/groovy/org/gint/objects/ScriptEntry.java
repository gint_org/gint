/*
 * Copyright (c) 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ScriptEntry {
    protected File file;
    protected String name;
    protected List<String> args;

    public ScriptEntry(final File file) {
        this.file = file;
    }

    public ScriptEntry(final String file) {
        this(new File(file));
    }

    public ScriptEntry(final File file, final Object name) {
        this.file = file;
        setName(name);
    }

    public ScriptEntry(final String file, final Object name) {
        this(new File(file), name);
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getName() {
        return name != null ? name : file.getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setName(Object name) { // this will handle GString
        this.name = name.toString();
    }

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<Object> args) { // handles GString
        this.args = new ArrayList<>();
        for (Object entry : args) {
            this.args.add(entry.toString());
        }
    }

}
