/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import org.codehaus.groovy.runtime.InvokerInvocationException
import org.gint.interfaces.CmdGenerator
import org.gint.plugin.Constants
import org.gint.plugin.Gint
import org.gint.plugin.GintUtils
import org.gint.plugin.Constants.TaskType
import org.gint.tasks.GintGroupTask
import org.gint.tasks.GintTask
import org.gint.tasks.GintRunnerTask
import org.gint.objects.ScriptEntry
import org.gradle.api.Task
import org.gradle.api.UnknownTaskException

import groovy.sql.Sql
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

@CompileStatic
@TypeChecked
public class TaskHelper extends BaseHelper {

    // Track last added task for each group of tasks (set up, tear down, and base) to preserve legacy ordering and boolean last task requests
    protected Map<String, GintTask> lastAddedTask = new HashMap<String, GintTask>()

    public TaskHelper(final Gint gint) {
        super(gint)
    }

    /**
     * Add Gint tasks
     * @param map representing a Gint task or null
     */
    public GintTask add(final Map<String, Object> map) {

        GintTask task = null
        if (map != null) {
            task = createTask(map) // create task object of the correct type
            //project.getLogger().quiet('map: {}', map)

            addTaskToGroup(task, map.group)
        }
        return task
    }

    public List<GintTask> add(final List<Map<String, Object>> list) {
        add('', list)
    }

    public List<GintTask> add(final List<String> group, final List list) {

        List<GintTask> taskList = new ArrayList<GintTask>()
        list.each { map ->
            if (map instanceof Map) {
                Task task = add((Map<String, Object>) map)
                taskList << task
                addTaskToGroup(task, group)
            }
        }
        return taskList
    }

    public List<GintTask> add(final String group, final List list) {
        return add([group], list)
    }

    /**
     * Add tasks based on an sql selection
     * @param connection to run select statement on
     * @param select - select rows
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public List<GintTask> add(final Sql connection, final String select) {
        List<GintTask> list = new ArrayList<GintTask>()

        if (connection == null) {
            gint.addFailedMessage("Unable to add tasks, SQL connection not valid.")
        } else {
            try {
                List rows = connection.rows(select)
                if (rows.size() > 0) {
                    Set<String> columns = rows[0].keySet()
                    rows.each { row ->
                        list << addFromRow(row, columns)
                    }
                }
            } catch (Exception exception) {
                gint.addFailedMessage("Unable to add tasks, exception running sql: " + exception.toString())
            }
        }
        return list
    }

    /**
     * Add a task based on a row elements and column list given the parameter names
     * @param row a sql row or other map like object
     * @param columns - column list that will be paired with row values to form the task
     * @return task that was added, information only for problem determination
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public GintTask addFromRow(final row, final Set<String> columns) {

        GintTask task = null
        Map<String, Object> map = [:]
        def success = true
        def numberOfColumns = [row.size(), columns.size()].min()
        for (int i = 0; i < numberOfColumns; i++) {
            String column = columns[i].trim()
            String value = row[i].trim()
            success = addColumnValue(map, column, value) && success // evaluate all before declaring failure
        }
        if (success) {
            if (numberOfColumns > 0) {
                if ((numberOfColumns > 1) || (row[0] != '')) {  // not blank csv row
                    add(map)
                }
            }
        } else {
            gint.addFailedMessage("Failed adding row data: " + row + ", columns: " + columns)
        }
        return task
    }

    /**
     * Add a task based a row and column list
     * @param row - sql row or map
     * @param column list that will be paired with row values to form the map
     * @return task that was added, information only for problem determination
     */
    protected GintTask addFromRow(final row, final List<String> columns) {

        GintTask task = null
        Map<String, Object> map = [:]
        def success = true
        columns.each { String column ->
            success = addColumnValue(map, column, row[column]) && success // evaluate all before declaring failure
        }
        if (success) {
            if (columns.size() > 0) {
                task = add(map)
            }
        } else {
            gint.addFailedMessage("Failed adding row data: " + row + ", columns: " + columns)
        }
        return task
    }

    /**
     * Add a column value to task with special interpretation of values based on column name
     * @param task map
     * @param column - column name
     * @param value - value to be modified and added to task
     * @return false if there was an exception trying to evaluated the value
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected boolean addColumnValue(final Map<String, Object> taskMap, final String column, final value) {

        boolean result = true
        final GROOVY_CODE_ENDING = '_code'
        try {
            def lowerCaseColumn = column.toLowerCase()
            if (helper.isNotBlank(value)) {
                // recognize boolean values as boolean
                taskMap[column] = (value == 'true' ? true : value == 'false' ? false : value)

                // TODO
                // handle closures
                //                if (lowerCaseColumn.equals('inline') || lowerCaseColumn.endsWith('closure')) {
                //                    taskMap[column + GROOVY_CODE_ENDING] = value // just for documentation/debugging purposes
                //                    taskMap[column] = binding.variables.groovyShell.evaluate(" { GintTask task -> ${value} }")
                //
                //                    // handle code
                //                } else if (lowerCaseColumn.endsWith(GROOVY_CODE_ENDING)) {
                //                    testcase[column] = value  // retain for documentation/debugging purposes
                //                    // strip off the code ending from the column name
                //                    testcase[column.substring(0, column.length() - GROOVY_CODE_ENDING.length())] = binding.variables.groovyShell.evaluate(value)
                //
                //                    // everything else
                //                } else {
                // taskMap[column] = value
                //}
            }
        } catch (Exception exception) {
            gint.printStackTrace(exception)
            message 'error', "Invalid value: ${helper.quoteString(value)} for column: ${column}"
            result = false
        }
        return result
    }


    public GintTask addSetUp(final Map<String, Object> map) {
        map.isSetUp = true
        return (GintTask) add(map)
    }

    public List<GintTask> addSetUp(final List<Map<String, Object>> list) {
        addSetUp('', list)
    }

    public List<GintTask> addSetUp(final String group, final List<Map<String, Object>> list) {
        list.each { map ->
            if (map instanceof Map) {
                map.isSetUp = true
            }
        }
        return (List<GintTask>) add(group, list)
    }

    public GintTask addTearDown(final Map<String, Object> map) {
        map.isTearDown = true
        return (GintTask) add(map)
    }

    public List<GintTask> addTearDown(final List<Map<String, Object>> list) {
        addTearDown('', list)
    }

    public List<GintTask> addTearDown(final String group, final List list) {
        list.each { map ->
            if (map instanceof Map) {
                map.isTearDown = true
            }
        }
        return (List<GintTask>) add(group, list)
    }

    /**
     * Centralize creating set up, tear down, and base tasks from a map
     * @param map
     * @return gint task
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected GintTask createTask(Map<String, Object> map) {
        String name = gint.addTaskClosure?.call(map) ?: map.name ?: gint.nextName
        verifyTaskName(name)
        name = name.replace(':', '_') // Gradle doesn't like : in task names

        GintTask task = newTask(name, map)
        task.group = task.group ?: GintUtils.getGroup(task) // set only if not set before

        loadFromMap(task, map)

        // Preserve arrival ordering of tasks (like gint2) except for tear down tasks that likely have different ordering
        // If task declares it's own shouldRunAfter setting, then do not add our own (becomes a user responsibility)
        def lastAdded = lastAddedTask.get(task.group)
        if (!map.containsKey('shouldRunAfter') && (lastAdded != null) && (task.group != Constants.GROUP_TEAR_DOWN)) {
            task.shouldRunAfter(lastAdded)
        }
        lastAddedTask.put(task.group, task)

        // Do we need duplicate tear down task?
        if (map.isTearDown && gint.doTearDownBoth()) {
            GintTask tearDownAfterTask = gint.project.task(name + '_afterTearDown', description: map.description, group: task.group, type: GintTask)
            tearDownAfterTask.type = TaskType.TEAR_DOWN_AFTER
            loadFromMap(tearDownAfterTask, map)
        }
        // helper.log 'task', task
        return task
    }

    /**
     * Allow subclass to modify the type of task to be created and do any custom processing
     * @param name
     * @param type
     * @return GintTask
     */
    protected GintTask newTask(final String name, final Map map) {
        GintTask task = (GintTask) gint.project.task(name, type: map.isRunner ? GintRunnerTask : GintTask)
        if (map.isSetUp) {
            task.type= TaskType.SET_UP
        } else if (map.isTearDown) {
            task.type = TaskType.TEAR_DOWN
        }
        return task
    }

    protected boolean verifyTaskName(final String name) {
        if (Constants.RESERVED_NAME_LIST.contains(name)) {
            println 'verify task name: ' + name
            throw new Exception("Unable to add task or group with name ${helper.quoteString(name)}. This is a reserved name.")
        }
    }

    /**
     * Add task to a group where group is a string or a list of strings
     * @param task
     * @param group
     */
    public void addTaskToGroup(final Task task, final group) {
        if (group instanceof List) {
            group.each { entry ->
                if ((entry instanceof String) || (entry instanceof GString)) {
                    addTaskToGroup(task, entry.toString())
                }
            }
        } else if ((group instanceof String) || (group instanceof GString)) {
            addTaskToGroup(task, group.toString())
        }
        // TODO do we want to fail here or just ignore
    }

    public void addTaskToGroup(final Task task, final String group) {

        if (helper.isNotBlank(group)) {
            verifyTaskName(group)
            Task groupTask = null
            try {
                groupTask = task.project.getTasks().getByName(group)
            } catch (UnknownTaskException exception) {
            }
            //project.getLogger().info('groupTask: {}', groupTask)
            if (groupTask == null) {
                groupTask = task.project.task(group, description: Constants.TASK_DESCRIPTION_GROUP, group: Constants.GROUP_BASE, type: GintGroupTask)
            }
            if (groupTask != null) {
                groupTask.dependsOn(task)
            }
        }
    }

    /**
     * Load task fields from a map
     * @param input amp
     * @param task
     */
    public void loadFromMap(final GintTask task, final Map<String, Object> inputMap) {
        task.map = inputMap // remember the user defined information

        inputMap.each { String key, value ->
            try {
                switch (key) {
                    case 'name': // already handled
                    case { Constants.IGNORE_TASK_KEY_LIST.contains(key) }:
                        break

                    case 'group':
                        task['gintGroup'] = value
                        break

                    case ['dependsOn', 'depends']: // both new gradle term and legacy gant term
                        setDependsOn(task, value)
                        break

                    case 'mustRunAfter':
                        setMustRunAfter(task, value)
                        break

                    case 'shouldRunAfter':
                        setShouldRunAfter(task, value)
                        break

                    case 'onlyIf':
                        if (value != null) {
                            if (value instanceof Closure) {
                                task.onlyIf = (Closure) value
                            } else {
                                task.onlyIf = { value == true }
                            }
                        }
                        break

                    case 'finalizedBy':
                        task.finalizedBy(value)
                        break

                    case ['cmdGenerator']:
                        if (value instanceof Closure) {
                            task.closures[key] = (CmdGenerator) value
                        } else if (value instanceof CmdGenerator) {
                            task.closures[key] = value
                        } // otherwise ignore
                        break

                    case { task.hasProperty(key) }:
                    //println 'key: ' + key + ', value: ' + value
                        if (value == null) {
                            try {
                                task[key] = value
                            } catch (InvokerInvocationException exception) {
                                // ignore this since we are probably trying to set a primitive type to null
                                // logger.warn('Ignore loading null value for key ' + helper.quoteString(key) + ' for task from a map.')
                            }
                        }  else {
                            task[key] = value
                        }
                        break

                    case { key.endsWith('Closure') || key == 'inline' }:
                        task.closures[key.replace('Closure', '')] = value
                        break

                    default:
                        break
                }
            } catch (Exception exception) {
                logger.warn('Ignore problem loading key ' + helper.quoteString(key) + ' for task from a map: ' + exception)
                gint.printStackTrace(exception)
            }
        }
        //debug()
    }

    protected void setDependsOn(final GintTask task, Object value) {
        if (value != null) {
            if (value instanceof Boolean) {
                if (value == true) {
                    setDependsOn(task, lastAddedTask[GintUtils.getGroup(task)])
                }
            } else {
                task.dependsOn(value)  // task name, path, relative path, closure
            }
        }
    }

    protected void setMustRunAfter(final GintTask task, Object value) {
        if (value != null) {
            if (value instanceof Boolean) {
                if (value == true) {
                    setMustRunAfter(task, lastAddedTask[GintUtils.getGroup(task)])
                }
            } else {
                task.mustRunAfter(value)  // task name, path, relative path, closure
            }
        }
    }

    protected void setShouldRunAfter(final GintTask task, Object value) {
        if (value != null) {
            if (value instanceof Boolean) {
                if (value == true) {
                    setShouldRunAfter(task, lastAddedTask[GintUtils.getGroup(task)])
                }
            } else {
                task.shouldRunAfter(value)  // task name, path, relative path, closure
            }
        }
    }

    public ScriptEntry newScriptEntry(final String file, final String name) {
        return new ScriptEntry(file, name);
    }

    public ScriptEntry newScriptEntry(final File file, final String name) {
        return new ScriptEntry(file, name);
    }
}
