/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.gint.helpers

import org.gint.plugin.Gint
import org.gint.plugin.Constants

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Class to allow parallel execution of platform executables or shell scripts.
 */
@CompileStatic
@TypeChecked
class ParallelHelper extends Helper {

    protected Process process = null  // process that runs a single command
    protected Thread outThread        // thread that handles receiving all standard output
    protected Thread errorThread      // thread that handles receiving all standard error output
    protected ArrayList outList = new ArrayList()    // storage for standard output
    protected ArrayList errorList = new ArrayList()  // storage for error output
    protected Map parameters = null
    protected Object command
    protected Closure message = Helper.getStandardMessageClosure()
    protected Integer returnCode = null // return code from running of the command

    public ParallelHelper(final Gint gint, final Map parameters = [:]) {
        super(gint)
        this.parameters = parameters ?: [:]

        def isWindows = gint.helper.isWindows()
        if (!(this.parameters.shell instanceof String)) {
            this.parameters.shell = isWindows ? 'cmd' : 'sh'
        }
        if (!(this.parameters.shellOption instanceof String)) {
            this.parameters.shellOption = isWindows ? '/c' : '-c'
        }
    }

    /**
     * Return the process associated with this object.
     * @return process - possibly null
     */
    public Process getProcess() {
        return this.process
    }

    /**
     * Set message closure
     */
    public void setMessageClosure(final Closure message) {
        this.message = message
    }

    /**
     * Get logging value - used to determine whether commands are logged
     * @return true if logging is enabled
     */
    public boolean getLog() {
        return (this.parameters.log != false)  // default to log = true
    }
    public void setLog(final boolean log) {
        this.parameters.log = log
    }

    /**
     * Get the return code from running the last command.
     * @return null or integer representing the running of the last command
     */
    public Integer getReturnCode() {
        return this.returnCode
    }

    /**
     * Start the process using a new Parallel object
     * @parameters map of parameters
     * @param command - can either be a string or a list of strings
     * @param tag - message tag for output
     * @return this (Parallel) object for future use
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected ParallelHelper startProcessWithNew(final Map parameters = [:], final Object command, final String tag) {
        def pHelper = new ParallelHelper(gint, parameters)   // need to construct a unique Parallel object
        pHelper.parameters.tag = tag
        pHelper.command = command
        //gint.helper.log('env', parameters.env?.getClass()?.getName())
        def env = null
        if (parameters.env == null) {
            // ignore
        } else if (parameters.env instanceof String[]) {
            env = (String [])parameters.env
        } else if (parameters.env instanceof Map) {
            env = []
            parameters.env.each { k, v ->
                env.add(/$k=$v/.toString())
            }
            //env = parameters.env.collect() { k, v -> "$k=$v" }
        } else if (parameters.env instanceof List<String>) {
            env = (List<String>) parameters.env
        } else {
            message 'warning', 'env parameter ignored as it is not an array, list, or map of strings.'
        }
        //gint.helper.log('env', env?.getClass()?.getName())
        File workDir = parameters.workDir
            ? (parameters.workDir instanceof File ? parameters.workDir : new File(parameters.workDir.toString()))
            : null
        pHelper.startProcess(command, tag, env, workDir)
        return pHelper
    }

    /**
     * start the process
     * @param command - can either be a string or a list of strings
     * @param tag - message tag for output
     * @param env - environment variables for new process as array, list, or Map
     * @param workDir - working directory for new process
     * @return this (Parallel) object for future use
     */
    protected ParallelHelper startProcess(final command, final String tag, final List<String> env, final File workDir = null) {
        return startProcess(command, tag, env?.toArray(new String[0]), workDir)
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    protected ParallelHelper startProcess(final command, final String tag, final env = null, final File workDir = null) {
        errorList = new ArrayList()  // allow re-use of object - start will start as if newly created
        outList = new ArrayList()
        if (this.parameters.log == true) {
            message(tag, command)
        }
        try {
            process = command.execute(env, workDir)
            if ((this.parameters.outFile == null) || (this.parameters.outFile == '')) {
                Closure outProcessing = (this.parameters['outProcessing'] ?: { outList.add(it) } )
                outThread = Thread.start { (new InputStreamReader(process.in)).eachLine(outProcessing) }
            } else {
                outThread = Thread.start {
                    def writer
                    try {
                        writer = new File(this.parameters.outFile).newWriter()
                        (new InputStreamReader(process.in)).eachLine { line ->
                            writer.writeLine(line)
                        }
                    } catch (IOException exception) {
                        message 'error', "Exception when creating out file ${this.parameters.outFile}. ${exception.toString()}"
                    } finally {
                        writer?.close()
                    }
                }
            }
            if ((this.parameters.errorFile == null) || (this.parameters.errorFile == '')) {
                Closure errorProcessing = (this.parameters['errorProcessing'] ?: { errorList.add(it) } )
                errorThread = Thread.start { (new InputStreamReader(process.err)).eachLine(errorProcessing) }
            } else {
                errorThread = Thread.start {
                    def writer
                    try {
                        def append = (this.parameters.errorFile == this.parameters.outFile) // append if the same
                        writer = new File(this.parameters.errorFile).newWriter(append)
                        (new InputStreamReader(process.err)).eachLine { line ->
                            writer.writeLine(line)
                        }
                    } catch (IOException exception) {
                        message 'error', "Exception when creating out file ${this.parameters.errorFile}. ${exception.toString()}"
                    } finally {
                        writer?.close()
                    }
                }
            }
            // send standardInput string to the process
            if (this.parameters.standardInput != null) {
                Thread.start {
                    def writer
                    try {
                        writer = new PrintWriter(new BufferedOutputStream(process?.out)) // process should be set, but once it was not
                        if (this.parameters.standardInput instanceof Collection) {
                            this.parameters.standardInput.each { line ->
                                writer << line + '\n'
                            }
                        } else {
                            writer << this.parameters.standardInput.toString()
                        }
                    } catch (IOException exception) {
                        message 'error', "Exception when handling standardInput. ${exception.toString()}"
                    } finally {
                        try {
                            writer?.close()
                        } catch (Exception ignore) {
                        }
                    }
                }
            }
        } catch (Exception exception) {
            message 'error', "Exception starting a parallel process. ${exception.toString()}. Command: ${command}"
            this.returnCode = Contants.FAIL_EXCEPTION
        }
        return this
    }

    /**
     * Check if the associated process has completed. Don't block if not.
     * @return true if process is complete or never been run, false otherwise
     */
    public boolean isComplete() {
        try {
            if (this.process != null) {
                this.process.exitValue()  // exception if not completed
            }
        } catch (IllegalThreadStateException ignore) {
            return false
        }
        return true
    }

    /**
     * End processing and handle output processing as requested
     * @param print - true to automatically output the output and error list
     * @return - result of running the OS shell or executable
     */
    public int end(final boolean print = true) {
        def result = Constants.FAIL_EXCEPTION  // abnormal end, process null trying to end
        if (this.process != null) {
            this.outThread.join()
            this.errorThread.join()
            result = process.waitFor()
            this.process = null

            if (print) {
                printOutList()
                printErrorList()
            }
            if ((this.parameters.outFile != null) && (this.parameters.outFile != '')) {
                message('listing', this.parameters.outFile)
            }
            if ((this.parameters.errorFile != null) && (this.parameters.errorFile != '')) {
                message('errlist', this.parameters.errorFile)
            }
            if (this.parameters.log == true) {
                message(this.parameters.tag, result + ' - ' + command)
            }
        } else {
            message 'error', "Attempt to end when the process is null"
        }
        this.returnCode = result
        return result
    }

    public List getOutList() {
        return this.outList
    }
    public void printOutList() {
        this.outList.each { line -> message 'out', line }
    }
    public List getErrorList() {
        return this.errorList
    }
    /**
     * Don't use message here - leaves a trailing null for some reason - TODO check later
     * Don't log as error, since ICmd output can just be low level messages (not errors)
     */
    public void printErrorList() {
        this.errorList.each { line -> message 'error', line }
    }
    //public def getCommand() {
    //  return this.command
    //}

    /**
     * Execute a command from the PATH.
     * @param parameters (optional) - Map of parameters: <code>outProcessing</code> is a <code>Closure</code> used to process
     * lines from standard out; <code>errorProcessing is a <code>Closure</code> used to process lines from
     * standard error; <code>outFile is a <code>String</code> used to identify an output file for standard out;
     * <code>errorFile is a <code>String</code> used to identify an output file for standard error;
     * @param command - the command as a single <code>String</code>.
     * @return this (Parallel) object for future use
     */
    public ParallelHelper executable(final Map parameters = [:], final command, final String tag = 'execute') {
        return startProcessWithNew(parameters, command, tag)
    }

    /**
     * Execute a command from the PATH.
     * @param parameters (optional) - Map of parameters: <code>outFile</code> is a <code>Closure</code> used to process
     * lines from standard out; <code>errProcessing is a <code>Closure</code> used to process lines from
     * standard error.
     * @param command - the command as a list of single <code>String</code>s.
     * @return this (Parallel) object for future use
     */
    public ParallelHelper executable(final Map parameters = [:], final Collection command, final String tag = 'execute') {
        return startProcessWithNew(parameters, command, tag)
    }

    /**
     * Execute a command from the PATH using a command shell.
     * @param parameters (optional) - Map of parameters: <code>outFile</code> is a <code>Closure</code> used to process
     * lines from standard out; <code>errProcessing is a <code>Closure</code> used to process lines from
     * standard error.
     * @param command - the command as a single <code>String</code>.
     * @return this (Parallel) object for future use
     */
    public ParallelHelper shell(final Map parameters = [:], final command) {
        return startProcessWithNew(parameters, [
            this.parameters.shell,
            this.parameters.shellOption,
            command
        ], 'shell')
    }

    /**
     * Execute a list of commands from the PATH using a command shell.
     * References:
     * <ul>
     * <li> Linux: http://www.comptechdoc.org/os/linux/manual2/runningcommands.html
     * <li> Windows: http://support.microsoft.com/kb/279253
     * </ul>
     * @param parameters (optional) - Map of parameters: <code>outFile</code> is a <code>Closure</code> used to process
     * lines from standard out; <code>errProcessing is a <code>Closure</code> used to process lines from
     * standard error.
     * @param list - is a list of commands <code>List</code>.
     * @param continueOnError - if true, run all commands, if false only run commands until first failure
     * @return this (Parallel) object for future use
     */
    public ParallelHelper shell(final Map parameters = [:], final Collection list, final boolean continueOnError = false) {
        def cmdString = helper.generateCommandString(list, continueOnError, helper.isWindows())
        return startProcessWithNew(parameters, [
            this.parameters.shell,
            this.parameters.shellOption,
            cmdString
        ], 'shell')
    }

    /**
     * Start a process to run ssh command on the host specified. Assumes host identifies user if needed and ssh keys are used (no passwords/paraphrases required)
     * @param parameters - advanced parameters. See above.
     * @param cmds - single cmd string or list of cmd strings
     * @param host - host name or userid@host or name of putty session identifier
     * @param targetIsWindows - to properly format multiple commands when continueOnError is true, need to know if target is a Windows system
     * @return Parallel object that has been started
     */
    public ParallelHelper ssh(final Map parameters = [:], final cmds, final host, final continueOnError = false, final targetIsWindows = false) {
        def list = (cmds instanceof Collection ? cmds : ([]<< cmds) )
        def cmd = [
            'ssh' ,
            host,
            '"' + gint.helper.generateCommandString(list, targetIsWindows, continueOnError) + '"'
        ]
        //println "shell cmd: " + cmd
        return startProcessWithNew(parameters, cmd, 'shell')
    }
}
