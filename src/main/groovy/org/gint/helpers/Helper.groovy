/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

import org.gint.plugin.Constants
import org.gint.plugin.Gint
import org.gradle.api.Task
import org.gradle.api.tasks.TaskState

import groovy.sql.GroovyResultSet
import groovy.sql.GroovyResultSetExtension
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import groovy.util.slurpersupport.GPathResult

// Enabling compile static gives compiler error TODO
@CompileStatic
@TypeChecked
public class Helper extends BaseHelper {

    protected Closure logFilter = null
    protected int defaultTextFieldLength = 40  // for pretty output formating

    public Helper(final Gint gint) {
        super(gint)
    }

    /**
     * Get the default text field length used for formatted output functions like logWithFormat
     * @return default text field length
     */
    public int getDefaultTextFieldLength() {
        return this.defaultTextFieldLength
    }

    /**
     * Set the default text field length used for formatted output functions like logWithFormat
     * @param length
     */
    public void setDefaultTextFieldLength(final int length) {
        this.defaultTextFieldLength = length
    }

    /**
     * Get the message closure used by the Helper functions - normally the standard GANT message closure
     * @return closure
     */
    public Closure getMessageClosure() {
        if (message == null) {
            message = gint.message
        }
        return message
    }

    /**
     * Set the message closure used to log message in Helper functions.
     * @param message
     */
    public void setMessageClosure(final Closure message) {
        this.message = message
    }

    /**
     * Get standard message closure
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Closure getStandardMessageClosure() {
        return { tag, msg -> println '         '.substring(tag.size()) + "[${tag}] ${msg}" }
    }

    /**
     * Get a standard message closure that puts lines into a list.
     * @param list - list to put standard messages
     * @param log - true to continue to log information as normal, false (default) to not log
     * @return closure that takes tag and msg (like the standard message closure)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Closure getStandardMessageClosure(final List list, final boolean log = false) {
        def messageClosure = { tag, msg ->
            def line = '         '.substring(tag.size()) + "[${tag}] ${msg}"
            list << line
            if (log) {
                println line
            }
        }
        return messageClosure
    }

    /**
     * Get a standard message closure that puts lines into a file.
     * @param file - file to write standard messages
     * @param log - true to continue to log information as normal, false (default) to not log
     * @return closure that takes tag and msg (like the standard message closure)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Closure getStandardMessageClosure(final File file, final boolean log = false) {
        def messageClosure = { tag, msg ->
            def line = '         '.substring(tag.size()) + "[${tag}] ${msg}"
            file << line
            file << System.properties.'line.separator'
            if (log) {
                println line
            }
        }
        return messageClosure
    }

    /**
     * Get a standard message closure that appends lines to a string builder
     * @param builder - string builder to append lines to
     * @param log - true to continue to log information as normal, false (default) to not log
     * @return closure that takes tag and msg (like the standard message closure)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Closure getStandardMessageClosure(final PrintWriter writer, final boolean log = false, final lineBreak = '\n') {
        def messageClosure = { tag, msg ->
            def line = '         '.substring(tag.size()) + "[${tag}] ${msg}"
            writer << line
            writer << lineBreak
            writer.flush()
            if (log) {
                println line
            }
        }
        return messageClosure
    }

    /**
     * Get a standard message closure that appends lines to a string builder
     * @param builder - string builder to append lines to
     * @param log - true to continue to log information as normal, false (default) to not log
     * @return closure that takes tag and msg (like the standard message closure)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Closure getStandardMessageClosure(final StringBuilder builder, final boolean log = false) {
        def messageClosure = { tag, msg ->
            def line = '         '.substring(tag.size()) + "[${tag}] ${msg}"
            builder.append(line).append('\n')
            if (log) {
                println line
            }
        }
        return messageClosure
    }

    /**
     * Set filter for logging. Filter defines entries (keys) to be excluded.
     * <pre>
     * Filter closure should take one parameter and return true if value is to be filter (not logged).
     * Filter pattern will be used as a stringContains pattern
     * Filter string will be used as a case insensitive, literal pattern
     * </pre>
     * @param filter pattern or closure returning boolean - example: { text -> return helper.stringContains(text, ~/(?i)password/) }
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void setLogFilter(final filter) {
        this.logFilter = (
                        (filter instanceof Closure) ? filter
                        : (filter instanceof Pattern  ? { text ->
                            this.stringContains(text, filter) }
                        : (filter instanceof String   ? { text ->
                            this.stringContains(text, filter, Pattern.CASE_INSENSITIVE | Pattern.LITERAL) }
                        : null)))
    }

    public Closure getLogFilter() {
        this.logFilter
    }

    /**
     * Closure to filter out values containing the word password. Closure returns true if text look like a password
     * @return closure that looks for passwords
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Closure getPasswordFilter() {
        return { text ->
            this.stringContains(text, "password", Pattern.CASE_INSENSITIVE | Pattern.LITERAL)
        }
    }

    /**
     * Evaluate a string replacing variables with values
     * @param string - string possibly containing replacement values
     * @return evaluated string
     */
    public def evaluateString(final String string) {
        Eval.me(quoteString(string, '"'))
    }

    /**
     * Get a value either directly, or if the value is a closure, evaluate it first.
     * Handle 0 and 1 parameter closures
     */
    static public def getValueHandleClosure(def value, firstParameter = null) {
        if (value instanceof Closure) { // make it convenient for caller
            return (((value.getMaximumNumberOfParameters() > 0) && (firstParameter != null)) ? value.call(firstParameter) : value.call())  // evaluate
        }
        return value
    }

    /**
     * Safe way to get the value of a variable or property
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined
     * @param omitProjectProperties - do NOT use project properties as part of the lookup options
     * @return value of variable or property or the default value if specified, otherwise null
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def getValue(final name, final defaultValue = null, final boolean omitProjectProperties = false) {

        def value
        if (System.properties.containsKey(name)) {
            value = System.properties[name] // this gets system properties even if they are updated in the script
        } else if (gint.project.ant.getProperties().containsKey(name)) { // only set if actually specified as a property, but it could be null
            value = gint.project.ant[name] // this includes all system properties as well even though we get them separately
        } else if (!omitProjectProperties) {
            value = gint.project.findProperty(name) ?: defaultValue
            // Avoid most likely conflicts with project properties where user expects the default to be assigned and not a built-in project property
            if ((value != null) && [
                // property found but is on the list then use default instead
                'components',
                'group',
                'name',
                'parent',
                'path',
                'properties',
                'state',
                'status',
                'version',
                'project', // this was the one we have specific conflicts with migrating gint2 code
            ].contains(name)) {
                value = defaultValue
            }
        } else {
            value = defaultValue
        }
        // log('value for ' + name, value)
        return value
    }

    /**
     * Same as getValue with additional check of the variable in all lower case
     * Useful for user provided parameters that may not be case accurate.  Only covers all lower case errors.
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined
     * @return value of variable or property or the default value if specified, otherwise null
     */
    public String getParameterValue(final name, final defaultValue = null) {
        def result = getValue(name)  // try regular case
        if (result == null) {        // try lowercase
            return getValue(name?.toString()?.toLowerCase(), defaultValue)
        }
        return result
    }

    /**
     * Get parameter value, but check boolean and integer values first
     * @param name
     * @return value converted to boolean or integer if applicable, string otherwise or the defaultValue if not set
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def getParameterValueConvertType(final name, final defaultValue = null) {
        def result = getBooleanParameterValue(name, null)
        if (result == null) {
            result = getIntegerParameterValue(name, null)
            if (result == null) {
                result = getParameterValue(name, null)
                if (result == null) {
                    result = defaultValue // may be boolean or String
                }
            }
        }
        return result
    }


    /**
     * Same as getParameterValue with additional check of existence of default file
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined
     * @param defaultFileNotFoundValue - return value if file is not found
     * @return value of variable or other defaults if appropriate
     */
    public def getFileParameterValue(final name, final defaultValue = null, final defaultFileNotFoundValue = null) {
        def value = getParameterValue(name, null) // null if not defined
        if ((value == null) && (defaultValue != null)) {
            value = (new File(defaultValue.toString()).exists()) ? defaultValue : defaultFileNotFoundValue
        }
        return value
    }

    /**
     * Same as getFileParameterValue except it can have an alternate value to check existence on before going to default
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined
     * @param alternateDefaultValue - an alternate file to check for existence
     * @param defaultValueFileNotFoundValue
     * @return value of variable or other defaults if appropriate
     */
    public def getFileParameterValue(final name, final defaultValue, final alternateDefaultValue, final defaultFileNotFoundValue) {
        def value = getFileParameterValue(name, defaultValue, alternateDefaultValue)
        if ((value == alternateDefaultValue) && !(new File(value.toString()).exists())) { // alternate doesn't exist either
            value = defaultFileNotFoundValue
        }
        return value
    }

    public def getFileParameterValue(final name, final defaultValue, final alternateDefaultValue, final alternateDefaultValue2, final defaultFileNotFoundValue) {
        def value = getFileParameterValue(name, defaultValue, alternateDefaultValue, alternateDefaultValue2)
        if ((value == alternateDefaultValue2) && !(new File(value.toString()).exists())) { // alternate2 doesn't exist either
            value = defaultFileNotFoundValue
        }
        return value
    }

    /**
     * Same as getValue with additional check of the variable in all lower case
     * Useful for user provided parameters that may not be case accurate.  Only covers all lower case errors.
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined
     * @return value of variable or property or the default value if specified, otherwise null
     */
    public def getRequiredParameterValue(final name) {
        def result = getValue(name)
        if (result == null) {
            throw new Exception("$name is a required parameter for this function")
        }
        return result
    }

    /**
     * Same as getParameterValue with ability to extend the lookup by evaluating the result multiple times until
     * an invalid evaluation. This means that the command line can refer to variables that refer to other variables etc...
     * The lookup cycle is limited to prevent looping.
     * This means the value retrieved is used to lookup again. Useful for allowing a command line parameter to
     * reference a property value, making it easier for the user to use command line to set more complex
     * values. Example is cli parameters.
     * @param name - property to look up
     * @param defaultValue - default value used only if the name is not found, default value is also looked up
     * @return value of named property or subsequent evaluations
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public String getParameterValueWithExtendedLookup(name, final defaultValue = null) {

        name = name?.toString()
        def value = null
        Pattern variablePattern = null
        for (int i = 0; i < 25; i++) {  // keep following chain, but don't loop forever
            def newValue = null
            if (!name?.contains('+') && !name?.contains('-')) { // exclude common failures
                try {
                    newValue = getParameterValue(name) // see if the name resolves to another value
                    if ((newValue != null) && isDebug()) {
                        message 'debug', "extendedLookup - name: ${name}, newValue: ${newValue}"
                    }
                } catch (Exception e) {
                    // ignore any errors, use the 1st previous value
                }
            }
            // Extended lookup to replace groovy style variables
            // They may not have been resolved automatically (at ANT property support) because they may have been in different property files
            if (newValue == null) {
                if (name?.contains('${')) {
                    if (variablePattern == null) {
                        variablePattern = Pattern.compile('\\$\\{([a-zA-Z0-9]+)\\}')
                    }
                    //logWithFormat("name", name)
                    //logWithFormat("variablePattern", variablePattern)
                    newValue = name.replaceAll(variablePattern, { all, variable ->
                        def replacement = getParameterValue(variable)
                        //logWithFormat("replacement", replacement)
                        return replacement ?: '${' + variable + '}' // leave as is
                    })
                }
            }
            if (newValue != null) {
                newValue = newValue.toString() // may be an integer for example
                if (newValue != name) { // also leave if same value is retrieved
                    value = newValue
                    name = value
                } else {
                    break // get out of here with the last non-null value
                }
            } else {
                break
            }
        }
        return value ?: defaultValue
    }

    /**
     * Similar to getParameterValueWithExtendedValue, except use alternateName only if name is null
     * and return name if lookup not found
     * @param name - name to lookup
     * @param alternateName to lookup
     * @param default value if alternate is used and nothing found
     * @return
     */
    public String getWithExtendedLookup(name, final alternateName, final defaultValue = null) {
        return name ? getParameterValueWithExtendedLookup(name, name) :
                        getParameterValueWithExtendedLookup(alternateName, defaultValue)
    }

    /**
     * Converts string or boolean true, false to boolean.
     * Note that a blank value indicates it is set from the command line, so this makes it true
     * Ensures it returns either true, false or the default value
     * If not set, set to default value. If set, ensure it is either false or set to true
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined (does not have to be boolean!)
     * @return value of variable or property or the default value if specified, otherwise false
     */
    public def getBooleanValue(final name, final Boolean defaultValue = false) {
        def value = getValue(name)  // null if not defined
        def result = (((value == '') || (value == 'true') || (value == true)) ? true
                        : ((value == 'false') || (value == false)) ? false : defaultValue)
        return result
    }

    /**
     * Same as getBooleanValue with additional check of the variable in all lower case
     * If not set, set to default value. If set, ensure it is either false or set to true
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined
     * @return value of variable or property or the default value if specified, otherwise false
     */
    public Boolean getBooleanParameterValue(final name, final Boolean defaultValue = false) {
        def value = getParameterValue(name) // null if not defined
        if (value instanceof String) {
            value = value.trim()
        }
        return (((value == '') || (value == 'true') || (value == true)) ? true
                        : ((value == 'false') || (value == false)) ? false : defaultValue)
    }

    /**
     * Converts string to int value.  Set to default value if the evaluated value is not an int
     * If not set, set to default value.
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined, does not need to be an int!!
     * @return value of variable or property or the default value if specified
     */
    public Integer getIntegerValue(final name, final Integer defaultValue = 0) {
        String value = getValue(name)  // null if not defined
        Integer result = defaultValue
        if (value != null) {
            try {
                result = value instanceof Integer ? value : Integer.parseInt(value.toString())
            } catch (NumberFormatException exception) {
                // ignore
            }
        }
        return result
    }

    /**
     * Same as getIntegerValue with additional check of the variable in all lower case
     * @param name - name of variable to get value
     * @param defaultValue - return value if variable is not defined (does not need to be an int!!)
     * @return value of variable or property or the default value if specified, otherwise false
     */
    public Integer getIntegerParameterValue(final name, final Integer defaultValue = 0) {
        //def value = getParameterValue(name) // null if not defined
        return getIntegerValue(name, defaultValue)
    }

    public Integer getInteger(final value, Integer defaultValue) {
        return value instanceof Integer ? value : value?.toString()?.isInteger() ? value.toString().toInteger() : defaultValue
    }

    /**
     * Reconstruct groovy/gant standard command line parameter string: -Dparm=value
     * If parameter is not defined, use default value.
     * If value is null, return blank
     * If value is '', only -Dparm will be returned
     * Use value ' ' to produce -Dparm=
     * @param name - name of parameter
     * @param defaultValue - value used if named variable is not defined
     * @param noEqualSign - false to show equal sign if value is ''
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    String reconstructParameter(String name, String defaultValue = null, boolean noEqualSign = true) {
        name = name.trim()
        def value = getParameterValue(name, defaultValue)
        return (value == null ? '' : '-D' + name + (((value == '') && noEqualSign) ? '' : '=' + ((value != ' ') && value.contains(' ') ? quoteString(value) : value)))
    }

    String reconstructBooleanParameter(String name, Boolean defaultValue = null) {
        name = name.trim()
        def value = getBooleanParameterValue(name, defaultValue)
        return (value == null ? '' : '-D' + name + ((value != false) ? '' : '=false'))
    }

    /**
     * Test if variable (or property) is defined - the variable has to be evaluated first
     * @param name of variable to check
     * @return true if name stands for a currently defined variable
     */
    public boolean isVariable(String name) {
        return (getValue(name) != null)
    }

    /**
     * Safe way to get the value of a property
     * @param name of property to get
     * @param defaultValue returned if property is not defined
     * @return value of property or default value if not defined
     */
    public def getPropertyValue(final name, final defaultValue = null) {
        return (isProperty(name) ? gint.project.ant.properties.get(name) : defaultValue)
    }

    /**
     * Test if property is defined - useful when dealing with Ant tasks
     * @param name of property
     * @return true if defined, false otherwise
     */
    public boolean isProperty(final name) {
        return gint.project.ant.properties.containsKey(name)
    }

    /**
     * Scan string that contains something like key = value separated by blank or comma or |
     * @param string
     * @param key
     * @return value
     */
    public String getValueFromKeyValueString(string, key) {
        def value = null
        def matcher = string =~ /(?i)(?m)(?:(?:[ ,|])|(?:^))${key} *= *(.*?)(?:(?:[ ,|])|(?:$))/   // blank, comma or | separated
        if (matcher.find()) {
            value = matcher.group(1).trim()
        }
        return value
    }

    /**
     * Simply reflect logger level setting
     * @return true if logger is set at this levelur specified -v or -d on command
     */
    public boolean isVerbose() {
        return logger.infoEnabled
    }

    /**
     * Has user specified -d on the gant command line.
     * For gant states: see http://gant.sourcearchive.com/lines/1.6.0/classorg_1_1codehaus_1_1gant_1_1GantState.html
     * @return true if user specified -d on command
     */
    public boolean isDebug() {
        return logger.debugEnabled
    }

    /**
     * Has user specified -q (quiet) on the gant command line
     * @return true if user specified -q on command
     */
    public boolean isQuiet() {
        return logger.quietEnabled
    }

    /**
     * Single/double quote a string by escaping internal quotes, default escape is to double it
     * @param string to quote - strings or gstrings are quoted, elements in collections are quoted, closures are evaluated first, other objects left as is
     * @param quote character default to single quote
     * @param escape character for embedded quotes
     * @param alternate quote - if string contains embedded quote but does not contain alternate quote, then quote with alternate
     * @return quoted string or string representation of a list of quoted strings
     */
    static public String quoteString(final element, final String primaryQuote = "'", final escape = null, final String alternateQuote = null) {
        if ((element instanceof String) || (element instanceof GString)) {  // don't quote non-elements
            def result = new StringBuilder()
            def quote = alternateQuote && element.contains(primaryQuote) && !element.contains(alternateQuote) ? alternateQuote : primaryQuote
            result.append(quote)
            element.each { aChar ->
                if (aChar == quote) {
                    result.append(escape ?: quote)  // escape embedded quote or double it
                }
                result.append(aChar)
            }
            result.append(quote)
            return result.toString()
        } else if (element instanceof Collection){
            return listToSeparatedString(list: element, entryClosure: { entry -> quoteString(entry, primaryQuote) })
        } else if (element instanceof Closure) {
            return quoteString(element.call(), primaryQuote)
        }
        return (element == null ? '' : element)
    }

    /**
     * Simple case of quoteString for double quoting with embedded double quotes doubled
     * @param element
     * @return double quoted string
     */
    static public String doubleQuoteString(final element) {
        return quoteString(element, '"')
    }

    /**
     * Reverse action for quoteString by removing quotes and removing internal doubled quotes. Nothing is done if string is not quoted
     * Copied from JAVA version.
     * @param string to quote - strings or gstrings are quoted, elements in collections are quoted, closures are evaluated first, other objects left as is
     * @param quote character - either ' or "
     * @return string with leading, trailing, and double quotes removed
     */
    static public def stripQuotes(element, final quote = "'") {
        if ((element instanceof String) || (element instanceof GString)) {  // don't strip quotes from non-elements
            if ((element.length() > 1) && (element.charAt(0) == quote) && (element.charAt(element.length() - 1) == quote)) {
                def result = new StringBuilder()
                boolean previousWasQuote = false // deal with multiple quotes in a row
                for (int i = 1; i < element.length() - 1; i++) {
                    if (previousWasQuote || ((element.charAt(i) != quote) || (i == element.length() - 1) || (element.charAt(i + 1) != quote))) {
                        result.append(element.charAt(i))
                        previousWasQuote = false
                    } else {
                        previousWasQuote = true
                    }
                }
                element = result.toString()
            }
        } else if (element instanceof Collection) {
            return listToSeparatedString(list: element, entryClosure: { entry -> stripQuotes(entry) })
        } else if (element instanceof Closure) {
            return stripQuotes(element.call())
        }
        return element
    }

    /**
     * Get file name but strip any leading path information
     * - deal with either / or \ based names no matter what platform this is running on by normalizing
     *   to / as in a gradle context path names can be mixed anyway like: src/itest/cli/gradle\cli.gradle
     * @param name - File name
     * @return just the name part (with extension)
     */
    static public def getNameNoPath(final String name) {
        def lastSlashIndex = name.replace('\\', '/').lastIndexOf('/') // normalize
        return lastSlashIndex < 0 ? name : name.substring(lastSlashIndex + 1) // remove path
    }

    /**
     * Get file name without extension
     * @param name - File name
     * @return just the name part without extension
     */
    static public def getNameNoExtension(final String name) {
        // TODO: replace with: name.take(entry.name.lastIndexOf('.'))
        def extensionIndex = name.lastIndexOf('.')
        if (extensionIndex >= 0) {
            return name.substring(0, extensionIndex)  // remove extension
        }
        return name
    }
    static public def getNameNoExtension(final File aFile) {
        return getNameNoExtension(aFile.getName())
    }

    /**
     * Get file extension
     * @param file - File
     * @return just the name part
     */
    static public def getExtension(String name) {
        def extensionIndex = name.lastIndexOf('.')
        if (extensionIndex >= 0) {
            return name.substring(extensionIndex + 1)
        }
        return ''
    }
    static public def getExtension(final File aFile) {
        return getExtension(aFile.getName())
    }

    static public java.nio.file.Path getPathRelativeToCurrentDirectory(final java.nio.file.Path path) {
        return java.nio.file.Paths.get(getCurrentDirectory()).relativize(path)
    }

    static public String getCurrentDirectory() {
        return System.getProperty("user.dir")
    }

    /**
     * Find duplicate entries in a list
     * @param list - a list of values
     * @param printList - if true, print out the list of duplicates
     * @return set of duplicate entries
     */
    public def getDuplicates(final List list, final printList = false) {
        def duplicates = []as Set
        def checkList = list as Set
        checkList.each { entry ->
            def found = list.indexOf(entry)  // first
            if (list.subList(found + 1, list.size()).indexOf(entry) >= 0) {
                if (printList) {
                    println "Duplicate: $entry"
                }
                duplicates << entry
            }
        }
        return duplicates
    }

    /**
     * Get a list of file objects from a directory matching selection. The list can be sorted by using something like .sort { it.lastModified() }
     * @param dir
     * @param selection regex (like .*\.txt)
     * @return list of File objects
     */
    static public Collection<File> getFileList(final String dir, final selection) {
        return new File(dir).listFiles().findAll {
            it.getName() ==~ selection
        }
    }

    /**
     * Read a file into a string respecting encoding, ignore invalid encodings
     * @param file to read
     * @return string representing the file contents
     */
    static public String readFile(final File file, final encoding = null) {
        assert file.exists()  // readFile requires that the file exist
        if (encoding != null) {
            try {
                return file.getText(encoding?.toString())
            } catch (java.io.UnsupportedEncodingException exception) {
                getStandardMessageClosure()('warning', "Ignoring invalid encoding ${encoding}")
            }
        }
        return file.getText()  // standard case, no encoding
    }
    static public String readFile(final fileName, final encoding = null) {
        if (fileName == null) {
            throw new Exception("File name must be specified.")
        }
        return readFile(new File(fileName.toString()), encoding)
    }

    /**
     * Read a file as list of lines respecting encoding, ignore invalid encodings
     * @return list of lines
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public List<String> readFileToList(final File file, final encoding = null) {
        def result = []
        boolean done = false
        if (encoding != null) {
            try {
                file.eachLine(encoding) { line -> result << line }
                done = true  // successful at processing
            } catch (java.io.UnsupportedEncodingException exception) {
                getStandardMessageClosure()('warning', "Ignoring invalid encoding ${encoding}")
                //if (getVerbose()) {
                //  message 'warning', "Ignoring invalid encoding ${encoding}"
                //}
            }
        }
        if (!done) {  // default to standard case
            file.eachLine { line -> result << line }
        }
        return result
    }
    static public List<String> readFileToList(final fileName, final encoding = null) {
        if (fileName == null) {
            throw new Exception("File name must be specified.")
        }
        return readFileToList(new File(fileName.toString()), encoding)
    }

    /**
     * Write a file from a list of string.
     * Note - another function could use withWriterAppend could also be used
     * @param file
     * @param list
     * @param encoding
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public void writeFileFromList(final File file, final list, boolean append = false, final encoding = null) {
        def closure = { out ->
            list.each { line -> out.println line }
        }
        if (append) {
            file.withWriterAppend(encoding ?: Charset.defaultCharset().toString(), closure)
        } else {
            file.withWriter(encoding ?: Charset.defaultCharset().toString(), closure)
        }
    }

    /**
     * Generate a naming sequence but start based on the pattern established by the first name. Primarily used for screenshot sequencing.
     * This mechanism is used to aid ordering when looking in directory or Bamboo
     * <pre>
     * xxx -> xxx001, xxx002, etc.
     * xxx98 -> xxx99, xxx100, etc.
     * </pre>
     * @param name
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public nextNumberedName(name) {
        assert name != null  // next numbered name cannot be null
        def extension = getExtension(name)
        def nextName = getNameNoExtension(name)
        def matcher = nextName =~ /((0*)(\d+))(?!\w+)/  // look for name ending in a number
        if (matcher.find()) {
            // println '1: ' + matcher.group(1) + ', 2: ' + matcher.group(2) + ', 3: ' + matcher.group(3) + ', 3: '
            def digitCount = matcher.group(1).size()
            def zeros = matcher.group(2) == '' ? 0 : Integer.parseInt(matcher.group(2))
            def postfixNumber =  Integer.parseInt(matcher.group(3)) + 1

            def postfix = String.format('%0' + digitCount + 'd', postfixNumber) // try to retain existin number of digits
            nextName = nextName.substring(0, matcher.start()) + postfix
        } else {
            nextName = nextName + String.format('%03d', 1) // start with 2 if file already there
        }
        //println "name: ${name}, nextName: ${nextName}"
        return nextName + (extension.size() > 0 ? '.' + extension : '')
    }

    static public generateNextFileName(name) {
        assert name
        while (true) {
            if ((new File(name?.toString())).exists()) {
                name = nextNumberedName(name)
            } else {
                break
            }
        }
        //println "name: ${name}"
        return name
    }

    /**
     * Verbose message - same as message, but looks at verbose setting
     * @param type - first part of what is looged
     * @param text - text of message
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void vMessage(final type = 'info', final text) {
        if (isVerbose()) {
            message(type, text)
        }
    }

    /**
     * Generate a standard target name from a prefix and a name:  prefix_name
     * @param prefix
     * @param name
     * @return target name derived from prefix and name
     */
    static public String generateTargetName(final String prefix, final String name) {
        return "${prefix}_${name}"
    }

    /**
     * Format text and value in the form:  text . . . . : value
     * @param text - text portion of formatted string
     * @param value - value portion of formatted string
     * @param textFieldLength - maximum length of text field for proper formatting
     * @return formatted string
     */
    public def formatText(String text, final value, final int textFieldLength = this.defaultTextFieldLength) {
        if (text == null) {
            text = ''
        }
        def filler
        if (text.size() >= textFieldLength) {
            filler = ''
        } else {
            filler = ((text.size() % 2)) ? ' ' : '  '  // add extra space if length of text is even
            for (int i = 0; i < (textFieldLength - text.size() - 2)/2; i++) {
                filler += '. '
            }
        }
        return text + filler + ": " + value.toString()
    }

    @Deprecated
    public logWithFormat(final text, value, final int textFieldLength, final String tag) {
        log(text, value, textFieldLength, tag)
    }

    /**
     * Log formatted text and value in the form:  text . . . . : value
     * Does the right thing for objects that implement Map, List etc..., for example GroovyRowResult
     * However sql eachRow returns a different object class for row and needs special code handling - goofy Groovy
     * @param text - text portion of formatted string
     * @param value - value portion of formatted string
     * @param textFieldLength - maximum length of text field for proper formatting
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void log(final text, value, final int textFieldLength = this.defaultTextFieldLength, final String tag = 'info') {

        if (value instanceof GroovyResultSet) {  // see http://jira.codehaus.org/browse/GROOVY-2859
            value = value.toRowResult()
        }
        if ((value instanceof Map) || (value instanceof Hashtable) || (value instanceof Properties)) {
            // preserve insertion order for linked hash maps, but sort others to get predictable order
            (value instanceof LinkedHashMap ? value.keySet() : value.keySet()?.sort()).each { name ->
                // Prevent looping when map or similar contains a reference to itself like gradle project for instance
                def newValue = value[name]
                if ((!(newValue instanceof Map) || !((Map)newValue).containsKey(name))
                && (!(newValue instanceof Hashtable) || !((Hashtable)newValue).containsKey(name))
                && (!(newValue instanceof Properties) || !((Properties)newValue).hasProperty(name))) {
                    log(logPrefix(text, '.') + name, value[name], textFieldLength, tag)
                } else {
                    println 'ignore: ' + name
                }
            }
        } else if (value instanceof Task) {
            ((Task) value).getProperties().sort().each { key, v ->
                if (v != null) {  // don't log null values
                    if (!(v instanceof Task) && !key.equals('taskThatOwnsThisObject')) { // watch out for recursion
                        log(logPrefix(text, '.') + key, v, textFieldLength, tag)
                    }
                }
            }
        } else if (value instanceof TaskState) {
            ((TaskState) value).getProperties().sort().each { key, v ->
                if (v != null) {  // don't log null values
                    log(logPrefix(text, '.') + key, v, textFieldLength, tag)
                }
            }
        } else if (value instanceof GroovyResultSetExtension) {  // special handling for eachRow handling
            def metaData = value.getResultSet().getMetaData()
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                def column = metaData.getColumnName(i)
                log(logPrefix(text, '.') + metaData.getColumnLabel(i), value.getAt(i-1))
            }
            // break out a list if it would overflow a single line
        } else if ((value instanceof Collection) && (value.toString().size() > (132 - textFieldLength))) {
            def count = 0
            try {
                value.each { element ->
                    log(logPrefix(text, '') + '[' + count + ']', element, textFieldLength, tag)
                    count++  // 0 based counting is probably best to avoid confusion for programmers
                }
            } catch (ConcurrentModificationException ignore) {
                // ignore case where we are trying to log tasks output field and logging more stuff !!!
            }
        } else if (value instanceof GPathResult) {  // XMLSlurper
            if (value.children().size() == 0) {
                if (isNotBlank(value.text())) {
                    log(text, value.text())
                }
            } else {
                def count = 0
                value.children().each { entry ->
                    // insert a count value if there are more than one of the same name in the set of being processed
                    def doCount = (value.children().findAll { e -> e.name() == entry.name() }).size() > 1
                    def countString = doCount ? '[' + count + ']' : '' // only put in count if needed
                    //println 'name: ' + value.name() + ', value children size: ' + value.children().size() + ', entry name: ' + entry.name + ', entry children size: ' + entry.children().size() + ', countString: ' + countString
                    log(logPrefix(text, '.') + entry.name() + countString, entry, textFieldLength, tag)
                    count++
                }
            }
            // now do the attributes
            if (value.attributes().size() > 0) {
                value.attributes().each { aKey, aValue ->
                    log(logPrefix(text, '.@') + aKey, aValue)
                }
            }
        } else if (value instanceof Calendar) {
            log(text, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(((Calendar) value).getTime()))

        } else if (value instanceof Matcher) {
            Matcher matcher = (Matcher) value
            log(logPrefix(text, '.') + 'regex', matcher.pattern().pattern())
            log(logPrefix(text, '.') + 'groupCount', matcher.groupCount())
            try {
                for (int i = 1; i <= matcher.groupCount(); i++) {
                    log(logPrefix(text) + '[' + i + ']', matcher.group(i))
                }
            } catch (IllegalStateException ignore) { // matches or find not called or failed, no further information available
            }

            // Guard against selenium not in class path
        } else if (value?.getClass()?.getName() == 'org.openqa.selenium.WebElement') {  // Selenium
            log(text, getWebElementAttributesNoExceptions(value, gint.getSeleniumHelper().getDriver()))
        } else {
            if ((getLogFilter() == null) || !(getLogFilter().call(text))) {
                message(tag, formatText(text, value, textFieldLength))
            }
        }
    }

    /**
     * Handle null log prefix request
     * @param text
     * @param separator
     * @return blank or prefix
     */
    protected String logPrefix(final text, final String separator = '') {
        return text == null ? '' : text.toString() + separator
    }

    /**
     * Same as <code>logWithFormat()</code> except respecting verbose setting
     */
    public void vLog(final text, final value, final int textFieldLength = this.defaultTextFieldLength, final String tag = 'info') {
        if (isVerbose()) {
            log(text, value, textFieldLength, tag)
        }
    }

    /**
     * Same as <code>logWithFormat()</code> except respecting debug setting and tagged with 'debug'
     */
    public void dLog(final text, final value, final int textFieldLength = this.defaultTextFieldLength, final String tag = 'debug') {
        if (isDebug()) {
            log(text, value, textFieldLength, tag)
        }
    }

    /**
     * Log a variable or list of variables using a formatted display of the variable name and its value
     * VARIABLE(s) MUST BE IN THE BINDING, not local variables :(.  Use logWithFormat for local variables.
     * IF NOT, just issue a error message, do NOT throw an exception! Nothing worse than having a simple
     * logging function cause automation to fail because of a typo or mistake.
     * @param variable name as a string
     * @param textFieldLength - maximum length of text field for proper formatting
     */
    @Deprecated
    @CompileStatic(TypeCheckingMode.SKIP)
    public void logVarWithFormat(final name, final int textFieldLength = this.defaultTextFieldLength) {
        //        //println "log var with format: " + name + ", instance of: " + name.getClass().getName()
        //        if (name instanceof Collection) {
        //            name.each { variable ->
        //                logVarWithFormat(variable, textFieldLength)
        //            }
        //        } else {
        //            try {
        //                if ((name instanceof String) && (binding.getVariable(name) instanceof Map)) {
        //                    gint.project.getPbinding.getVariable(name).keySet().each { variable ->
        //                        logWithFormat(name + '.' + variable, binding.getVariable(name)[variable], textFieldLength)
        //                    }
        //                } else {
        //                    def value = getValue(name, '')
        //                    logWithFormat(name, value, textFieldLength)
        //                }
        //            } catch(MissingPropertyException exception) {
        //                // logging error should just show message, not an exception
        //                message 'error', "${name} is not a global (in binding) variable. Make global or use logWithFormat instead."
        //            }
        //        }
    }

    /**
     * Same as <code>logWithFormat()</code> except respecting verbose setting
     */
    @Deprecated
    public void vLogVarWithFormat(final name, final int textFieldLength = this.defaultTextFieldLength) {
        if (isVerbose()) {
            logVarWithFormat(name, textFieldLength)
        }
    }

    /**
     * Get the attributes for an selenium web element - requires Selenium in classpath.
     * If driver is not provided, only some standard attributes are returned.
     * technique from http://stackoverflow.com/questions/11430773/how-to-use-javascript-with-selenium-webdriver-using-java
     * @param element
     * @return Map of attributes to values. Some or all attributes will be included depending on driver being available
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public getWebElementAttributes(final element, final driver = null) {
        def result
        if (driver != null) {
            result = driver.executeScript('var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;', element)
        } else {
            result = [
                type: element.getAttribute('type') ?: '',
                id: element.getAttribute('id') ?: '',
                name: element.getAttribute('name') ?: '',
                class: element.getAttribute('class') ?: '',
                style: element.getAttribute('style') ?: '',
                value: element.getAttribute('value') ?: '',
                href: element.getAttribute('href') ?: '',
                text: element.getAttribute('text'),
                selected: element.getAttribute('selected'),
                disabled: element.getAttribute('disabled'),
            ]
        }
        // special values we add/update
        result += [
            text: result.text == null ? element.getText() : '',
            selected: result.selected ?: false,
            disabled: result.disabled ?: false,
        ]
        return result
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, String> getWebElementAttributesNoExceptions(final element, final driver = null) {
        try {
            getWebElementAttributes(element, driver)
        } catch (Exception exception) {
            if (isVerbose()) {
                message 'error', 'Ignore error accessing web element: ' + element + '. Error: ' + exception
            }
            return [:]
        }
    }

    /**
     * Adds non-null, non-blank entry to a set or list, otherwise ignored - this is used to collect non-duplicate entries
     * @param entry to add
     * @param list or set
     * @return list
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public def addEntryToList(final entry, final list) {
        if ((entry != null) && (entry != '')) {
            list.add(entry)
        }
        return list
    }

    /**
     * Convert list of strings to a string with entries separated by separator character
     * @param list - list of strings - can be null or empty
     * @param separator - string used to separate list entries, defaults to comma (with blank) separated
     * @param emptyResult - what is returned for a null or empty list, defaults to empty string
     * @param entryClosure - closure to apply to each entry prior to it being included in the separated list
     * @param pre - string to add to the start of the string returned
     * @param post - string to add to the end of the string returned
     * @return string representing a separated list
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String listToSeparatedString(final Map parameters) {
        def result = new StringBuilder()
        if ((parameters?.list == null) || (parameters.list.size() == 0)) {
            result.append(parameters.emptyResult ?: '')
        } else {
            def separator = (parameters?.separator != null) ? parameters.separator : ', '
            parameters.list.each { entry ->
                if (parameters.entryClosure != null) {
                    entry = parameters.entryClosure(entry) // run user supplied closure on the entry
                }
                result.append(separator).append(entry)
            }
            result.delete(0, separator.size()) // removes the first separator sequence
            if (parameters?.pre != null) {
                result.insert(0, parameters.pre)
            }
            if (parameters?.post != null) {
                result.append(parameters.post)
            }
        }
        return result.toString()  // .trim()
    }

    /**
     * Convert list of strings to a string with entries separated by separator character
     * @param list of strings - can be null or empty
     * @param separator - string used to separate list entries, defaults to comma (with blank) separated
     * @param emptyResult - what is returned for a null or empty list, defaults to empty string
     * @param escape - escape character to add before the separator
     * @return string representing a separated list
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public def listToSeparatedString(final list, final separator = ', ', final emptyResult = '', final escape = null) {
        return listToSeparatedString([list: list, separator: separator, emptyResult: emptyResult,
            entryClosure: escape ? { entry -> ((entry instanceof String) || (entry instanceof GString)) ? entry.replace(separator, escape + separator) : entry } : null ])
        /*
         if ((list == null) || (list.size() == 0)) {
         return emptyResult
         }
         def result = new StringBuilder()
         list.each { entry ->
         if (escape) {
         entry = entry.replace(separator, escape + separator)
         }
         result.append(separator).append(entry)
         }
         return result.toString().substring(separator.size())   // removes the first separator sequence
         */
    }

    /**
     * Convert a string into a list based on a separator regex.  Similar to string.split() except to a list
     * @param string - input
     * @param separatorRegex
     * @result - list of trimmed strings determined from the input string
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public List separatedStringToList(final string, final separatorRegex = ',', final convertToObject = false) {
        def list = []
        if (string != null) {
            def array = string.split(separatorRegex)
            array.each { entry->
                entry = entry.trim()
                if (convertToObject) { // convert string representations of integers and booleans to real objects
                    if (entry.isInteger()) {
                        list << Integer.parseInt(entry)
                    } else if (entry.equalsIgnoreCase('false')) {
                        list << false
                    } else if (entry.equalsIgnoreCase('true')) {
                        list << true
                    } else {
                        list << entry  // always include the literal string
                    }
                } else {
                    list << entry
                }
            }
        }
        return list
    }

    /**
    * Get a list of value strings based on a comma separated list of colon separated (1 or 2 separators) values
    * using an end favored matching strategy optionally with either 1 or 2 qualifiers:
    *
    * 1 separators: Values are either
    * -  1 part name
    * -  2 part name where first part matches qualifier1 parameter and second part is the value
    # -  3 or more part name is same as 2 part name ignoring 3 and subsequent parts
    *
    * 2 separators: Values are either
    * -  1 part name
    * -  2 part name where first part matches qualifier2 parameter and second part is the value
    # -  3 part name where first part matches qualifier1 parameter, second part matches qualifier2, and third part is the value
    # -  4 or more part name is same as 3 part name ignoring 4 and subsequent parts
    *
    * Use single quote for strings containing separators, but we ONLY recommend using relatively simple names
    * for tasks, scripts, suites to avoid problems and especially avoid comma and colons.
    * @param - map with entries
    *   stringList: comma separated list of strings, strings may be colon separated
    *   qualifier1: first qualifier for a 1 or 2 part qualification
    *   qualifier2: second qualifier if a 2 part qualification, set to null to fall back to a 1 part qualification
    */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected List<String> getListFromQualifiedStringList(final Map map) {
        List<String> list = []
        //log('map', map)
        if (map.stringList != null) {
            // split comma separated list
            csvDataAsList(data: map.stringList, quote: "'").each { entry ->
                def (v1, v2, v3) = csvDataAsList(data: entry, delimiter: ':', quote: "'")
                //def (v1, v2) = entry.tokenize(':')
                //log('v1', v1); log('v2', v2); log('v3', v3);
                if (v1 != null) {
                    v1 = v1.trim()
                    if (v2 == null) {                 // 1 part name only
                        list << v1                    // always included
                    } else {                          // >1 part name
                        if (v3 == null) {             // 2 part name only
                            if (map.qualifier2 == null) {
                                if (v1 == map.qualifier1) {
                                    list << v2
                                }
                            } else if (v1 == map.qualifier2) {   // first matches 2nd valid qualifier
                                list << v2            // include
                            }
                        } else {                      // 3 part name
                            if (
                                     (v1 == map.qualifier1 && v2 == map.qualifier2) // both qualifiers match
                                  || (v1 == '' && v2 == map.qualifier2) // first missing and second matches
                                  || (v2 == '' && v1 == map.qualifier1) // second missing and first matches
                               ) {
                                list << v3            // include
                            }
                        }
                    }
                }
            }
        }
        //log('list', list)
        return list; // could be empty
    }

    /**
     * Convert map of strings to a string with entries separated by separator character
     * @param map of strings - can be null or empty
     * @param keyValuehandling - string used to separate key and value for an entry or closure that returns a string given key and value
     * @param separator - string used to separate entries, defaults to &
     * @param emptyResult - what is returned for a null or empty list, defaults to empty string
     * @return string representing a separated list
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String mapToSeparatedString(final Map map, final separator = '&', final keyValueHandling = '=', final emptyResult = '') {
        if ((map == null) || (map.size() == 0)) {
            return emptyResult
        }
        def result = new StringBuilder()
        def keyValueClosure = keyValueHandling instanceof Closure ? keyValueHandling : { key, value -> key + keyValueHandling + value}
        map.each { key, value ->
            if (value instanceof Closure) {  // needed to support late evaluation of values
                value = value.call()
            }
            result.append(separator).append(keyValueClosure(key, value))
        }
        return result.toString().substring(separator.size())   // removes the first separator sequence
    }

    /**
     * Search a list of maps for the first entry who's key matches the value
     * @param list - list of maps to search
     * @param key - key to use for the lookup
     * @param value - value to match
     * @return - map entry that satisfies the match or null
     */
    static public def findMapEntry(final Collection<Map> list, final key, final value) {
        for (entry in list) {
            if (entry[key] == value) {
                return entry
            }
        }
        return null
    }

    /**
     * Find first instance of value in the map. Should work no matter what object type, but tested with Strings
     * @param map - map of key value pairs
     * @param value - value to look for
     * @return key - first key that has value
     */
    static public def findFirstKeyWithValue(final Map map, final value) {
        def result = null
        for (def i = map.keySet().iterator(); (result == null) && i.hasNext(); ) {
            def key = i.next()
            if (map.get(key) == value) {
                result = key
            }
        }
        //println "Find key for ${value}: $result"
        return result
    }

    /**
     * Load properties from a file
     * @param propertyFile - either File or string representing file name
     * @param inProperties - existing properties to load into (replace)
     * @param ignoreError - true to eat the exception and put out message only
     * @return properties that were loaded
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Properties loadProperties(final propertyFileName, final Properties inProperties = null, final ignoreError = true) {
        return loadProperties(new File(propertyFileName + (propertyFileName.indexOf('.') > 0 ? '' : '.properties')), inProperties, ignoreError)
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    public Properties loadProperties(final File propertyFile, final Properties inProperties = null, final ignoreError = true) {
        def properties = inProperties ?: new Properties()
        if (propertyFile.exists()) {
            try {
                properties.load(new FileInputStream(propertyFile))
            } catch (IOException exception) {
                if (!ignoreError) {
                    throw exception
                }
                message 'warning', "Ignore error loading properties from ${propertyFile}"
            }
        }
        return properties
    }

    /**
     * Store properties
     * @param propertyFile - either File or string representing file name
     * @param properties to store
     * @param ignoreError - true to eat the exception and put out message only
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void storeProperties(final propertyFile, final Properties properties, final ignoreError = true) {

        //log('property file', propertyFile)
        try {
            properties.store(new FileOutputStream(propertyFile), null)
        } catch (IOException exception) {
            if (!ignoreError) {
                throw exception
            }
            message 'warning', "Ignore error storing properties to ${propertyFile}. " + exception.toString()
        }
    }

    /**
     * Verify all search entries are found in at least one target entry
     * - recurses on the first parameter if it is a list of ..., until it finds a non list, same for target entries
     * @param search - string/pattern/closure or list of strings/patterns/closures to search for
     * @param target string or list of strings or map with ordered and/or unordered lists. What we are searching in.
     * @param pattern flag - see JAVA pattern, use Pattern.CASE_INSENSITIVE for instance
     * @param ordered - search the target data in order
     * @param logFailure - set to true to display an info message
     * @param logSuccess - set to true to display an info message on success
     * @result true if all data items are found in at least one list entry
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public boolean verifySearchInTarget(final Map parameters) {
        def result = true
        //log('parameters', parameters)
        //log('search class', parameters?.search?.getClass()?.getName())
        //log('target class', parameters?.target?.getClass()?.getName())
        if (parameters?.search instanceof Closure) {  // map potentially with ordered and unordered lists
            return verifySearchInTarget(parameters + [search: getValueHandleClosure(parameters.search, parameters.firstParameter)])
        }
        if (parameters?.target instanceof Closure) {  // map potentially with ordered and unordered lists
            return verifySearchInTarget(parameters + [target: getValueHandleClosure(parameters.target, parameters.firstParameter)])
        }
        if (parameters?.search instanceof Map) {  // map potentially with ordered and unordered lists
            def map = parameters.clone()
            map.ordered = true
            map.search = parameters.search.ordered
            result = verifySearchInTarget(map)
            if (result && (parameters.search.unordered != null)) {
                map.ordered = false
                map.search = parameters.search.unordered
                result = verifySearchInTarget(map)
            }
        } else  if (parameters?.target instanceof Collection) {
            result = false
            def map = parameters.clone()
            for (targetEntry in parameters.target) { // not using closure so an early return is possible
                map.target = targetEntry
                result = verifySearchInTarget(map)
                //result = verifySearchInTarget(search: parameters.search, target: targetEntry, patternFlag: parameters.patternFlag)
                //message 'debug', "$result, $searchEntry, $target, $patternFlag"
                if (result) {  // once found we are done
                    if (parameters.logSuccess == true) {
                        println ''
                        message 'info', Constants.RED + "Found${(parameters.search instanceof Pattern) ? ' pattern:' : ':'} ${parameters.search}" + Constants.NO_COLOR
                    }
                    return true
                }
            }
        } else if (parameters?.search instanceof Collection) {
            def map = parameters.clone()
            for (searchEntry in parameters.search) {
                map.search = searchEntry
                if (!verifySearchInTarget(map)) {
                    if (parameters.logFailure == true) {
                        // GINT-89: if searchEntry is a closure that evaluates to a pattern we can't give the correct message since we are not going to re-execute the closure
                        println ''
                        message 'info', Constants.RED + "Not found${(searchEntry instanceof Pattern) ? ' pattern:' : ':'} ${searchEntry}" + Constants.NO_COLOR
                        if (map.ordered == true) {
                            def endIndex = [
                                200,
                                map?.workingTarget?.size()
                            ].min()
                            message 'info', Constants.RED + "Ordered search started at (${map?.workingTarget?.size()} characters remaining): " + Constants.NO_COLOR + map?.workingTarget?.substring(0, endIndex)
                            message 'info', 'End of ordered search message'
                        }
                    }
                    return false // any single failure is a failure for all
                }
                dLog('map', map)
            }
        } else {  // neither target or search are lists
            result = stringContains(parameters) // can't use a clone here since we need the workingTarget
            if (parameters?.logSuccess == true) {
                println ''
                message 'info', Constants.RED + "Found${(parameters.search instanceof Pattern) ? ' pattern:' : ':'} ${parameters.search}" + Constants.NO_COLOR
            }
        }
        return result
    }

    /**
     * Verify all search entries are NOT found in any target entry
     * - recurses on the first parameter if it is a list of ..., until it finds a non list, same for target
     * @param target string/pattern/closure or list of strings/atterns/closures or map with ordered and/or unordered lists. What we are searching in.
     * @param search - string/patterns or list of string/patterns to search for
     * @param pattern flag - see JAVA pattern, use Pattern.CASE_INSENSITIVE for instance
     * @param logFailure - set to true to display an info message
     * @result true if all search items are not found in at any target entry
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public boolean verifySearchNotInTarget(final Map parameters) {
        def result = true
        def map = parameters.clone()

        if (parameters?.search instanceof Map) {  // map potentially with ordered and unordered lists
            if (parameters.search.ordered != null) {
                map.ordered = true
                map.search = parameters.search.ordered
                result = verifySearchNotInTarget(map)
            }
            if (result && (parameters.search.unordered != null)) {
                if (parameters.search.ordered != null) {
                    map = parameters.clone()  // need a fresh start
                }
                map.ordered = false
                map.search = parameters.search.unordered
                result = verifySearchNotInTarget(map)
            }
        } else if (parameters.search instanceof Collection) {
            if (parameters.ordered == true) { // search must satisfy ordering constraint
                //map.logSuccess = (parameters.logFailure == true)
                map.logFailure = (parameters.logSuccess == true)

                result = !verifySearchInTarget(map)  // want to do ordered search
            } else {
                for (searchEntry in parameters.search) {
                    map.search = searchEntry
                    if (!verifySearchNotInTarget(map)) {
                        if (parameters.logFailure == true) {
                            message 'info', "Found${(searchEntry instanceof Pattern) ? ' pattern:' : ':'} ${searchEntry}"
                            if (map?.target instanceof String && searchEntry instanceof String) {
                                def index = map.target.indexOf(searchEntry)
                                if (index != null) {
                                    def lower = [0, index - 80].max()
                                    def upper = [
                                        index + searchEntry.size() + 80,
                                        map.target.size()
                                    ].min()
                                    message 'info', 'Context: ' + map.target.substring(lower, upper)
                                }
                            }
                        }
                        return false // any single failure is a failure for all
                    }
                }
            }
        } else { // search is NOT a collection (so ordered doesn't matter)
            if (parameters.search != null) {
                //map.logSuccess = (parameters.logFailure == true) // prevent failData entries being logged
                map.logFailure = (parameters.logSuccess == true)
                result = !verifySearchInTarget(map)
            }
        }
        //println "not contain = ${result}:\n search: ${map.search}, \n target: ${map.target}"
        return result
    }

    /**
     * Enables a case insensitive version of String contains: s1.contains(s2)
     * - other pattern matching flags could also be used
     * @param target string to search - toString is used to ensure it is a string
     * @param search string or (regex) pattern to find - example regex pattern is ~/is *a/ or ~/(?i)is *a/ for case insensitive
     * @param pattern flag - see JAVA pattern, use Pattern.CASE_INSENSITIVE for instance ONLY when s2 is a string!
     * @return true if s2 is found in s1 under the conditions specified by patternFlag (s2 string) or regex pattern (s1 pattern)
     */
    static public boolean stringContains(final target, final search, final Integer patternFlag = null) {
        return stringContains(target: target, search: search, patternFlag: patternFlag)
    }

    /**
     * Enables a case insensitive version of String contains: s1.contains(s2) as well as general pattern matching
     * - other pattern matching flags could also be used
     * - note that the incoming map will have workingTarget added/changed
     *   -- if target is closure, only evaluated once
     *   -- if ordered is used, it will get updated with the substring value for later searching
     * @param target string to search in - toString is used to ensure it is a string
     * @param search string or (regex) pattern to find - example regex pattern is ~/is *a/ or ~/(?i)is *a/ for case insensitive
     * @param pattern flag - see JAVA pattern, use Pattern.CASE_INSENSITIVE for instance ONLY when s2 is a string!
     * @return true if s2 is found in s1 under the conditions specified by patternFlag (s2 string) or regex pattern (s1 pattern)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public boolean stringContains(final Map map) {
        boolean result = true
        //println "target: ${map?.target}, search: ${map?.search}, patternFlag: ${map?.patternFlag}, ordered: ${map?.ordered}"
        if (map?.search != null) {
            if (map.workingTarget == null) {
                map.workingTarget = ((map.target instanceof Closure) ? map.target.call() : map.target).toString()
            }
            def convertedSearch = ((map.search instanceof Closure) ? map.search.call() : map.search)
            def pattern = (convertedSearch instanceof Pattern) ? convertedSearch : Pattern.compile(convertedSearch.toString(), ((map.patternFlag != null) ? map.patternFlag : Pattern.LITERAL))
            def matcher = pattern.matcher(map.workingTarget)  // save the matcher for subsequent calls
            result = matcher.find()
            if (result && (map.ordered == true)) {
                map.workingTarget = map.workingTarget.substring(matcher.end()) // now only go from this point on
                //println "substring working target at: ${matcher.end()}, ${map.workingTarget}"
            }
        }
        return result
    }

    /**
     * Generate a list of a specific map attribute (ignoring nulls)
     * Example: [[name: 'x'], [name, 'y']] -> ['x', 'y'] using attribute name
     * @param listOfMaps to search for attribute
     * @param attribute to search for
     * @result list of values, one for each map in the list that has a non-null value for the attribute
     */
    static public List generateListFromListOfMaps(final List<Map> listOfMaps, final attribute) {
        def result = []
        listOfMaps.each { entry ->
            def value = entry.get(attribute)
            if (value != null) {
                result.add(value)
            }
        }
        return result
    }

    /**
     * Are we running on Linux?
     * @return true if running on linux
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public boolean isLinux() {
        return System.properties.'os.name'.contains('Linux')
    }

    /**
     * Are we running on windows?
     * @return true if running on windows
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public boolean isWindows() {
        return System.properties.'os.name'.contains('Windows')
    }

    /**
     * Are we running on MacOs?
     * @return true if running on macOS
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public boolean isMacOs() {
        return System.properties.'os.name'.contains('Mac')
    }

    /**
     * Get the OS platform we are running with a simplified name
     * @return linux, windows, macos, or unknown
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String getOsPlatform() {
        isLinux() ? 'linux' : isMacOs() ? 'macos' : isWindows() ? 'windows' : System.getProperty('os.name')
    }

    /**
     * Get friendly version of OS architecture like amd64, arm64 ...
     */
    def getOsArch() {
        def arch = System.getProperty('os.arch')
        if (arch == 'x86_64') {
            arch = 'amd64'
        } else if (arch == 'aarch64') {
            arch = 'arm64'
        }
        return arch
    }

    /**
     * Handled problems on windows with blanks in file names
     * @param name - name to quote
     * @param double quoted name if it contains a blank on windows, otherwise leave alone
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected def windowsQuoteName(final name) {
        return (isWindows() && name.contains(' ')) ? quoteString(name, '"') : name
    }

    /**
     * Escape $ on non-Windows systems to be command line friendly
     * @param text to escape
     */
    String dollarEscape(final String text) {
        return (isWindows() ? text : text.replace('$', '\\$'))
    }

   /**
     * Escape regex special characters in a string
     * @param text to escape
     * null string return null
     */
    static public String escapeRegex(String string) {
        final String specialCharacters = "([\\^\\/\\.\\*\\+\\?\\|\\(\\)\\[\\]\\{\\}\\\\])" // double escape all regex special
        return string?.replaceAll(specialCharacters, '\\\\$1') // double escape \ since it is a regex special character
    }

    /**
     * Command separator based on operating system
     * @param continueOnError - true to get command separator appropriate for continue to run commands even if earlier command failed
     * @return command separator appropriate for the OS
     */
    static public String getCommandSeparator(continueOnError = false) {
        return (continueOnError ? (isWindows() ? ' & ' : ' ; ') : ' && ')
    }

    /**
     * Get time in seconds
     * @return time in seconds
     */
    static public long getTimeInSeconds() {
        return (long) (System.currentTimeMillis()/1000)
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    public void logTimestamp(final String prefix = '') {
        message 'info', prefix + 'timestamp: ' + System.currentTimeMillis()
    }

    public void vLogTimestamp(final String prefix = '') {
        if (isVerbose()) {
            logTimestamp(prefix)
        }
    }

    /**
     * Format a time duration in seconds to hh:mm:ss
     * @param seconds representing some time duration
     * @return formated in hours:minutes:seconds
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String formatTimeDuration(final int seconds) {
        def remaining = seconds
        def hours = (int) remaining/3600
        remaining -= hours * 3600
        def minutes = (int) remaining/60
        remaining -= minutes * 60
        return "${hours.toString().padLeft(2,'0')}:${minutes.toString().padLeft(2,'0')}:${remaining.toString().padLeft(2, '0')}"
    }

    /**
     * Run a command or list of commands. Named parameters (map)
     * @param cmd - command to run or a list of commands
     * @param log - true to log the command and results (default is true)
     * @param logOutput - true to log output and error output automatically (default is true)
     * @param continueOnError - true so processing (of a list) will not fail on failure of command (default is false)
     * @param inParallel - to have list of commands run in parallel
     * @param returnParallel - true to return a parallel object for each command (default is false) - instead of true/false
     * @param standardInput - string sent as standardInput to the command process
     * @return default is to return true if all commands were successful or false otherwise.
     * Use the returnParallel option to return either a parallel object or a list of parallel objects corresponding to each cmd run in parallel
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def runCmd(final Map parameters) {
        def result = true
        def pResult = null
        if (parameters != null) {
            def map = [
                log: parameters.log != false,
                standardInput: parameters.standardInput,
            ]
            if ((parameters.inParallel == true) && (parameters.cmd instanceof Collection)) {
                pResult = []
                parameters.cmd.each { cmd ->
                    if (cmd instanceof Collection) {
                        pResult << gint.parallelHelper.shell(map, cmd, parameters.continueOnError == true)
                    } else {
                        pResult << gint.parallelHelper.shell(map, cmd)
                    }
                }
                pResult.each { p ->
                    p.end(parameters.logOutput != false)
                    result = result && (p.getReturnCode() == 0)
                }
            } else {
                if (parameters.cmd instanceof Collection) {
                    pResult = gint.parallelHelper.shell(map, parameters.cmd, parameters.continueOnError == true)
                } else {
                    pResult = gint.parallelHelper.shell(map, parameters.cmd)
                }
                pResult.end(parameters.logOutput != false)
                result = (pResult.getReturnCode() == 0)
            }
        }
        return (parameters.returnParallel) ? pResult : result
    }

    /**
     * Generate a command string from the command strings in a list. Use the proper command separators depending on options and OS
     * @param list - list of commands
     * @param continueOnError - true to run remaining commands after a command fails, the default is true to stop after an error
     * @param useWindowsSyntax - true to use Windows command separator if necessary. Default will figure out if running on Windows. It only differs if continueOnError is true
     * @return single command string (perhaps an empty string)
     */
    public String generateCommandString(final Collection list, final continueOnError = false, final useWindowsSeparator = null) {
        def builder = new StringBuilder()
        def commandSeparator = (continueOnError ? ((useWindowsSeparator == true) || ((useWindowsSeparator == null) && isWindows()) ? ' & ' : ' ; ') : ' && ')
        list?.each { cmd ->
            builder.append commandSeparator  // substring will ignore first one later
            builder.append cmd
        }
        def string = builder.toString()
        return (string.size() >= commandSeparator.size() ? builder.toString().substring(commandSeparator.size()) : '') // remove extra leading separator
    }

    /**
     * Profiles are represented by property files. The list is read in order and processed. Due to the nature of (immutable) properties,
     * the first setting of a property cannot be changed by later settings. Reverse the list if you want the last setting to hold instead.
     * @param profileList is a list or set of profile names
     * @param directoryList is a list of directories used to search for profiles based on profile name plus extension .properties. The
     *          first directory containing the profile is used
     * @return true if all profiles requested were loaded successfully
     */
    public boolean loadProfile(final profile, final List directoryList = ['.']) {
        if (profile instanceof Collection) {
            return loadProfileInternal(profile, directoryList)
        }
        return loadProfileInternal([profile.toString()], directoryList)
    }

    /**
     * Same as <code>loadProfile</code> with handling of lists.
     * Synchronize so that multiple threads loading profiles don't get messed up with error messages.
     * @param profileList
     * @return true if all profiles requested were loaded successfully, false is any one of them failed
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected synchronized boolean loadProfileInternal(final profileList, final List directoryList = ['.']) {
        boolean result = true
        profileList.each { profile ->
            profile = profile.trim()
            if (profile != '') {   // ignore blank names
                boolean loaded = false
                //message 'debug', "Load profile: ${profile}"
                def existingCodePropertyList = getCodePropertyList() // already evaluated names whose entries end in '_code'
                for (String directory : directoryList) {
                    def fileName = profile + '.properties'
                    File file = new File(directory, fileName)
                    //message 'debug', "Load profile from file: ${file.getAbsolutePath()}"
                    if (file.exists()) {
                        loaded = binding.ant.property(file: "${directory}/${profile.trim() + '.properties'}")
                        //message 'debug', "Properties: ${binding.ant.project.properties}"
                        if (loaded) {
                            // get list of entries ending in '_code' that will be evaluated as a groovy code, ignore already loaded names
                            def codePropertyList = getCodePropertyList(existingCodePropertyList)
                            //message 'debug', 'New code properties: ' + codePropertyList
                            //message 'debug', 'Existing code properties: ' + existingCodePropertyList
                            existingCodePropertyList += codePropertyList
                            if (!evaluateAsCode(codePropertyList, file)) {
                                result = false
                            }

                        } else {
                            message 'error', 'Error loading: ' + file.getAbsolutePath()
                            result = false
                        }
                    } else {
                        // file not found - that is ok generally
                        vMessage 'warning', 'Profile not found: ' + file.getAbsolutePath()
                    }
                    if (loaded) {
                        break  // break on first loaded profile
                    }
                }
                if (!loaded) {
                    message 'error', profile.trim() + ' profile failed to load using directory list: ' + directoryList
                    result = false
                }
            }
        }
        return result
    }

    /**
     * Get a list of names such that there is a project property with name + GROOVY_CODE_ENDING
     * @param ignoreList - ignore any names in this list
     * @return list of names (properties stripped of GROOVY_CODE_ENDING)
     */
    protected List getCodePropertyList(final ignoreList = []) {
        def nameList = []
        gint.project.ant.properties.each { String key, value ->
            if (key.endsWith(Constants.GROOVY_CODE_ENDING)) {
                def name = key.substring(0, key.lastIndexOf(Constants.GROOVY_CODE_ENDING))  // just add the real name to the list
                if (!(name in ignoreList)) {
                    nameList << name
                }
            }
        }
        return nameList
    }

    /**
     * Handle a list of variable names that need to be evaluated as code.
     * Handle out of order references by retrying when there is a MissingPropertyException.
     * @param list of variable names (without code ending)
     * @return true if all evaluations were successful, false if any one evaluation failed
     */
    protected boolean evaluateAsCode(List list, final File file) {
        def result = true
        def lastSize = list.size() + 1  // bigger than total list size
        def doAgainList = []
        while ((lastSize > 0) || ((list != null) && (list.size() > 0))) {
            list.each { name ->
                if (!evaluateAsCode(name?.toString(), doAgainList, file)) {
                    result = false  // at least one failed
                }
            }
            lastSize = list ? list.size() : 0
            list = doAgainList
            //message 'debug', "Do again: ${doAgainList}. Last: ${lastSize}, Now: ${list?.size()}"
            if (list == null) {
                break
            }
            if ((list != null) && (lastSize == list.size())) {
                doAgainList = null // let normal exception handling show errors
            } else {
                doAgainList = []
            }

        }
        return result
    }

    /**
     * Handle a single variable that needs to be evaluated as code
     * @param name - name of variable (without code ending)
     * @param doAgainList - list that the name will be added to if code evaluation resulted in MissingPropertyException
     * @return true if successful evaluation or name added to doAgainList, false if not
     */
    protected boolean evaluateAsCode(final String name, final List doAgainList, final File file) {
        def result = true
        // name of property that will be set
        def value = getValue(name + Constants.GROOVY_CODE_ENDING)
        if (value != null) {
            for (int i = 0; i < 2; i++) { // loop twice only to cover out of memory error
                try {
                    //message 'debug', "Evaluate: ${name}"
                    setProperty(name, Eval.me(value.toString()))
                    break
                } catch (MissingPropertyException exception) {
                    //message 'debug', "Missing property for ${name}: ${exception}"
                    if (doAgainList == null) {
                        evaluationError(name, file, exception)
                        result = false
                    } else {
                        doAgainList << name
                    }
                    break
                } catch (Exception exception) {
                    evaluationError(name, file, exception)
                    result = false
                    break
                }
            }
        } else {
            evaluationError(name, file)
            result = false
        }
        return result
    }

    /**
     * Evaluation error message
     * @param name - name of variable (without code ending)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected void evaluationError(final String name, final File file, final exception = null) {
        setProperty(name + Constants.GROOVY_CODE_ENDING, null)
        setProperty(name, null)
        message 'error', "Error evaluating property: ${name} from ${name + Constants.GROOVY_CODE_ENDING} in file: ${file.getAbsolutePath()}"
        if (exception) {
            message 'error', exception.toString()
        }
    }

    /**
     * Check for null or blank when trimmed as a string
     * @param string
     * @return true if value is not null and not blank when trimmed as a string
     */
    static public boolean isBlank(final string) {
        return (string == null) || (string.toString().trim() == '')
    }

    /**
     * Check for not null and not blank when trimmed
     * @param string
     * @return true if value is not null and not blank when trimmed as a string
     */
    static public boolean isNotBlank(final string) {
        return (string != null) && (string.toString().trim() != '')
    }

    /**
     * Ensure that a non-null List is returned
     * @param value - object or list
     * @return value if already a list, otherwise return a list with 0 entries if value is null or 1 entry of value
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Collection getAsCollection(final value) {
        if (!(value instanceof Collection)) {
            def result = []
            if (value != null) {
                result << value
            }
            return result
        }
        return value
    }

    /**
     * Splits the data into meaningful strings to produce a 1 dimensional array. Handles
     * delimiters within quoted strings. This is mostly common with a JAVA version, so make it JAVA like as much as possible
     * Notes:
     * <ul>
     * <li> CSV is not a formal standard, but the best reference is The Comma Separated Value (CSV) File Format (http://www.creativyst.com/Doc/Articles/CSV/CSV01.htm)
     * <li> Another reference is http://tools.ietf.org/html/rfc4180, but in our implementation whitespace is trimmed unless it is quoted - see first reference !!!!
     * <li> This support tries to follow this standard with the flexibility of multiple delimiters
     * <li> special values for delimiter are whitespace, blank or blanks, tab, and pipe
     * <li> eolString is left in the last string of a row so outside code can distinguish rows (use value.endsWith(eolString) to find row ends)
     * <li> nothing is trimmed from the strings, users may want to trim the output string
     * <li> a leading delimiter (except for blank, whitespace, tab, and complex delimiters) for a line causes a blank value entry
     * </ul>
     * String patternString = "(?:^|,)(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))"
     *                         <-----><--<------------------> <---------->>
     *                            1     2         3          4     5
     * http://www.regular-expressions.info/lookaround.html
     * 1 - start of line or ,
     * 2 - positive lookahead 3 (multiple times) followed by 5
     * 3 - not quote (multiple times) quote then not quote (multiple times) quote        xxx"xxx"
     * 4 - multiple times (blocks of even numbered quotes in strings)                    xxx"xxx"xxx"xxx"
     * 5 - negative lookahead not quote (multiple times) quote (odd number)              xxxxx"
     * Examples
     *     ,xxxx"xxxxx"yyyyy", - no  not an even number of "
     *     ,xxxx"xxxxx"yyyy,   - yes even number of "
     *
     * @param data - string to split
     * @param file - file to read data from if data is null
     * @param fileName - file to read data from if data, file are null
     * @param delimiter - what to split on (in regex compatible format!!!!) or special values of blank, blanks, tab, whitespace, pipe
     * @param quote - what is used to quote strings that may contain delimiter, usually double quote
     * @param eolString - how end of line is determined
     * @return list of strings, trimmed with enclosing quotes removed, but preserves ending eol character
     */
    static public List<String> csvDataAsList(final Map parameters) {
        List<String> values = new ArrayList<String>()
        if (parameters == null) {
            return values
        }
        String data = parameters.data ?: (parameters.file instanceof File) ? readFile(parameters.file, parameters.encoding) : parameters.fileName ? readFile(parameters.fileName, parameters.encoding) : ""
        String delimiter = parameters.delimiter ?: ','
        String quote = parameters.quote ?: '"'
        String eol = parameters.eol ?: '\n'

        boolean blankWhenLineStartsWithDelimiter = true  // when true, leading delimiter should be considered a empty value
        if ((delimiter.length() == 1) && (delimiter != " ")) {  // the most common case
            //
        } else if (delimiter.equalsIgnoreCase("whitespace")) {
            delimiter = "[ \\t\\x0B\\f]+" // whitespace, \s+ does not work here !
            blankWhenLineStartsWithDelimiter = false
        } else if (delimiter.equalsIgnoreCase("blank") || delimiter.equalsIgnoreCase("blanks") || delimiter.equals("") || delimiter.equals(" ")) {
            delimiter = " +" // one or more blanks (tabs DON'T count - use whitespace if you need tabs!)
            blankWhenLineStartsWithDelimiter = false
        } else if (delimiter.equalsIgnoreCase("tab")) {
            delimiter = "\\t" // a single tab character
            blankWhenLineStartsWithDelimiter = false
        } else if (delimiter.equalsIgnoreCase("pipe")) {
            delimiter = "\\|"
            // handle a quoted single character
        } else if ((delimiter.length() == 3) && (delimiter.substring(0, 1).equals(quote)) && (delimiter.substring(2, 1).equals(quote))) {
            delimiter = delimiter.substring(1, 2)  // remove quotes
        }

        String patternString = "(?:^|" + delimiter + ")(?=(?:[^" + quote + "]*" + quote + "[^" + quote + "]*" + quote + ")*(?![^" + quote + "]*" + quote + "))"

        // If a line starts with delimiter, need to insert a space to have it handled properly
        if (blankWhenLineStartsWithDelimiter) {
            Pattern pattern = Pattern.compile(eol + delimiter)
            Matcher matcher = pattern.matcher(data)
            data = matcher.replaceAll(eol + " " + delimiter)
            if (delimiter.equals("\\|") || delimiter.equals("\\*") || delimiter.equals("\\+")) {  // a single escaped character | * or +
                delimiter = delimiter.substring(1) // use just the escaped character
            }
            if (data.startsWith(delimiter)) {  // handle first line which may not start with eolString, for normal delimiters (non-regex)
                data = " " + data
            }
        }

        // now do main parsing
        Pattern pattern = Pattern.compile(patternString, Pattern.MULTILINE)

        // Because of http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=5050507, need to limit looking forward to prevent stack overflow
        // so do the regex in chunks determined by limit size and finding a convenient break point (eol)
        // Do normal splitting, but add the last entry to the next chunk in the hopes we can not mess things up too much
        int limit = 5000  // where to start looking for natural split point, should be < 7K (noticed limit for Sun JDK on Windows)

        int endIndex = 0

        while (endIndex < data.length()) {
            int startIndex = endIndex
            if (data.length() < startIndex + limit) {
                endIndex = data.length() // normal case for small csv requests, go directly to splitting
            } else { // look for eol where the there is an even number of quotes prior to it
                boolean evenQuote = true
                for (endIndex = startIndex; endIndex < data.length(); endIndex++) {
                    if (data.charAt(endIndex) == quote) {
                        evenQuote = !evenQuote
                    }
                    if (evenQuote && (data.charAt(endIndex) == eol)) {
                        endIndex++
                        break
                    }
                }
            }
            String[] strings = pattern.split(data.substring(startIndex, endIndex))
            // this parse technique has some anomalies, so repair those now
            int startPosition = 0
            if ((strings.length > 1) && (strings[0].equals(""))) { // if more than one entry found, then first entry is blank and needs to be ignored
                startPosition = 1
            }
            for (int i = startPosition; i < strings.length; i++) {
                values.add(getCsvDataElement(strings[i], quote, eol)) // as specified in reference csv standard
            }
            // System.out.println("start: " + startIndex + ", end: " + endIndex + ", length: " + strings.length); // debug
        }
        return values
    }

    /**
     * Same as <code>csvDataAsList</code>
     */
    static public List<String> csvDataAsList(final data, final String delimiter = ',', final String quote = '"', final String eolString = '\n') {
        return csvDataAsList([data: data, delimiter: delimiter, quote: quote, eolString: eolString])
    }

    /**
     * Same as <code>csvDataAsList</code> but reads the data from a file first.
     * @param file - file to be read for csv data
     * @return see <code>csvDataAsList</code>
     */
    static public List<String> csvDataAsList(final File file, final delimiter = ',', final quote = '"', final eolString = '\n') {
        return csvDataAsList([file: file, delimiter: delimiter, quote: quote, eolString: eolString])
    }

    /**
     * Given a raw data element after csv parsing, correct the data to conform to csv standards
     * <pre>
     * - ignore whitespace
     * - remove surrounding quotes (if it is quoted)
     * </pre>
     * @param data to convert
     * @param quote character
     * @param eolString used to determine end of line
     * @return processed data element
     */
    static public String getCsvDataElement(final String data, final String quote = '"', final String eol = Constants.EOL) {
        boolean isEol = data.endsWith(eol)
        //println "data size: ${data.size()}, eol: ${eol}, data: ${data}"
        String trimmed = data.trim()
        String stripped = stripQuotes(trimmed, quote)
        if (isEol) {
            stripped += eol  // must restore the ending eol so rows can be distinguished
        } else if ((stripped.size() != trimmed.size()) && stripped.endsWith(eol)) { // quotes were actually stripped and eol at end
            //stripped.substring() += ' '  // add blank so that this does not accidently look like an eol condition
            stripped = stripped.substring(0, stripped.length() - eol.length()) + " " // remove eolString so that this does not accidently look like an eol condition
        }
        return stripped
        //return stripQuotes(data.trim(), quote) + (eol ? eolString : ""); // must restore the ending eol so rows can be distinguished
    }

    /**
     * Similar to <code>csvDataAsList</code>, except returns a list of rows, each row is a list of the columns in the row
     */
    static public List<List<String>> csvDataAsListOfRows(final Map parameters) {
        String eolString = parameters.eolString ?: "\n"
        List<String> values = csvDataAsList(parameters)
        ArrayList<List<String>> result = new ArrayList<List<String>>() // list of rows
        ArrayList<String> list = new ArrayList<String>()  // for a single row of data
        boolean addToRowList = true  // used to ensure empty lists are not added
        values.each { value ->
            if (addToRowList) {
                result << list
                addToRowList = false
            }
            if (value.endsWith(eolString)) {
                list << value.substring(0, value.length() - eolString.length())
                list = new ArrayList<String>()
                addToRowList = true
            } else {
                list << value
            }
        }
        return result
    }

    /**
     * Convert a row list to a map list assuming the first row is a list of column names. This is primarily for csv like conversions.
     * <pre>
     * column1, column2, column3
     * r1c1, r1c2, r1c3
     * r2c1, r2c2, r2c3
     *
     * Gets converted to:
     *
     *  map[1].column1  . . . . . . . . . . . . . : r1c1
     *  map[1].column2  . . . . . . . . . . . . . : r1c2
     *  map[1].column3  . . . . . . . . . . . . . : r1c3
     *  map[2].column1  . . . . . . . . . . . . . : r2c1
     *  map[2].column2  . . . . . . . . . . . . . : r2c2
     *  map[2].column3  . . . . . . . . . . . . . : r2c3
     *  map[3].column1  . . . . . . . . . . . . . : r3c1
     * </pre>
     * @param rowList - a list of rows, each row is a list of strings
     */
    static public List<Map<String, Object>> convertRowListToMapList(final List<List<Object>> rowList) {
        List<Map<String, Object>> mapList = []
        for (int i = 1; i < rowList.size(); i++) {
            def map = [:]
            for (int j = 0; j < rowList[0].size(); j++) {
                if (j < rowList[i].size()) {
                    map[rowList[0][j].toString()] = rowList[i][j]
                }
            }
            mapList << map
        }
        return mapList
    }

    /**
     * Get a column from a multi-dimensional list - http://stackoverflow.com/questions/8041166/groovy-get-first-elements-in-multi-dimensional-array
     * @param list
     * @param index - 0 based index
     * @return list of index values
     */
    static public List<Object> getColumn(List<List<Object>> list, int index) {
        // Needs gradle 5.x for this logic. Use older logic for now
        //return list?.collect { entry ->
        //    return (index >= 0 && index < entry.size()) ? entry[index] : null
        //}
        List<Object> newList = null
        if ((index >= 0) && (list != null)) {
            newList = []
            list.each { entry ->
                newList.add(index < entry.size() ? entry[index] : null)
            }
        }
        return newList
    }

    /**
     * Convert 2 string lists into a map with list1 values as keys and list2 values
     * as values for the same relative position in the list
     * Example:  [key1,key2], [value1,value2] -> [key1: value1, key2: value2].
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public Map<String, String> generateMap(List<String> list1, List<String> list2) {
        [list1, list2].transpose().inject([:]) { a, b -> a[b[0]] = b[1]; a }
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    static public Map<String, String> generateMap(String[] list1, String[] list2) {
        [list1, list2].transpose().inject([:]) { a, b -> a[b[0]] = b[1]; a }
    }

    /**
     * Return the JVM process name, contains the PID, helpful for identifying running process.
     * For instance, use in long running Bamboo scripts that may need to be canceled reliably
     * See http://golesny.de/wiki/code:javahowtogetpid
     * Other JVM information that might be useful:
     * <ul>
     * <li> java.lang.management.ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().toString()
     * <li> java.lang.management.ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().toString()
     * </ul>
     * @return JVM process name
     */
    static public String getProcessName() {
        return java.lang.management.ManagementFactory.getRuntimeMXBean().getName()
    }

    /**
     * Get Pom object. See http://maven.apache.org/ant-tasks/apidocs/org/apache/maven/artifact/ant/Pom.html
     * @return pom
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def getPom(final pomFileName = 'pom.xml') {
        return gint.project.ant."antlib:org.apache.maven.artifact.ant:pom"(id: 'na_but_required', file: pomFileName)
    }

    /**
     * Standard jsap (http://martiansoftware.com/jsap/) parameter setting of: --name value
     * with proper handling of quotes.
     * Primary quote is ", however, jsap also supports ', so use this as an alternate where possible.
     * Note that the quoteReplace may be necessary depending on the data
     * - Windows command strings has some peculiarities with special windows characters like
     *   redirection operators (<, >) and other special characters (|, (, ), &, %).  As long as
     *   these characters are contained within a double quoted string, they are ignored as special
     *   characters.  This works fine normally for jsap parameters.  However, if the parameter
     *   contains an escaped double quote (\"), Windows gets confused and again starts considering
     *   special characters after the escaped quote. While this is a rare situation, the command
     *   will fail.
     * - Convert embedded double quotes to something like ^^^ instead of escaping it and then use the findReplace
     *   parameter to turn it back into a "
     * - The only negative with this is that valid ^^^ characters will be converted as well
     * @param name - parameter name
     * @param inValue - parameter value, usually a string but could be a list for jsap list based parameters
     * @param replaceQuote - for help with Windows problems with embedded quotes in strings, replace and convert back later using findReplace on cli
     * @param escape - default used on quoteString
     * @return string in jsap parameter notation with blanks at start/end to make it easier to concatenate
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public String jsapParameter(final name, final inValue, final replaceQuote = null, final char separator = ',', escape = null) {
        // handle quoting the value, but be careful with escaping
        // escape ending \ if quoting so it does not escape the quote - if it ends with more than one \, do nothing

        escape = escape ?: isWindows() ? '"' : '\\'  // default if not provided to OS specific value
        //System.out.println("escape: " + escape + ", name: " + name)

        def valueClosure = { value ->
            value = (replaceQuote != null) ? value?.replace('"', replaceQuote) : value
            if (value instanceof Closure) {
                value = value.call()
                // Check for JSONObject (simple json) so that the escaping is right - use string test to avoid object dependency
            } else if (value.getClass().getName().toLowerCase().endsWith('jsonobject')) {
                return quoteString(value.toString(), '"', '\\')  // JSON specific escaping
            }
            return ((value instanceof String) || (value instanceof GString)) ? quoteString(value + (value.endsWith("\\") && (!value.endsWith("\\\\")) ? '\\' : ''), '"', escape) : value
        }

        def result = '' // default to nothing
        if (inValue instanceof Set) { // Old (legacy) support
            if (inValue.size() > 0) {
                result = jsapParameter(name, listToSeparatedString([list: inValue, separator: ',', entryClosure: valueClosure]))
            } else {
                // omit the parameter since there are no values
            }
        } else if (inValue instanceof Collection) {
            if (inValue.size() > 0) {
                // value = listToSeparatedString([list: inValue, separator: ',', entryClosure: valueClosure])
                def builder = new StringBuilder()  // see GINT-180
                inValue.each() { string ->
                    builder.append(jsapParameter(name, string, replaceQuote))
                }
                result = builder.toString()
            } else {
                // omit the parameter since there are no values
            }
        } else if (inValue instanceof Map) {
            if ('env'.equalsIgnoreCase(name)) {
                // omit env parameter as that is a special value for setting environment variables for the process
            } else if (inValue.size() > 0) {
                def builder = new StringBuilder()  // see GINT-180
                inValue.each() { key, value ->
                    builder.append(jsapParameter(key, value, replaceQuote))
                }
                result = jsapParameter(name, builder.toString().trim())
            } else {
                // omit the parameter since there are no values
            }
        } else {
            def value = valueClosure(inValue)
            result = " ${name.size() == 1 ? '-' : '--'}${name} ${(value != null) ? value : ''}${(value != null) ? ' ': ''}"
        }
        return result
    }

    /**
     * Standard jsap parameter string based on key, value provided by the parameter map
     * @param parameters - use value null for a switch parameter (ie. --continue)
     * @return jsap based parameter string with multiple parameters
     */
    public String jsapParameter(final Map parameters, escape = null) {
        return mapToSeparatedString(parameters, '', { key, value -> jsapParameter(key, value, null, ',' as char, escape) })
    }

    /**
     * Remove a single parameter and value (if there is one) from the string. This doesn't cover all cases, but
     * should cover most normal cases. For instance it will not correctly remove a parameter with an embedded -.
     * @param parameter name or collection of parameter names
     * @param string from which the parameter will be removed
     * @return string with parameter removed if found or input string if not found
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public String removeJsapParameter(final parameterName, final string) {
        if (parameterName instanceof Collection) {
            def result = string
            parameterName.each { param -> result = removeJsapParameter(param, result) }
            return result
        } else {
            def separator = (parameterName.size() == 1) ? '-' : '--'
            def regex = /((?:.* )|(?:^)|(?: ))${separator}${parameterName}\s*[^-]*((?:-\S+.*)|(?:--\S+.*)|(?:\s*$))/
            //def regex = /(.*) ${separator}${parameterName}\s*((?: - .*)|(?: -- .*)).*$/
            //logWithFormat('regex', regex)
            def matcher = (string.toString() =~ regex)
            return matcher.matches() ? matcher.group(1) + matcher.group(2) : string
        }
    }

    /**
     * Given a pattern with a single group reference, do a find and return match group or null
     * @param data to search in
     * @param pattern - regex pattern with a single group
     * @return group(1) if find is successful, otherwise null
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public String getSingleFind(final String data, final Pattern pattern) {
        def matcher = data == null ? null : pattern.matcher(data)
        return (matcher?.find() ? matcher.group(1) : null)
    }

    /**
     * Recursively removes nulls from a map. Useful for using logWithFormat to remove un-interesting values
     * @param map
     * @return self (map with nulls removed)
     */
    static public Map removeNulls(Map map) {
        def keysToRemove = []
        map.each { key, value ->
            if (value == null) {
                keysToRemove << key
            } else if (value instanceof Map) {  // recurse on embedded maps
                removeNulls(value)
            }
        }
        keysToRemove.each { key ->
            map.remove(key)
        }
        return map
    }

    /**
     * Html encode special characters.
     * @param string
     * @return modified string
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String htmlEncode(string) {
        return string.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;').replace(/'/, '&#39;')
    }

    /**
     * Append the request parameters to the url string using appropriate connector
     * @param urlString
     * @param requestParameters - should not have leading connector
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String appendRequestParameters(def urlString, def requestParameters) {
        if (isBlank(requestParameters)) {
            return urlString
        }
        return urlString + (urlString.contains('?') ? '&' : '?') + requestParameters
    }

    /**
     * Download url to file. From  http://wordpress.transentia.com.au/wordpress/2010/02/08/copy-image-from-url-to-file/
     * @param Map with url and file
     * @return - true if file created and has a non-zero length
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public boolean downloadFile(final Map map) {
        assert map
        assert map.file
        if (isNotBlank(map.url)) {
            def File file
            if (map.file instanceof String) {
                file = new File(map.file)
            } else {
                assert map.file instanceof File
                file = map.file
            }
            file.withOutputStream { os ->
                new URL(map.url).withInputStream { is -> os << is }
            }
            log('url', map.url)
            log('file', file.getAbsolutePath())
            log('file size', file.length())
            return file.length() > 0
        } else {
            message 'warning', 'url was blank'
            return false
        }
    }

    /**
     * Generate a text string made up of a subset of the first and last characters of text so that text is no longer than limit
     * @param text
     * @param limit or null to return text as is
     * @param endSize number of end characters used
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    static public String abbreviateText(final text, int limit = Integer.MAX_VALUE, int endSize = 3) {
        assert text != null
        assert endSize > 0
        assert limit >= endSize // limit must be bigger than the end size
        return (text.size() < limit) ? text : (text.substring(0, limit - endSize) + text.substring(text.size() - endSize))
    }

    /**
     * Get a map of property key to value for all properties that match the patterns provided.
     * If a pattern is provided, then it must match (results are ANDed together)
     * @param namePattern - regex string or pattern with single replacement if defined in pattern
     * @param valuePattern - regex string or pattern
     * @return map property key (perhaps modifed) to value
     */
    public Map<String, Object> getMatchingProperties(Map parameters) {
        Map<String, Object> map = [:]

        Pattern namePattern  = (parameters.namePattern  instanceof Pattern) ? (Pattern) parameters.namePattern  : parameters.namePattern  != null ? ~/${parameters.namePattern}/  : null
        Pattern valuePattern = (parameters.valuePattern instanceof Pattern) ? (Pattern) parameters.valuePattern : parameters.valuePattern != null ? ~/${parameters.valuePattern}/ : null

        gint.project.ant.properties.each() { name, value ->
            def match = (namePattern == null)
            if (!match) {
                def matcher = namePattern.matcher(name)
                match = matcher.matches()
                if (match) {
                    name = (matcher.groupCount() > 0) ? matcher.group(1) : name
                }
            }
            match = match && ((valuePattern == null) || valuePattern.matcher(value?.toString()).matches())
            if (match) {
                map.put(name, value)
            }
        }
        return map
    }

    /**
     * This is the standard JSON date format. See http://stackoverflow.com/questions/10286204/the-right-json-date-format.
     * This needs to be double quoted for use on a JSAP command.
     * @return
     */
    static public getJsonDateFormatString() {
        return "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    }

    /**
     * This is a pattern that will match the standard json date format
     * @return
     */
    static public getJsonDateRegexString() {
        return '\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}[+-]\\d{4}'
    }

    /**
     * Copied/changed from CliUtils for same function
     * Specialized compare separated string for >= . Primarily used for version checks. Like 5.8.5 >= 5.7 and 5.10.3 > 5.8
     *
     * <pre>
     * - compare false if s1 is shorter than s2 after split
     * - compare false if s1 does not have digits at position less that s2 has digits
     * </pre>
     *
     * @param s1
     * @param s2
     * @param separator - usually DOT for version checks
     * @param sepatator2 - usually DASH as sometimes you have 5.7-rc1
     * @return true if s1 is greater than or equal to s2
     */
    public static boolean compareSeparatedGreaterThanOrEqual(String s1, String s2, String separator = '.', String separator2 = '-') {
        // System.out.println("s1: " + s1);
        // System.out.println("s2: " + s2);
        boolean result = (s1 != null) && (s2 != null)
        if (result) {
            def a1 = s1.replaceAll(separator2, separator).tokenize(separator)
            def a2 = s2.replaceAll(separator2, separator).tokenize(separator)

            result = a2.size() <= a1.size()
            for (int i = 0; result && (i < a2.size()); i++) {
                result = a1[i].isInteger() && a2[i].isInteger()
                if (result) {
                    int compare = Integer.compare(Integer.parseInt(a1[i]), Integer.parseInt(a2[i]))
                    if (compare > 0) { // we are done
                        break
                    }
                    result = compare >= 0
                }
            }
        }
        return result
    }

    /**
     * Get the value for a line formatted as pretty output with a label of name (case-insensitive)
     * @param string - like: Title . . . . . . . . . . . . : JIRA 7.1.4
     * @param name - like: title
     * @return
     */
    public static String getPrettyFormatValue(string, name, int level = 0) {
        def indent = '  '.multiply(level)
        def matcher = string =~ /(?i)(?m)^${indent}${name} {1,2}(?:\. )+: (.*)$/  // case-insensitive, multi-line to match on $ for end of line
        return matcher.find() ? matcher.group(1) : null
    }

    public static String getPrettyFormatValue(Collection<String> list, name, int level = 0) {
        String result
        for (String entry in list) {
            result = getPrettyFormatValue(entry, name, level)
            if (result != null) { // get first one found
                break
            }
        }
        return result
    }

    /**
     * Clean out key values from the map the reference null values
     * @param map
     */
    public static void removeNullValues(Map map) {
        if (map != null) {
            def removeList = []
            map.each { key, value ->
                if (value == null) {
                    removeList << key
                }
            }
            map.keySet().removeAll(removeList)
        }
    }

    /**
     * Capitalize each word in a string. Example: capitalizeEachWord('hello world') == 'Hello World'
     * @param string
     * @return string with each word capitalized
     */
    public static String capitalizeEachWord(string) {
        string?.toString().tokenize().collect { it.capitalize() }.join(' ')
    }

    /**
     * Gradle doesn't allow certain characters in task names.
     * This will replace all of these invalid characters with a replacement.
     * This is most important when tasks are being created from files or other things
     * that can have a variety of characters in their name
     * @param string to have characters replaced
     * @param replacement - replacement for each character
     * @return modified string
     */
    public static String cleanTaskName(String string, String replacement = '_') {
        return string?.collectReplacements {
            if (it in ['/', '\\', ':', '<', '>', '"', '?', '*', '|']) {
                return replacement
            } else {
                return null
            }
        }
    }

    /**
     * Determines if running in a Bitbucket pipeline. Similarly for other pipelines
     * products that set the CI environment variable. If there are other standard environment
     * variables that are used for this purpose, they may be added in the future.
     */
    public static boolean isPipeline() {
        String ci = System.getenv('CI')
        return (ci != null) && (ci.toLowerCase() != 'false')
    }
}
