/*
 * Copyright (c) 2009, 2019 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import java.sql.ResultSetMetaData
import java.sql.Timestamp

import org.gint.plugin.Gint

import groovy.sql.Sql
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

@CompileStatic
@TypeChecked
public class SqlHelper extends BaseHelper{

    protected Closure message
    protected Helper helper

    protected jdbcDrivers = [
        postgres: 'org.postgresql.Driver',
        postgresql: 'org.postgresql.Driver',
        mysql: 'com.mysql.jdbc.Driver',
        jtds: 'net.sourceforge.jtds.jdbc.Driver',
        mssql: 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
        oracle: 'oracle.jdbc.driver.OracleDriver',
        db2: 'com.ibm.db2.jdbc.app.DB2Driver',
        db2400: 'com.ibm.as400.access.AS400JDBCDriver',
    ]

    // list of connections opened during Gint processing, make sure they get closed with finalize task
    List<Sql> sqlConnectionList = []

    public SqlHelper(final Gint gint) {
        super(gint)
        message = gint.message
        helper = gint.helper
    }

    /**
     * Get the map of jdbc drivers used as defaults
     * @return map of database names to driver name
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, String> getJdbcDrivers() {
        return jdbcDrivers
    }

    /**
     * Get a new SQL instance from the provided parameters
     * @param parameters is a map containing databaseType, user, password, database or name, host and port,driver
     * @throws exception if the Sql instance cannot be established
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Sql getConnection(final Map parameters) {
        def url
        def driver
        def databaseType = null
        if (parameters.url == null) {
            databaseType = parameters.databaseType ?: 'postgresql'
            driver = parameters.driver ?: jdbcDrivers[databaseType]
            url = getJdbcUrl(parameters)
        } else {
            url = parameters.url
            if (parameters.driver == null) {
                driver = getDriverFromUrl(url)
            } else {
                driver = parameters.driver
            }
        }
        if (helper.isDebug()) {
            message 'debug', "SQL url: ${url}"
        }
        def connection = null
        try {
            try {
                connection = Sql.newInstance(url, parameters.user, parameters.password, driver)
            } catch (ClassNotFoundException exception) {
                if (exception.message == 'org.postgresql.Driver') {
                    // retry
                    gint.addJdbcConfiguration() // default postgres configuration
                    connection = Sql.newInstance(url, parameters.user, parameters.password, driver)
                }
            }
        } catch (Exception exception) {
            message 'error', "Connection to a database of type ${databaseType ?: driver} failed using url ${url}."
            message 'error', 'Exception was: ' + exception.getMessage()
            throw exception
        }
        sqlConnectionList << connection // remember so it can be automatically cleaned up after gint is done
        return connection
    }

    /**
     * Get a connection from a profile string
     * <pre>
     * dbUrl = jdbc:postgresql://examplegear.com:5432/test dbUser = automation | dbPassword = automation
     * </pre>
     * @param profile
     * @return connection
     */
    public Sql getConnectionFromProfile(final String profile) {
        def parameters = [:]
        def matcher = profile =~ /(?i)dbUrl *= *(\S*?)(?:(?:[ ,|])|(?:$))/  // blank, comma or | separated
        if (matcher.find()) {
            parameters.url = matcher.group(1)
        }
        matcher = profile =~ /(?i)dbUser *= *(\S*?)(?:(?:[ ,|])|(?:$))/
        if (matcher.find()) {
            parameters.user = matcher.group(1)
        }
        matcher = profile =~ /(?i)dbPassword *= *(\S*?)(?:(?:[ ,|])|(?:$))/
        if (matcher.find()) {
            parameters.password = matcher.group(1)
        }
        matcher = profile =~ /(?i)driver *= *(\S*?)(?:(?:[ ,|])|(?:$))/
        if (matcher.find()) {
            parameters.driver = matcher.group(1)
        }
        return getConnection(parameters)
    }

    /**
     * Get the jdbc driver string based on the URL. Do the best you can to provide a default.
     *
     * @param url
     * @return driver - best guess given the url or blank
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    def String getDriverFromUrl(final String url) {
        if (url.contains("postgresql:")) {
            return jdbcDrivers.postgresql
        } else if (url.contains("mysql:")) {
            return jdbcDrivers.mysql
        } else if (url.contains("jtds:")) {
            return jdbcDrivers.jdts
        } else if (url.contains("sqlserver:")) {
            return jdbcDrivers.msssql
        } else if (url.contains("oracle:")) {
            return jdbcDrivers.oracle
        } else if (url.contains("as400:")) {
            return jdbcDrivers.db2400
        }
        return ""
    }

    /**
     * Construct valid URL for the database parameters provided. Use defaults for missing elements.
     * @param parameters is a map with at least databaseType provided and usually database or name for the name of database
     * @return url string
     */
    public String getJdbcUrl(final Map parameters) {
        if ((parameters == null) || ((parameters.databaseType != 'db2400') && !helper.isNotBlank(parameters.database) && !helper.isNotBlank(parameters.name))) {
            throw new Exception("Parameters are missing for determining a JDBC url.")
        }
        def dbName = (parameters.database ?: parameters.name)
        def databaseType = parameters.databaseType ?: 'postgresql'
        def host = parameters.host ?: 'localhost'
        switch (databaseType) {
            case 'postgres':
            case 'postgresql':
                def port = (parameters.port ?: '5432')
                return "jdbc:postgresql://${host}:${port}/${dbName}"
            case 'mysql':
                def port = (parameters.port ?: '3306')
                def options = (parameters.options ?: "?autoReconnect=true")
                return "jdbc:mysql://${host}:${port}/${dbName}${options}"
            case 'mssql':
                def port = (parameters.port ?: '1433')
                return "jdbc:jtds:sqlserver://${host}:${port}/${dbName}"
            case 'oracle':
                def port = (parameters.port ?: '1521')
                return "jdbc:oracle:thin:@${host}:${port}:${dbName}"
            case 'db2':
                def port = (parameters.port ? ":port:${parameters.port}" : '')
                return "jdbc:db2://${host}${port}/${dbName}"
            case 'db2400':
                def databaseName = (parameters.database ? "database name=${parameters.database}": '')
                def options = (parameters.options ?: "prompt=false;translate binary=true; extended metadata=true;${databaseName}")
                return "jdbc:as400://${host};${options}"
            default:
                throw new Exception("Database type '${databaseType}' not known. A JDBC url could not be generated.")
        }
    }

    /*
     * Run a non-select sql statement against an arbitrary connection, parameters are provided via a (non null) map
     * @param sql (required) - sql string to execute
     * @param connection (required) - connection to run against
     * @param ignoreExceptions (optional) - true to not throw exception, just report error, default is false
     * @param logExceptions (optional) - log additional error information, default is true
     * @param errorMessage (optional) - additional error message text when error occurs
     * @param virtual (optional) - true means don't run the sql, just log sql statement that would have been run (default false)
     * @param verbose (optional) - true provides additional logging information (default is false)
     * @return the number of rows affected (if any), -1 for errors
     */
    //  def runSql(final String sql, final Sql myConnection, final boolean ignoreExceptions = false, final boolean logExceptions = true, final String errorMessage = '') {
    // @CompileStatic(TypeCheckingMode.SKIP)
    public int run(final Map parameters) {

        //helper.log('parameters', parameters)
        def updateCount = 0
        if (parameters.virtual == true) {
            message('virtual', parameters.sql)
        } else {
            if (parameters.verbose == true) {
                message 'info', parameters.sql
            }
            if (parameters.connection instanceof Sql) {
                Sql connection = (Sql) parameters.connection
                try {
                    connection.executeUpdate(parameters.sql.toString()) // this makes it possible to retrieve update count
                    updateCount = connection.getUpdateCount()
                } catch (Exception exception) {
                    if (parameters.logExceptions != false) {
                        if (parameters.verbose != true) {  // not already logged
                            message('error', parameters.sql)
                        }
                        message('error', exception.toString())
                        parameters.exception = exception.toString()
                        if (parameters.errorMessage) {
                            message('error', parameters.errorMessage)
                        }
                    }
                    if (parameters.ignoreExceptions != true) {
                        throw exception
                    } else {
                        updateCount = -1
                    }
                }
            } else {
                if (parameters.logExceptions) {
                    message 'error', 'Connection is invalid or null for an SQL run request.'
                }
                updateCount = -1
            }
        }
        return updateCount
    }

    /**
     * Run an SQL statement
     * @see SqlHelper#runSql(Map)
     * @param sql - sql to run
     * @param connection - existing database connection
     * @return the number of rows affected (in any), -1 for errors
     */
    public int run(final Sql connection, final sql) {
        return run([connection: connection, sql: sql])
    }

    /**
     * Run an SQL insert or update  with the provided column to value map. Assumes that table has defined a primary key that can be used for lookup.
     * First, an lookup is done on the key to determine whether an insert or update should be attempted, for the update the primary key as the where clause
     *
     * @param primaryKey (required) - primary key to use
     * @param table (required) - table to update
     * @param valueMap (required) - maps columns to values
     * @param columnMap (optional) - map column names to table column names - assumes valueMap is still in original column names, defaults to null
     * @param connection (required) - db connection
     * @param columnList (optional) - a map of columns and values to be inserted, at least one column must be provided, defaults to valueMap keySet
     * @param ignoreNull (optional) - true to have null values not be used in set values
     * @param ignoreExceptions (optional) - true to not throw exception, just report error, default is false
     * @param logExceptions (optional) - log additional error information, default is true
     * @param errorMessage (optional) - additional error message text when error occurs
     * @param virtual (optional) - true means don't run the sql, just log sql statement that would have been run (default is false)
     * @param verbose (optional) - true provides additional logging information (default is false)
     * @return the number of rows affected, -1 for errors.  0 is returned if there are no columns
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public int runInsertOrUpdate(final Map parameters) {
        assert parameters.connection != null
        assert parameters.table != null
        assert parameters.primaryKey != null
        assert parameters.valueMap != null

        def originalKey = helper.findFirstKeyWithValue(parameters.columnMap ?: parameters.valueMap, parameters.primaryKey) // needs to be the original key
        if (originalKey == null) {
            if (helper.isDebug()) {
                helper.log('columnMap', parameters.columnMap)
                message 'debug', 'Did not find first key: ' + parameters.primaryKey
            }
            originalKey = parameters.primaryKey
        }
        def where = "where ${parameters.primaryKey}=${prepareField(parameters.valueMap[originalKey])}"

        String sql = "select ${parameters.primaryKey} from ${parameters.table} ${where}"
        int result = runSelect(connection: parameters.connection, sql: sql, verbose: parameters.verbose)

        if (result == 0) { // no rows match the key
            result = runInsert(parameters) // + [ignoreExceptions: false, logExceptions: false])
        } else {
            result = runUpdate(parameters + [where: where])
        }
        return result
    }

    /**
     * Run an SQL insert with the provided column to value map
     * @param table - table to update
     * @param valueMap - maps columns to values
     * @param columnMap - map column names to table names - assumes valueMap is still in original column names, defaults to null
     * @param connection - db connection
     * @param columnList - a map of columns and values to be inserted, at least one column must be provided, defaults to valueMap keySet
     * @param ignoreNull - true to have null values not be used in set values
     * @param ignoreExceptions (optional) - true to not throw exception, just report error, default is false
     * @param logExceptions (optional) - log additional error information, default is true
     * @param errorMessage (optional) - additional error message text when error occurs
     * @param virtual (optional) - true means don't run the sql, just log sql statement that would have been run (default is false)
     * @param verbose (optional) - true provides additional logging information (default is false)
     * @return the number of rows affected, -1 for errors.  0 is returned if there are no columns
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public int runInsert(final Map parameters) {
        def columnList = (parameters.columnList ?: parameters.valueMap.keySet())
        if (columnList.size() > 0) {
            def columnString = ''
            def valueString = ''
            columnList.each { column ->
                if ((parameters.ignoreNull == true) && (parameters.valueMap[column] == null)) {
                    // don't do null
                } else {
                    columnString += ', ' + (parameters.columnMap?.get(column) ?: column)
                    valueString += ', ' + prepareField(parameters.valueMap[column])
                }
            }
            def newParameters = parameters
            newParameters.sql = "insert into ${parameters.table} (${columnString.substring(1)}) values(${valueString.substring(1)})"
            return run(newParameters)
        }
        if (parameters.verbose == true) {
            message 'warning', 'No columns specified for insert.'
        }
        return 0
    }


    /**
     * Run an SQL update with the provided columns to value map
     * @param table - table to update
     * @param valueMap - maps columns to values
     * @param columnMap - map column names to table names - assumes valueMap is still in original column names, defaults to null
     * @param connection - db connection
     * @param columnList - a map of columns and values to be inserted, at least one column must be provided, defaults to valueMap keySet
     * @param where - full where clause if any
     * @param ignoreNull - true to have null values not be used in set values
     * @param ignoreExceptions (optional) - true to not throw exception, just report error, default is false
     * @param logExceptions (optional) - log additional error information, default is true
     * @param errorMessage (optional) - additional error message text when error occurs
     * @param virtual (optional) - true means don't run the sql, just log sql statement that would have been run (default false)
     * @param verbose (optional) - true provides additional logging information (default is false)
     * @return the number of rows affected, -1 for errors.  0 is returned if there are no columns
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public int runUpdate(final Map parameters) {
        assert parameters.connection != null
        assert parameters.table != null
        def rowsUpdated = 0
        def columnList = (parameters.columnList ?: parameters.valueMap.keySet())
        if (columnList.size() > 0) {
            def setList = []
            columnList.each { column ->
                if ((parameters.ignoreNull == true) && (parameters.valueMap[column] == null)) {
                    // don't do anything for null values
                } else {
                    setList << "${(parameters.columnMap?.get(column) ?: column)}=${prepareField(parameters.valueMap[column], 'NULL')}"
                }
            }
            def newParameters = parameters
            newParameters.sql = "update ${parameters.table} set ${helper.listToSeparatedString(setList)} ${parameters.where ?: ''}"
            rowsUpdated = run(newParameters)
        }
        return rowsUpdated
    }

    /**
     * Run select statement - primarily used to log results from running a select statement.
     * @param parameters
     * @param sql - select statement
     * @param defaultValue - returned integer value returned on error - defaults to -1
     * @param log - unless false, row data will be logged
     * @param verbose (optional) - true provides additional logging information (default is false)
     * @return row count or defaultValue if there was an error
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public int runSelect(final Map parameters) {
        assert parameters.connection != null
        if (parameters.verbose == true) {
            message 'info', parameters.sql
        }
        def rowCount = 0
        try {
            parameters.connection.eachRow(parameters.sql.toString()) { row ->
                if (parameters.log != false) {
                    helper.log('row', row)
                }
                rowCount++
            }
        } catch (Exception exception) {
            message 'error', "Error running select: ${parameters.sql}"
            message 'error', exception.toString()
            return (parameters.defaultValue instanceof Integer) ? parameters.defaultValue : -1
        }
        return rowCount
    }
    public int runSelect(final Sql connection, final sql) {
        return runSelect([connection: connection, sql: sql])
    }

    /**
     * Count the rows in the table wrt the where clause
     * @param connection
     * @param table name
     * @param column name, default is * (this allows you to count rows using distinct for example)
     * @param where or null - partial (no 'where') or full where clause
     * @param defaultValue - integer value returned if something goes wrong with sql providing the value - defaults to 0
     * @param verbose - true to log the sql statement
     * @return count
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public int countRows(final Map parameters) {
        assert parameters.connection != null
        assert parameters.table != null
        def where = ((parameters.where && (parameters.where != '')) ? ((parameters.where.startsWith('where') ? '' : 'where ') + parameters.where) : '')
        def newParameters = parameters.clone()
        def column = parameters.column ?: '*'
        newParameters.sql = "select count(${column}) as \"count\" from ${parameters.table.replace('/', '.')} ${where}"
        newParameters.defaultValue = (parameters.defaultValue instanceof Integer) ? parameters.defaultValue : 0
        if (parameters.table == null) {
            message "error", "countRows requires a table name."
            return newParameters.defaultValue
        }
        return getFirstColumn(newParameters)
    }

    public int countRows(final Sql connection, final table, final whereClause = null, final defaultValue = 0) {
        return countRows([connection: connection, table: table, where: whereClause, defaultValue: defaultValue])
    }

    /**
     * Get the first column value from the first row from running the select (or similar) statement. Intended for
     * getting counts and similar values.
     * Note a groovy sql anomaly with columns with the same name: select 'a', 'b' will return 'b' for the first column.
     * Use select 'a' as column1, 'b' as column2 to make it work properly. Groovy stores the row data as a Map, so duplicate
     * column names get overwritten by subsequent values
     * @param connection
     * @param sql - statement to run
     * @param defaultValue - value returned if result would otherwise be null
     * @param verbose - true to log the sql statement
     * @return result or default value if result is null
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def getFirstColumn(final Map parameters) {

        def result = null
        def row = null
        try {
            row = parameters.connection?.firstRow(parameters.sql.toString())
            if (parameters.verbose == true) {
                message 'info', "Sql: ${parameters.sql}"
            }
        } catch (Exception exception) {
            message 'error', "Exception: ${exception.toString()} with sql:\n ${parameters.sql}"
        }
        if (row) {
            result = row.getAt(0)  // get the first column
        }
        return (result != null ? result : parameters.defaultValue)
    }

    public def getFirstColumn(final Sql connection, final sql, final defaultValue = null, final boolean verbose = false) {
        return getFirstColumn([connection: connection, sql: sql, defaultValue: defaultValue, verbose: verbose])
    }

    /**
     * Get timestamp - doesn't work for db2,
     * @param connection
     * @return timestamp in form yyyy-MM-dd hh:mm:ss.mmm
     */
    public String getNow(final Sql connection) {
        return getFirstColumn(connection, "select now()").toString()
    }

    /**
     * Prepare a field value for an sql statement.
     * - quote strings
     * - escape \
     * - handle lists and sets as arrays
     * @param value to handle
     * @param ifNullValue - return this if value is null
     * @return modified value appropriate for use in an sql statement
     */
    //@CompileStatic(TypeCheckingMode.SKIP)
    public def prepareField(final value, ifNullValue = null) {
        def result = ifNullValue
        if (value != null) {
            if ((value instanceof String) || (value instanceof GString)) {
                result = helper.quoteString(value.replace("\\", "\\\\"))
            } else if ((value instanceof Timestamp) || (value instanceof Date)) {
                result = "'${value}'"
            } else if ((value instanceof List) || (value instanceof Set)) {
                result = prepareField("{ " + helper.listToSeparatedString(value, ', ', '', '\\') + " }")
            } else if (value instanceof String[]) {
                def list = []
                value.each { String entry ->
                    list << entry.trim()
                }
                result = prepareField(list)
            } else if (value instanceof Map) {
                result = prepareField(value.toString())
            } else {
                result = value
            }
        }
        return result
    }

    /**
     * Get column names from SQL metadata as a list so it is easier to deal with. Metadata can be obtained via
     * metadata closure on eachRow or rows requests - example: connection.each("select ...", metadataClosure) { ...
     * @param - metadata from an result set
     * @return - column list
     */
    public List<String> getColumns(final ResultSetMetaData metadata) {
        def result = []
        def columnCount = metadata.getColumnCount()
        for (int i = 1; i <= columnCount; i++) {    // note that columns names start at index 1
            result << metadata.getColumnName(i)
        }
        //helper.log('columns', result)
        return result
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    public List<String> getColumns(final Sql connection, final table) {
        ResultSetMetaData metadata;
        connection.eachRow('select * from ' + table + ' limit 0', { it -> metadata = it}, { })
        return getColumns(metadata)
    }

    /**
     * Close any connection opened during this run
     */
    public void closeSqlConnections() {
        sqlConnectionList.each { entry ->
            if (entry != null) {
                try {
                    entry.close()
                } catch (Exception ignore) {
                }
            }
        }
    }
}
