/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.gint.helpers

import org.gint.plugin.Constants
import org.gint.plugin.Gint
import org.gint.tasks.GintTask

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

@CompileStatic
@TypeChecked
class ClosureHelper extends BaseHelper {

    public ClosureHelper(final Gint gint) {
        super(gint)
    }

    /**
     * Run the list of task closures
     * @param list of closures
     * @param task
     * @param closureResultKey - key in task to update with the results of the last closure
     * @return true if all closures run successfully (no exceptions), false as soon as one fails with exception
     */
    protected boolean runClosure(final List list, final GintTask task, final String closureResultKey = null, final String closureExceptionKey = null) {
        def result = true
        list.each { entry ->
            if (entry instanceof Closure) {
                result = result && runClosure(entry, task, closureResultKey, closureExceptionKey)
            }
        }
        return result
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    protected boolean runClosure(final closure, final GintTask task, final String closureResultKey = null, final String closureExceptionKey = null) {
        if (closure instanceof Closure) {
            if (gint.useSynchronizedClosures) {
                return runSynchronizedClosure(closure, task, closureResultKey, closureExceptionKey)
            } else {
                return runClosureInternal(closure, task, closureResultKey, closureExceptionKey)
            }
        } else {
            gint.message 'warning', "Ignore entry that is not a closure: ${closure}"
            return false
        }
    }

    @Synchronized
    protected boolean runSynchronizedClosure(final Closure closure, final GintTask task, final String closureResultKey = null, final String closureExceptionKey = null) {
        return runClosureInternal(closure, task, closureResultKey, closureExceptionKey)
    }

    /**
     * Run a task closure
     * @param closure
     * @param task or other similar Map parameter for the closure
     * @param closureResultKey - key in task to update with the results of the closure
     * @return true if closure was run successfully (no exceptions)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected boolean runClosureInternal(final Closure closure, final GintTask task, final String closureResultKey = null, final String closureExceptionKey = null) {

        def exception = null
        def result = false
        try {
            if (closureResultKey != null) {
                task.closures[closureResultKey] = closure.call(task)
            } else {
                closure.call(task)
            }
            result = true
        } catch (AssertionError e) {
            exception = e
            if (closureExceptionKey != null) {
                task.closures[closureExceptionKey] = Constants.FAIL_ASSERTION
            }
        } catch (Exception e) {
            exception = e
            if (closureExceptionKey != null) {
                task.closures[closureExceptionKey] = Constants.FAIL_EXCEPTION
            }
        } catch (Error e) {
            // primarily for assertion failures or other errors
            exception = e
            task.closures[closureExceptionKey] = Constants.FAIL_ERROR
        }
        if (exception != null) {
            //gint.message(task.result == task.expected ? 'ignore' : 'error', task.name + ': ' + exception.toString()
            gint.addFailedMessage("Closure for task ${task.name} generated exception: " + exception.toString(), true)

            if (exception instanceof AssertionError) {
                // do not log stack trace for assertion failures as power asserts are pretty specific for debugging
                task.getOutData() << exception.getMessage()  // log the assertion failure data  // TODO is this needed, we did for legacy
            } else {
                if (gint.getVerbose()) {
                    exception.printStackTrace()
                }
            }
        }
        return result
    }

    /**
     * Handle inline task. At this point inline is a closure
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected void handleInline(final GintTask task) {
        def retryCount = 0

        def exception = null
        // closure should return true, false or a integer return code, null is considered as successful
        try {
            Closure inline = (Closure) task.closures.inline
            def closureResult = inline.call(task)
            //helper.log('closureResult', closureResult)

            task.result = closureResult
            if (closureResult == null || closureResult == true) {
                task.result = 0
            } else if (closureResult instanceof Integer) {
                task.result = (Integer) closureResult
            } else if (closureResult == false) {
                task.result = -1
            } else { // else leave as is, could be a string, etc...
                task.result = closureResult
            }

        } catch (AssertionError e) {
            exception = e
            task.result = Constants.FAIL_ASSERTION
        } catch (Exception e) {
            exception = e
            task.result = Constants.FAIL_EXCEPTION
        } catch (Error e) {
            exception = e
            task.result = Constants.FAIL_ERROR
        }

        if (exception != null) {
            gint.reportHelper.taskMessage(task.result == task.expected ? 'ignore' : 'error', task.name + ': ' + exception.toString())
            task.getOutData() << exception.toString()
            task.failReason = exception.getMessage() // Explicitly set the the actual power assert error message

            if (exception instanceof AssertionError) {
                // do not log stack trace for assertion failures as power asserts are pretty specific for debugging
            } else {
                task.getOutData() << exception.getStackTrace().toString()
                if (gint.getVerbose()) {
                    exception.printStackTrace()
                }
            }
        }
    }
}
