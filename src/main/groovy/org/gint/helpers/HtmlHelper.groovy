/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.gint.helpers

/**
 * Class to provide help for some simple HTML constructs. If methods here are not flexible enough, use code as template to customize
 */
class HtmlHelper {

    public static String getTableWrapper(tableBody) {
        return "<table><tbody>${tableBody}\n</tbody>\n</table>\n"
    }

    public static String getTableRow(List<String> columns, type = 'td') {
        def builder = new StringBuilder()
        columns.each { column ->
            builder.append('<').append(type).append('>').append(column).append('\n</').append(type).append('>')
        }
        return builder.toString()
    }

    public static String getTableBody(List<String> rows) {
        def builder = new StringBuilder()
        rows.each { row ->
            builder.append('<tr>').append(row).append('\n</tr>')
        }
        return builder.toString()
    }

    /**
     * Wrap something in a HTML CDATA construct
     * @param body
     * @return wrapped string
     */
    public static String cdataWrapper(body) {
        return '<![CDATA[' + body + ']]>'
    }
}
