/*
 * Copyright (c) 2009, 2020 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import org.gint.plugin.GintUtils2
import org.gint.objects.Runner
import org.gint.objects.ScriptEntry
import org.gint.plugin.Gint
import org.gint.tasks.GintTask
import org.gradle.api.Task
import org.gradle.api.tasks.Exec
import org.gradle.api.tasks.TaskExecutionException
import org.gint.tasks.GintRunnerTask

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

@CompileStatic
@TypeChecked
public class ScriptRunner extends BaseHelper{

    protected final String quote = gint.helper.isWindows() ? '"' : "'" // single quote on linux to avoid any shell interpretations, windows needs double quote

    public ScriptRunner(final Gint gint) {
        super(gint)
    }

    public List<Task> add(final Map<String, Object> parameters = null) {

        Runner runner = new Runner(gint, parameters)

        boolean doLog = (parameters?.log != null) || (System.getProperty('log') != null)
        // additional args and tasks for itest cmd line including handling parameters with embedded blanks
        List<String> moreArgs = gint.helper.csvDataAsList( //
                        data: System.getProperty('args'), //
                        delimiter: 'blank',  // whitespace was an alternative here
                        quote: "'",
                        )
        File logDir
        if (doLog) {
            def log = parameters?.log ?: System.getProperty('log')
            if (((log instanceof String) || (log instanceof GString)) && gint.helper.isNotBlank(log)) {
                logDir = gint.project.file(log.toString())
            } else if (log instanceof File) {
                logDir = log
            } else {
                logDir = gint.project.file(gint.project.buildDir.toString() + '/logs')
            }
            logDir.mkdirs()
        }
        // gint.helper.log('logDir', logDir)

        // Best to let Gint determine workerCount setting, but, you can set it explicitly
        Integer workerCount = parameters?.workerCount != null && parameters.workerCount instanceof Integer ? parameters.workerCount : null;
        def doParallel = gint.project.gradle.startParameter.isParallelProjectExecutionEnabled() || parameters?.parallel || parameters.workerCount != null

        if (doParallel) {
            return [ addTaskForParallel(runner, logDir, moreArgs, parameters,  workerCount) ]
        } else {
            return addScriptTasks(runner, logDir, moreArgs, parameters)
        }
    }

    // Add task for each script
    protected List<Task> addScriptTasks(final Runner runner, final File logDir, final List<String> moreArgs, final Map<String, Object> parameters) {

        boolean doLog = logDir != null
        List<Task> taskList = new ArrayList<>()

        // Find or create grouping task
        Set<Task> groupTaskSet = (runner.taskNamePrefix == null) ? null : gint.project.getTasksByName(runner.taskNamePrefix, false)
        Task groupTask = groupTaskSet == null ? null : groupTaskSet.size() == 1 ? groupTaskSet.getAt(0) : gint.project.task(runner.taskNamePrefix)

        runner.getSelectedFileList().each { File entry ->
            //helper.log 'entry name', entry.name

            String baseName = helper.getNameNoExtension(entry.name) // no prefix and no extension
            String extension = helper.getExtension(entry.name)

            if (runner.selectFile(entry.name, baseName, extension)) { // secondary selection based on entry name

                // When list tasks, see if the first line of the files indicates a task description
                def description = ''
                if (gint.project.gradle.startParameter.taskNames.contains('tasks')) {
                    def descriptionIndicator = '// Description: '
                    def firstLine = entry.readLines().get(0)
                    description = firstLine.startsWith(descriptionIndicator) ? firstLine.substring(descriptionIndicator.size()).trim() : ''
                }

                def relativeFile = runner.scriptDir.toPath().relativize(entry.toPath()).toFile() // this is a nio Path convert to a java File
                // We may need to qualify the simple name if we are doing recursive to prevent duplication and provide directory info for those in a sub-directory only
                if (runner.recursive && !entry.getParentFile().getAbsolutePath().equals(runner.scriptDir.getAbsolutePath())) {
                    baseName = helper.getNameNoExtension(relativeFile.getPath())
                }
                def name = (runner.taskNamePrefix == null ? '' : runner.taskNamePrefix + '-') + gint.helper.cleanTaskName(baseName)
                //helper.log 'task name', name

                // Create task
                Exec task = (Exec) gint.project.task(name, type: Exec, description: description)
                taskList << task

                transferParametersToTask(parameters, task)
                groupTask?.dependsOn(task)

                task.setExecutable(parameters?.executable?.toString() ?: gint.helper.isWindows() ? '.\\gradlew.bat' : './gradlew')
                if (parameters?.workingDir) {
                    task.workingDir = parameters?.workingDir
                }
                if (parameters?.environment instanceof Map) {  // add user specified environment variables
                    task.environment((Map) parameters.environment)  // should be map key to string normally
                }

                List args = GintUtils2.getArgs(parameters, runner.providedArgs, moreArgs, runner.scriptDirName, relativeFile)
                task.setArgs(args)

                task.setIgnoreExitValue(true) // defer error until we have a chance to do other things

                // task inputs and outputs
                runner.inputDirList.each { task.inputs.dir(it) }
                runner.inputFileList.each { task.inputs.file(it) }
                task.inputs.file(entry.getAbsolutePath())
                task.outputs.files(runner.outputFileClosure.call(baseName))

                File log = doLog ? new File("${logDir}/${baseName}.txt") : null

                def cmd  // cmd to run

                task.doFirst { Exec execTask ->
                    if (doLog) {
                        def outputStream = new FileOutputStream(log)
                        outputStream << new Date() // log time and date of run
                        outputStream << '\n\n'
                        task.setStandardOutput(outputStream)
                    }
                    task.setErrorOutput(new ByteArrayOutputStream())

                    StringBuilder builder = new StringBuilder()
                    execTask.commandLine.each {
                        builder.append(it.contains(' ') && !it.endsWith(quote) ? gint.helper.quoteString(it, quote) : it)
                               .append(' ')
                    }
                    cmd = builder.toString()
                    println '\nExec task: ' + execTask.commandLine
                    println gint.reportHelper.BLUE + 'Run: ' + baseName + System.lineSeparator() + cmd + gint.reportHelper.NO_COLOR + '\n'
                }

                task.doLast { Exec execTask ->
                    def failed = execTask.executionResult.get().exitValue != 0

                    // This prevents intermixed output (standard out and error) that we saw especially in Bamboo logs :(
                    println execTask.errorOutput.toString()
                    println '- '.multiply(50)

                    if (doLog) {
                        println '\nOutput logged to ' + log.getAbsolutePath()
                        log << execTask.errorOutput.toString()
                    }
                    println System.lineSeparator() + (failed ? gint.reportHelper.RED : gint.reportHelper.GREEN) + 'Run: ' + baseName + System.lineSeparator() + cmd + gint.reportHelper.NO_COLOR + System.lineSeparator()

                    if (failed) {
                        throw new TaskExecutionException(execTask, new Exception("${baseName} failed with " + execTask.executionResult.get().exitValue))
                    }
                }
            }
        }
        return taskList
    }

     // Add single task to run each script with a parallel thread pool
    protected Task addTaskForParallel(final Runner runner, final File logDir, final List<String> moreArgs, final Map<String, Object> parameters, final Integer workerCount) {

        List<ScriptEntry> scriptList = new ArrayList<>()
        runner.getSelectedFileList().each { File file ->
            ScriptEntry entry = new ScriptEntry(file)
            //helper.log 'entry name', entry.name

            String baseName = helper.getNameNoExtension(entry.name) // no prefix and np extension
            String extension = helper.getExtension(entry.name)

            if (runner.selectFile(entry.name, baseName, extension)) { // secondary selection based on entry name
                scriptList << entry
            }
        }
        //def relativeFile = runner.scriptDir.toPath().relativize(entry.toPath()).toFile() // this is a nio Path convert to a java File
        def name = parameters?.taskNamePrefix ? parameters.taskNamePrefix.toString() : 'itest'
        GintRunnerTask task = (GintRunnerTask) gint.project.task(name, type: GintRunnerTask, description: parameters?.description ?: "")
        task.setParameters(parameters)
        task.setProvidedArgs(runner.providedArgs)
        task.setMoreArgs(moreArgs)
        task.setScriptDir(runner.scriptDir)
        task.setScriptDirName(runner.scriptDirName)
        task.setLogDir(logDir);
        task.setScriptList(scriptList)
        task.setWorkerCount(workerCount)

        transferParametersToTask(parameters, task)

        return task
    }

    protected transferParametersToTask(Map parameters, Task task) {
        if (parameters != null) {
            def list = [ // any gradle task like Exec
                'dependsOn',
                'mustRunAfter',
            ]
            if (task instanceof GintTask) {
                list += [
                    'expected',
                    'ignoreFailure',
                    'data',
                    'failData',
                    'output',
                ]
            }
            list.each { String entry ->
                if (parameters.containsKey(entry)) {
                    switch (entry) {

                    case 'dependsOn':
                       task.dependsOn(parameters[entry])
                       break;

                    case 'mustRunAfter':
                       task.mustRunAfter(parameters[entry])
                       break;

                    default:
                        task[entry] = parameters[entry]
                    }
                }
            }
        }
    }
}
