/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import org.gint.plugin.GintUtils2
import org.gint.interfaces.CmdGenerator
import org.gint.plugin.Constants
import org.gint.plugin.Gint
import org.gint.tasks.GintTask
import org.gradle.api.Task

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Class to provide GINT command generator support.
 * A GINT command generator closure is a closure that takes a gint task as a parameter and produces a command string
 * suitable for running on the current platform.
 * This class helps manage the command generators by associating them with names and parameters used to construct them.
 */
@CompileStatic
@TypeChecked
class CmdGeneratorHelper extends BaseHelper {

    protected Map<Map<String, Map<String, Object>>, Closure> generators = [:] // Closure takes a Task as parameter

    public CmdGeneratorHelper(final Gint gint) {
        super(gint)
    }

    public CmdGenerator getCmdGenerator(final String name, final Map parameters = null) {

        assert name != null // getCmdGenerator name parameter must not be null
        switch (name) {
            case 'gant':            return getCmdGeneratorForGant(parameters)
            case 'gradle':          return getCmdGeneratorForGradle(parameters)
            case 'gradlew':         return getCmdGeneratorForGradlew(parameters)
            case 'groovy':          return getCmdGeneratorForGroovy(parameters)
            case 'java':            return getCmdGeneratorForJava(parameters)
            case 'curl':            return getCmdGeneratorForCurl(parameters)
            case 'ftp':             return getCmdGeneratorForFtp(parameters)
            case 'findReplace':     return getCmdGeneratorForFindReplace(parameters)
            case 'image':           return getCmdGeneratorForImage(parameters)

            default: break
        }
        return null
    }


    /**
     * Built in support for GANT commands
     * Closure that generates a command string based on the initial or task provided parameters:
     * - dir (both)
     * - file (both) - required, either name or File
     * - classpath (both)
     * - options (both)
     * - arguments
     * - targets
     * - redirect
     * @return closure that generates a gant command string
     */
    public CmdGenerator getCmdGeneratorForGant(final Map parameters = null) {

        def home = System.getenv().'GANT_HOME'
        def defaultCli = helper.windowsQuoteName((home ? (home + GintUtils2.getFileSeparator() + 'bin' + GintUtils2.getFileSeparator()) : '') + 'gant')
        def cli = helper.getWithExtendedLookup(parameters?.cli, 'gantCli', defaultCli)
        boolean specialCase = cli?.startsWith('"') && helper.isWindows()

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def dir = (map?.dir ?: parameters?.dir)
            def cd = (dir != null) ? "cd ${dir} ${helper.getCommandSeparator()}" : ''

            def classpath = map?.classpath ?: parameters?.classpath
            def cp = helper.isNotBlank(classpath) ? "--classpath ${classpath}" : ''

            def options = map?.options ?: parameters?.options ?: ''

            def file = map?.file ?: parameters?.file ?: ''
            if (file instanceof File) {
                file = file.getAbsolutePath()
            }

            if (specialCase && (map?.shell == null)) { // gant is executable
                map?.shell = false  // best chance the it will work on Windows as cmd will mess up on more than 2 quotes
            }
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"

            return "${cd} ${cli} ${cp} ${options} -f ${file} ${map?.arguments ?: ''} ${map?.targets ?: ''} ${redirect}"
        }
    }

    /**
     * Built in support for Gradle commands
     * Closure that generates a command string based on the initial or task provided parameters:
     * - dir (both)
     * - file (both) - required, either name or File
     * - classpath (both)
     * - options (both)
     * - arguments
     * - tasks
     * - redirect
     * @return closure that generates a gant command string
     */
    public CmdGenerator getCmdGeneratorForGradle(final Map parameters = null) {

        def home = System.getenv().'GRADLE_HOME'
        def defaultCli = helper.windowsQuoteName((home ? (home + GintUtils2.getFileSeparator() + 'bin' + GintUtils2.getFileSeparator()) : '') + 'gradle')
        def cli = helper.getWithExtendedLookup(parameters?.cli, 'gradleCli', defaultCli)
        boolean specialCase = cli?.startsWith('"') && helper.isWindows()

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def dir = (map?.dir ?: parameters?.dir)
            def cd = (dir != null) ? "cd ${dir} ${helper.getCommandSeparator()}" : ''

            //def classpath = map?.classpath ?: parameters?.classpath
            //def cp = helper.isNotBlank(classpath) ? "--classpath ${classpath}" : ''

            def options = map?.options ?: parameters?.options ?: ''

            def file = map?.file ?: parameters?.file ?: ''
            if (file instanceof File) {
                file = file.getAbsolutePath()
            }

            if (specialCase && (map?.shell == null)) { // gant is executable
                map?.shell = false  // best chance the it will work on Windows as cmd will mess up on more than 2 quotes
            }
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"

            return "${cd} ${cli} ${options} -b ${file} ${map?.arguments ?: ''} ${map?.tasks ?: ''} ${redirect}".toString()
        }
    }

public CmdGenerator getCmdGeneratorForGradlew(final Map parameters = null) {

        def defaultCli = helper.windowsQuoteName(gint.helper.isWindows() ? '.\\gradlew.bat' : './gradlew')
        def cli = helper.getWithExtendedLookup(parameters?.cli, 'gradlewCli', defaultCli)
        boolean specialCase = cli?.startsWith('"') && helper.isWindows()

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def dir = (map?.dir ?: parameters?.dir)
            def cd = (dir != null) ? "cd ${dir} ${helper.getCommandSeparator()}" : ''

            //def classpath = map?.classpath ?: parameters?.classpath
            //def cp = helper.isNotBlank(classpath) ? "--classpath ${classpath}" : ''

            def options = map?.options ?: parameters?.options ?: ''

            def file = map?.file ?: parameters?.file ?: ''
            if (file instanceof File) {
                file = file.getAbsolutePath()
            }

            if (specialCase && (map?.shell == null)) { // gant is executable
                map?.shell = false  // best chance the it will work on Windows as cmd will mess up on more than 2 quotes
            }
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"

            return "${cd} ${cli} ${options} -b ${file} ${map?.arguments ?: ''} ${map?.tasks ?: ''} ${redirect}".toString()
        }
    }

    /**
     * Built in support for Groovy commands
     * Closure that generates a command string based on the initial or task provided parameters:
     * - dir (both)
     * - file (both) - usually required, either name or File
     * - classpath (both)
     * - options (both)
     * - arguments
     * - redirect
     * @return closure that generates a command string
     */
    public CmdGenerator getCmdGeneratorForGroovy(final Map parameters = null) {

        def home = System.getenv().'GROOVY_HOME'
        def defaultCli = helper.windowsQuoteName((home ? (home + GintUtils2.getFileSeparator() + 'bin' + GintUtils2.getFileSeparator()) : '') + 'groovy')
        def cli = helper.getWithExtendedLookup(parameters?.cli, 'groovyCli', defaultCli)
        boolean specialCase = cli?.startsWith('"') && helper.isWindows()

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def dir = (map?.dir ?: parameters?.dir)
            def cd = (dir != null) ? "cd ${dir} ${helper.getCommandSeparator()}" : ''

            def classpath = map?.classpath ?: parameters?.classpath
            def cp = helper.isNotBlank(classpath) ? "-cp ${classpath}" : ''

            def options = map?.options ?: parameters?.options ?: ''

            def file = map?.file ?: parameters?.file ?: ''
            if (file instanceof File) {
                file = file.getAbsolutePath()
            }

            if (specialCase && (map?.shell == null)) { // groovy is executable
                map?.shell = false  // best chance the it will work on Windows as cmd will mess up on more than 2 quotes
            }
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"

            return "${cd} ${cli} ${cp} ${options} ${file} ${map?.arguments ?: ''} ${redirect}".toString()
        }
    }

    /**
     * Built in support for JAVA commands
     * Closure that generates a command string based on the initial or task provided parameters:
     * - dir (both)
     * - class (both) - if specified, jar is ignored
     * - jar (both) - either class or jar is required
     * - classpath (both)
     * - options (both)
     * - arguments
     * - redirect
     * @return closure that generates a command string
     */
    public CmdGenerator getCmdGeneratorForJava(final Map parameters = null) {

        def home = System.getenv().'JAVA_HOME'
        def defaultCli = helper.windowsQuoteName((home ? (home + GintUtils2.getFileSeparator() + 'bin' + GintUtils2.getFileSeparator()) : '') + 'java')
        def cli = helper.getWithExtendedLookup(parameters?.cli, 'javaCli', defaultCli)
        boolean specialCase = cli?.startsWith('"') && helper.isWindows()

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def dir = (map?.dir ?: parameters?.dir)
            def cd = (dir != null) ? "cd ${dir} ${helper.getCommandSeparator()}" : ''

            def classpath = map?.classpath ?: parameters?.classpath
            def cp = helper.isNotBlank(classpath) ? "-cp ${classpath}" : ''

            def options = map?.options ?: parameters?.options ?: ''

            def name = map?.class ?: map?.jar ?: parameters?.class ?: parameters?.jar
            def isJar = helper.isNotBlank(map?.jar) || (!map?.class && helper.isNotBlank(parameters?.jar))

            if (specialCase && (map?.shell == null)) { // java is executable
                map?.shell = false  // best chance the it will work on Windows as cmd will mess up on more than 2 quotes
            }
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"

            return "${cd} ${cli} ${cp} ${options} ${isJar ? '-jar ' : ''} ${name} ${map?.arguments ?: ''} ${redirect}".toString()
        }
    }

    /**
     * Generate closure that will generate the commands for curl
     * <p> The task definition can have keys:
     * <ul>
     * <li> url - to test
     * <li> options - options string
     * <li> cookie - true or name of reference task or file name
     * <li> postData - for a post action (-d, --data option)
     * <li> user - user for authentication
     * <li> password - password for authentication
     * </ul>
     * <p> The following task keys are modified as a result of this:
     * <ul>
     * <li> output - output parameter is updated if not specified
     * <ul>
     * <li> file - set to default value if not specified
     * </ul>
     * </ul>
     * @result closure that generates a curl command
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public CmdGenerator getCmdGeneratorForCurl(final Map parameters = null) {
        // -L to follow redirects
        // -f to fail with return code 22 instead of fail document

        def cli = helper.getWithExtendedLookup(parameters?.cli, 'curlCli', 'curl -L -f --silent --show-error')
        return (CmdGenerator) { Task task ->
            Map<String, Object> map = getUserMap(task) // may be null

            def builder = new StringBuilder()
            builder.append(cli)

            if (map?.output == null) {
                map?.output = [:]
            }
            if (map?.output?.file == null) {
                map?.output?.file = this.gint.getOutputFile(map?.name, '.html')
            }
            builder.append(" --output ${map?.output?.file}")

            def cookie = map?.cookie ?: parameters?.cookie ?: false
            if (cookie == false) {
                // do nothing
            } else if (cookie == true) {
                cookie = gint.getOutputFile(map?.name, '.cookies')
            } else if (helper.isNotBlank(map?.cookie)) {
                Task referencedTask = gint.project.getTasks().getByName(map.cookie.toString())
                Map referencedMap = getUserMap(referencedTask)
                if (referencedMap != null) {
                    cookie = (referencedMap.cookie == true) ? gint.getOutputFile(referencedMap.name, '.cookies') : referencedMap.cookie
                } else {
                    // assume cookie references a file name
                }
            }
            if (cookie) {
                builder.append(" --cookie ${cookie} --cookie-jar ${cookie}")
            }
            def user = map?.user ?: parameters?.user
            def password = map?.password ?: parameters?.password
            if (user != null) {
                builder.append(" --user ${user}")
                if (helper.isNotBlank(password)) {
                    builder.append(":${password}")
                }
            }
            if (map?.postData) {
                def postData = (map?.postData instanceof Map) ? helper.mapToSeparatedString((Map) map?.postData) : map?.postData
                builder.append(' --data "').append(postData).append('"')
            }
            if (map?.options) {
                builder.append(' ').append(map?.options)
            }
            //message 'debug', 'curl builder: ' + task
            builder.append(' ').append(map?.url instanceof Closure ? map?.url.call() : map?.url?.toString())
            // redirect likely not needed because of output support
            // builder.append(' ').append(map?.redirect ?: '')  // usually "> fileName" or "> fileName 2>&1"
            return builder.toString()
        }
    }

    /**
     * Built in support for FTP
     * Closure that generates a command string for an ftp command
     * - host
     * - user
     * - password
     * - dir local directory
     * - cd remote cd command
     * - bin for binary transfer - default is do nothing
     * - ascii for ascii transfer - default is do nothing
     * - request - ftp line command like get xxxx yyyy or put yyyy xxxxx
     * @return closure that generates a command string
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public CmdGenerator getCmdGeneratorForFtp(final Map parameters = null) {

        def cli = helper.getWithExtendedLookup(parameters?.cli, 'ftpCli', 'ftp -n -i')
        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def dir = (map?.dir ?: parameters?.dir)
            def dirCmd = (dir != null) ? "cd ${dir} ${helper.getCommandSeparator()}" : ''

            def user = (map?.user ?: parameters?.user)
            def password = (map?.password ?: parameters?.password)
            def userString = (user ? "user ${user} ${password}\n" : '')

            def ascii = ((map?.ascii == true) || (map?.bin == false)) ? 'ascii\n' : ''
            def bin = ((map?.bin == true) || (map?.ascii == false)) ? 'bin\n' : ''
            def cd = (map?.cd ? "cd ${map?.cd}\n" : '')

            def request = (map?.request instanceof List) ? helper.listToSeparatedString(list: map?.request, separator: '\n') : map?.request

            map?.standardInput = "${userString}${ascii}${bin}${cd}${request}\nquit"
            // ftp command can complete successfully, put there may be error records produced by the script
            // fail if there is error output
            if (map?.resultClosure == null) {
                map?.resultClosure = errorListResultClosure  // add error list closure
                if (task instanceof GintTask) {
                    ((GintTask)task).closures.result = errorListResultClosure
                }
            }
            def cmd = cli?.trim() + (((map?.debug instanceof Boolean) ? map?.debug : ((parameters?.debug instanceof Boolean) ? parameters.debug : helper.isDebug())) ? ' -d ' : ' ') +
                            (map?.host ?: (parameters?.host ?: ''))
            //message 'debug', "cmd: ${cmd}, input: ${map?.standardInput}"
            if (this.gint.getVerbose()) {
                message 'info', "Ftp request: ${map?.request}"
            }
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
            return "${dirCmd} ${cmd} ${redirect}".toString()
        }
    }

    /**
     * Closure that will return a negative result if there are error list entries from running a command
     * @param - task
     * @return original result if not 0, or 0 if there are no errors in error list, -1 otherwise
     */
    def errorListResultClosure = { GintTask task ->
        //message 'debug', "errorListClosure for ${map?.name}"
        //message 'debug', "size: ${map?.parallel.getErrorList().size()}"
        return task.result != 0 ? task.result : (task.parallel?.getErrorList()?.size() == 0 ? 0 : -1)
    }

    /**
     * FindReplace CLI
     * @return closure
     */
    public CmdGenerator getCmdGeneratorForFindReplace(final Map parameters = null) {

        def cli = helper.getWithExtendedLookup(parameters?.cli, 'findReplaceCli', 'findReplace')
        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def action = helper.jsapParameter('action', (map?.action ?: parameters?.action ?: 'find'))
            def pattern = (map?.pattern != null) ? helper.jsapParameter('pattern', map?.pattern) : ''
            def replace = (map?.replace != null) ? helper.jsapParameter('replace', map?.replace) : ''

            def source = map?.source ?: parameters?.source
            source = source ? helper.jsapParameter('source', source) : ''

            def target = map?.target ?: parameters?.target
            target = target ? helper.jsapParameter('target', target) : ''

            def line = (map?.line == true) || ((map?.line == null) && (parameters?.line == true)) ? ' -l ' : ''
            def ignoreCase = (map?.ignoreCase == true) || ((map?.ignoreCase == null) && (parameters?.ignoreCase == true)) ? ' --ignoreCase ' : ''
            def literal = (map?.literal == true) || ((map?.literal == null) && (parameters?.literal == true)) ? ' --literal ' : ''
            def leaveBlank = (map?.leaveBlank == true) || ((map?.leaveBlank == null) && (parameters?.leaveBlank == true)) ? ' --leaveBlank ' : ''
            def simulate = map?.simulate || ((map?.simulate == null) && (parameters?.simulate == true)) ? ' --simulate ' : ''
            def showLines = (map?.showLines == true) || ((map?.showLines == null) && (parameters?.showLines == true)) ? ' --showLines ' : ''

            def include = (map?.include != null) ? helper.jsapParameter('include', map?.include) : ''
            def exclude = (map?.exclude != null) ? helper.jsapParameter('exclude', map?.exclude) : ''

            def transform = map?.transform ?: parameters?.transform
            transform = (transform != null) ? helper.jsapParameter('transform', transform) : ''

            def parameterString = getParameterStringForJsapGenerator(map)

            def verbose = (map?.verbose == true) ? ' -v ' : ''
            def debug = (map?.debug == true) || ((map?.debug != false) && helper.isDebug()) ? ' --debug ' : ''
            def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? this.gint.getOutputFile(map?.name) : map?.file)) : '')
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
            return "${cli} ${action} ${pattern}${replace}${include}${exclude}${source}${target}${transform}${line}${ignoreCase}${literal}${simulate}${parameterString}${leaveBlank}${showLines}${verbose}${debug} ${fileParameter} ${redirect}".toString()
        }
    }

    /**
     * Closure that generates a command string for image CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters - other CLI parameters
     * <ul>
     * Parameters:
     * - action
     * - file - file name or true to use standard output file name based on task name
     * - parameters
     * @param name of product
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForImage(final Map parameters) {

        def cli = helper.getWithExtendedLookup(parameters?.cli, 'imageCli', 'image')
        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def action = map?.action ?: map?.name // default action to name of task
            def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? this.gint.getOutputFile(map?.name) : map?.file)) : '')
            def parameterString = getParameterStringForJsapGenerator(map)
            //def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
            return "${cli} --action ${action} ${fileParameter} ${parameterString}"
        }
    }

    /**
     * Handle the parameters parameter as either a string, Map, or closure. Turn into a string
     * @param task
     * @return
     */
    def getParameterStringForJsapGenerator(Map map) {

        def parameters = map?.parameters
        if (parameters instanceof Closure) {
            parameters = parameters.call()  // handle closure as well as map and string
        }
        return (parameters instanceof Map) ? helper.jsapParameter(parameters) : parameters ?: ''
    }

    public Map getUserMap(final Task task) {
        if (task?.hasProperty(Constants.FIELD_NAME_USER_MAP)) {
            def userMap = task[Constants.FIELD_NAME_USER_MAP]
            if (userMap instanceof Map) {
                Map map = (Map) userMap
                if (map.cmdGeneratorParameterAugments instanceof Map) {
                    map += (Map) map.cmdGeneratorParameterAugments
                }
                return map
            }
        }
        //throw new Exception('Task class ' + map?.getClass().getName() + ' does not have a ' + Constants.FIELD_NAME_USER_MAP ' field.'")
        return null
    }
}
