/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.Path
import java.sql.SQLException
import java.util.regex.Pattern

import org.gint.plugin.Constants
import org.gint.plugin.Gint
import org.gint.plugin.GintUtils2
import org.gint.plugin.Constants.TaskType
import org.gint.tasks.GintTask
import org.gradle.api.Task
import org.gradle.api.logging.configuration.ConsoleOutput
import org.gradle.api.tasks.TaskState

import groovy.sql.Sql
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.xml.MarkupBuilder

//@CompileStatic
//@TypeChecked
public class ReportHelper extends BaseHelper{

    protected Closure message
    protected Helper helper

    protected String headerLine

    // protected String dbReport = null
    protected Sql dbReportConnection = null // dbReport parameter must represent a valid db profile
    protected File cmdLog = null // log file

    protected Closure reportFilter
    protected Closure cmdLogClosure = { entry -> entry + '\n' }

    protected Object textReport // could be true or a file name

    protected StringBuilder taskReport // for text report output
    protected StringBuilder finalReport = new StringBuilder() // always needed

    protected final static String PAD = '         ' // maximum tag size

    protected final String BLUE
    protected final String GREEN
    protected final String RED
    protected final String NO_COLOR

    protected final String RUN_SUCCESSFUL
    protected final String RUN_FAILED

    // non-tearDown tasks that failed AND had ignoreFailure=true
    protected List<String> quarantinedSetUpTasks = []
    protected List<String> quarantinedBaseTasks = []

    Closure appendLine = { String tag, String msg ->
        finalReport.append(Constants.EOL).append(PAD.substring(tag.size())).append('[').append(tag).append('] ').append(msg)
    }

    Closure appendLineRed = { String tag, String msg ->
        finalReport.append(Constants.EOL).append(PAD.substring(tag.size())).append(Constants.RED).append('[').append(tag).append('] ').append(msg).append(Constants.NO_COLOR)
    }

    Closure appendLineGreen = { String tag, String msg ->
        finalReport.append(Constants.EOL).append(PAD.substring(tag.size())).append(Constants.GREEN).append('[').append(tag).append('] ').append(msg).append(Constants.NO_COLOR)
    }

    Closure appendPretty = { String tag, String text, Object value ->
        appendLine tag, helper.formatText(text, value)
    }

    Closure appendTaskLine = { String tag, String msg ->
        if (taskReport != null) {
            taskReport.append(Constants.EOL).append(PAD.substring(tag.size())).append('[').append(tag).append('] ').append(msg)
        }
    }

    public ReportHelper(final Gint gint) {
        super(gint)
        message = gint.message
        helper = gint.helper
        textReport = gint.helper.getParameterValueConvertType('textReport', false) // TODO: should this be a configuration parameter?
        if (textReport) {
            taskReport = new StringBuilder()
        }
        if (doColorOutput()) {
            BLUE      = '\033[0;34m' // start blue
            GREEN     = '\033[0;32m' // start green
            RED       = '\033[0;31m' // start red
            NO_COLOR  = '\033[0m'    // always end color change back to no color
        } else {
            BLUE      = ''
            GREEN     = ''           // default to no color unless console request indicates otherwise
            RED       = ''
            NO_COLOR  = ''
        }
        RUN_SUCCESSFUL = Constants.GREEN + '    <<< RUN SUCCESSFUL' + Constants.NO_COLOR
        RUN_FAILED     = Constants.RED   + '    <<< RUN FAILED'     + Constants.NO_COLOR
    }

    /**
     * At least respect plain text request and not show color
     * @return
     */
    public doColorOutput() {
        def consoleOutput = gint.project.gradle.startParameter.consoleOutput
        //helper.log 'console', consoleOutput
        //helper.log 'has console', GintUtils2.hasConsole()
        def hasConsole = true // GintUtils2.hasConsole() doesn't work  // TODO
        return (consoleOutput == ConsoleOutput.Rich) || (consoleOutput == ConsoleOutput.Auto && hasConsole)
    }

    public void taskMessage(final String tag, final String text) {
        appendTaskLine tag, text // for text report
        message tag, text
    }

    /**
     * Finalize reports - normally case is one or more task report
     * @return result - normally true and ONLY set to false when something NOT tasks related fails so different final reporting can occur
     */
    public boolean finalizeReport() {

        gint.doReporting = false // prevent another report being done
        boolean result = true

        Exception savedException = null

        try {
            long endTime = System.currentTimeMillis()
            long elapsedTime = (endTime - gint.startTime)/1000 // in seconds

            def counts = getResultCounts()

            gint.logger.quiet '' // extra blank line before report, do not want it in builder though

            appendResultSummary(finalReport, endTime, elapsedTime, counts) // reporting

            finalReport.append Constants.EOL // blank line
            finalReport.append getOutputFooter() // finalize the logged output

            if ((counts.base.failed > 0) || (counts.setUp.failed > 0)) {
                gint.failed = true
                // DO NOT set result to false !!! See header
            } else if (gint.failed) {
                result = false // something not task related occurred, logging of error will be done later
            } else if (gint.failedMessageList?.size() > 0) {
                appendLine 'warning', "Some assert or exception failures noted during run but they were ignored or retries were successful."
                gint.failedMessageList.each { string -> appendLine 'warning', string }
            }

            if (gint.xmlReport) {
                File file = GintUtils2.getAbsoluteFile(new File(gint.xmlReport), true); // make sure parents created
                generateXmlReport(file, elapsedTime, counts.base.failed, counts.base.skipped + counts.base.notRun, counts.base.total)
            }

            if (textReport) { // text report requested
                generateTextReport()
            }

            result = prepareForDbReport() && result
            if (dbReportConnection != null) { // only proceed if report requested and no errors
                result = generateDbReport(endTime, elapsedTime, counts) && result
            }
            // TODO
            //            def mailReport = gint.helper.getParameterValueConvertType('mailReport', false)
            //            if (mailReport) {
            //                sendMailReport(mailReport, builder, (counts.base.failed > 0) || gint.getFailed())
            //            }
            //            def imReport = gint.helper.getParameterValueConvertType('imReport', false)
            //            if (imReport) {
            //                sendImReport(mailReport, builder, (counts.base.failed > 0) || gint.getFailed())
            //            }

        } catch (Exception exception) {
            appendLine 'error', "Exception finalizing report. Exception is: ${exception.toString()}"
            gint.failed = true
            result = false
            savedException = exception
        } finally {
            gint.logger.quiet(finalReport.toString())  // always log
            if (savedException) {
                gint.printStackTrace(savedException) // if requested
            }
        }
        //helper.log 'result end report', result
        //helper.log 'gint.failed', gint.failed
        return result
    }

    /**
     * Append result summary to builder
     */
    //@CompileStatic(TypeCheckingMode.SKIP)
    protected void appendResultSummary(final StringBuilder builder, final long endTime, final long elapsedTime, final Map<String, Map<String, Integer>> counts) {

        // only need failure summary if more than 1 error or 1 error and it didn't just happen
        //if ((counts.failed > 1) || ((lastResult || helper.isQuiet()) && (counts.failed > 0)))  {
        if (counts.base.failed > 0) { // TODO
            //builder.append Constants.EOL // blank line
            appendLine 'info', 'Failure summary:'

            for (boolean doTearDown = false; ; doTearDown = true) {

                gint.project.tasks.each() { entry ->
                    if ((entry instanceof GintTask)) {
                        GintTask task = (GintTask) entry

                        //task.debug()
                        if (task.state.executed && !task.state.skipped && !gint.tasksSkipped.contains(task)) {
                            if (task.success != true || task.failed == true) {
                                //if (!task.outputValues?.success && (task.ignoreFailure != true)) {
                                if (task.ignoreFailure != true) {
                                    //if ((!doTearDown && (task.tearDown != true)) || (doTearDown && (task.tearDown == true))) { // This is wrong use
                                    if ((!doTearDown && !task.isTearDownTask()) || (doTearDown && task.isTearDownTask())) {
                                        appendLineRed 'failed', (getFailedText(task) ?: '')
                                        if (task.failDetail && gint.showFailDetail) {
                                            appendLineRed 'reproduce', task.failDetail
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (doTearDown || !gint.helper.isVerbose()) {  // no tear down failure messages unless verbose set on gant level
                    break
                } else {
                    builder.append(Constants.EOL) // blank line
                    appendLine 'info', 'Tear down failures (ignored):'
                }
            }
            builder.append(Constants.EOL) // blank line between end of failure summary and report
        }
        //builder.append(Constants.EOL) // blank line between end of failure summary and report
        if (gint.stopNow) {
            appendLine 'info', 'Run stopped due to ' + ((counts.setUp.failed > 0) ? 'setUp failure.' : 'a specific request.')
        }

        //helper.log('counts', counts)
        //helper.logger.quiet('')

        // Normal counts reporting
        if (counts.base.failed > 0) {
            appendPretty 'info', 'Failed base tasks', counts.base.failed
            builder.append RUN_FAILED
            gint.failed = true
        }

        appendPretty 'info', 'Successful base tasks', counts.base.success
        builder.append ((counts.base.success > 0) && (counts.base.failed == 0) && (!gint.failed) ? RUN_SUCCESSFUL : '')

        if (counts.base.quarantined > 0) {
            appendPretty 'info', 'Quarantined base tasks', counts.base.quarantined
        }

        if (counts.base.notRun > 0) {
            appendPretty 'info', 'Not run base tasks', counts.base.notRun
        }
        if (counts.base.skipped > 0) {
            appendPretty 'info', 'Skipped base tasks', counts.base.skipped
            if (gint.skippedLevels.size() > 0) {
                if (gint.level < Integer.MAX_VALUE) {
                    appendPretty 'info', 'Level of run', gint.level
                }
                appendPretty 'info', 'Skipped levels', gint.skippedLevels
            }
        }
        appendPretty 'info', 'Total base tasks', counts.base.total

        // Restart counts reporting
        if ((counts.restarted.success > 0) || (counts.restarted.failed > 0)) {
            builder.append Constants.EOL // blank line
            appendPretty 'info', 'Failed tasks after retry', counts.restarted.failed
            appendPretty 'info', 'Successful tasks after retry', counts.restarted.success
        }

        // Setup counts reporting - 3.7.0 no longer conditioning showing setup tasks
        // if ((counts.setUp.total > 0) && ((counts.setUp.failed > 0) || (counts.setUp.quarantined > 0))) {
        builder.append Constants.EOL // blank line
        if (counts.setUp.failed > 0) {
            appendPretty 'info', 'Failed setUp tasks', counts.setUp.failed
            builder.append RUN_FAILED
        }
        appendPretty 'info', 'Successful setUp tasks', counts.setUp.success
        if (counts.setUp.notRun > 0) {
            appendPretty 'info', 'Not run setUp tasks', counts.setUp.notRun
        }
        if (counts.setUp.quarantined > 0) {
            appendPretty 'info', 'Quarantined setUp tasks', counts.setUp.quarantined
        }
        if (counts.setUp.skipped > 0) {
            appendPretty 'info', 'Skipped setUp tasks', counts.setUp.skipped
        }
        appendPretty 'info', 'Total setUp tasks', counts.setUp.total
        // }

        // TearDown counts reporting
        if ((counts.tearDown.total > 0) && (counts.tearDown.failed > 0)) {
            builder.append Constants.EOL // blank line
            if (counts.tearDown.failed > 0) {
                appendPretty 'info', 'Failed tearDown tasks', counts.tearDown.failed
            }
            appendPretty 'info', 'Successful tearDown tasks', counts.tearDown.success
            if (counts.tearDown.notRun > 0) {
                appendPretty 'info', 'Not run tearDown tasks', counts.tearDown.notRun
            }
            if (counts.tearDown.skipped > 0) {
                appendPretty 'info', 'Skipped tearDown tasks', counts.tearDown.skipped
            }
            appendPretty 'info', 'Total tearDown tasks', counts.tearDown.total
        }
        appendPretty 'info', 'Elapsed run time', GintUtils2.getFormattedElapsedTime(elapsedTime)

        if (quarantinedSetUpTasks.size() > 0 || quarantinedBaseTasks.size() > 0) {
            builder.append Constants.EOL // blank line
            appendPretty 'info', 'Quarantined tasks with ignored failures', quarantinedSetUpTasks + quarantinedBaseTasks
        }
    }

    /**
     * Get the text for the 'failed' message when a task fails
     * @param task
     * @return formatted text with standard task name and fail reason
     */
    protected String getFailedText(final GintTask task) {
        def builder = new StringBuilder()
        builder.append(task.name)
        if (helper.isNotBlank(task.description)) {
            builder.append(' (').append(task.description).append(')')
        }
        builder.append(':')
        builder.append(task.failReason?.startsWith('assert ') ? Constants.EOL : ' ')  // special case of assert so the power assert lines have correct columns
        if (task.failReason?.size() <= Constants.DISPLAY_LIMIT_SUMMARY) {
            builder.append(task.failReason?.trim())
        } else {
            builder.append(task.failReason?.substring(0, Constants.DISPLAY_LIMIT_SUMMARY).trim())
        }
        return builder.toString()
        //"${task.name + (task.description ? ' (' + task.description + ')' : '')}:\n${task.failReason?.substring(0, [task?.failReason?.size(), Constants.DISPLAY_LIMIT_SUMMARY].min())}".trim()
    }

    /**
     * Get result counts
     * @return map of counts
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected LinkedHashMap<String, LinkedHashMap<String, Integer>> getResultCounts() {

        LinkedHashMap<String, LinkedHashMap<String, Integer>> counts = [
            base:      [ success: 0, quarantined: 0, skipped: 0, failed: 0, notRun: 0, total: 0 ],
            setUp:     [ success: 0, quarantined: 0, skipped: 0, failed: 0, notRun: 0, total: 0 ],
            tearDown:  [ success: 0, quarantined: 0, skipped: 0, failed: 0, notRun: 0, total: 0 ],
            //internal:  [ success: 0, skipped: 0, failed: 0, notRun: 0, total: 0 ],
            restarted: [ success: 0,             failed: 0 ], // tasks that were retried and subsequently were successful or failed
            failedList: [],      // failed task names - not to include setUp or tearDown
            failedSetUpList: [], // failed setup
        ]
        gint.project.tasks.matching { it instanceof GintTask }.all { GintTask task ->
            //gint.project.tasks.each() { entry ->

            //logger.quiet('task: {}, task object: {}, is gint: {}', object.getName(), object.getClass().getName(), object instanceof GintTask)

            TaskState state = task.getState()

            String type = task.type == TaskType.SET_UP ? 'setUp' : (task.type == TaskType.TEAR_DOWN  || task.type == TaskType.TEAR_DOWN_AFTER) ? 'tearDown' : 'base'

            counts[type].total++

            //                if (type != Constants.TYPE_INTERNAL) {
            //                    println ''
            //                    helper.log('type', type)
            //                    task.debug()
            //                    println ''
            //                }

            if (state.skipped || gint.tasksSkipped.contains(task)) {
                counts[type].skipped++

                // Note: special case tearDown - they will be marked as failed since they will NOT fail the test like base tasks would if we allowed them to be counted as failed
            } else if (((type == 'tearDown') || !task.ignoreFailure) && (task.failed || state.failure != null)) {
                counts[type].failed++
                if (type != 'tearDown') { // ignore tear down failures or explicitly requested to ignore
                    if (type == 'setUp') {
                        counts.failedSetUpList << task.getName()
                    } else {
                        counts.failedList << task.getName()
                    }
                }
            } else if (state.executed) {
                if ((type == 'setUp') && quarantinedSetUpTasks.contains(task.name) ||
                    (type == 'base')  && quarantinedBaseTasks.contains(task.name)) {
                    counts[type].quarantined++
                } else {
                    counts[type].success++
                }
                // println('success task: ' + entry.name + ', type: ' + type)
            }
            // TODO restarted
        }

        //helper.log('failed list', counts.failedList)
        gint.saveFailedList(counts.failedSetUpList + counts.failedList)  // remembered in property file for next default start

        ['base', 'setUp', 'tearDown'].each { type ->
            counts[type].notRun = counts[type].total - counts[type].success - counts[type].quarantined - counts[type].failed - counts[type].skipped
        }

        //gint.helper.log('counts', counts)
        return counts
    }

    /**
     * Prepare for XML reporting. Set xmlReport based code setting or input parameter.
     */
    protected void prepareForXmlReport() {

        // parameter setting will override code setting
        gint.xmlReport = helper.getParameterValueConvertType('xmlReport', gint.xmlReport == null ? false : gint.xmlReport)
        if (gint.xmlReport == true) {
            gint.xmlReport = gint.getTestReportsDirectory() + System.properties.'file.separator' + getDefaultXmlReportName() // default name
        } else if (gint.xmlReport instanceof String) {
            def file = GintUtils2.getAbsoluteFile(gint.xmlReport)
            if (file.isDirectory() || (!file.exists() && !gint.xmlReport.contains('.'))) { // directory or already exists or name looks like a file name
                if (!file.exists()) {
                    file.mkdirs()  // creates all directories needed for the path
                }
                gint.xmlReport += System.properties.'file.separator' + getDefaultXmlReportName()
            }
        }
        //helper.log('xmlReport', gint.xmlReport)
    }

    /**
     * Default XML report name
     * @return
     */
    protected String getDefaultXmlReportName() {
        return gint.getName().toLowerCase() + '.xml'
    }

    /**
     * Default text report name
     * @return
     */
    protected String getDefaultTextReportName() {
        return gint.getName().toLowerCase() + '.txt'
    }

    /**
     * Prepare for DB reporting. Report any problems connecting to database is requested
     * Note that dbReportConnection will be null if no report is requested.
     * <pre>
     * A profile looks like:
     * dbUrl = jdbc:postgresql://host:port:db | dbUser = user | dbPassword = password | logTable = gint_logs
     * </pre>
     * @return true if successful or no report needed
     */
    protected boolean prepareForDbReport() {

        boolean result = true

        // If not already configured, see if gint parameter was parameter
        gint.dbReport = gint.dbReport != null ? gint.dbReport : helper.getParameterValue('dbReport', null)

        //message 'debug', 'dbReport: ' + gint.dbReport
        //message 'debug', 'dbReport class: ' + dbReport?.getClass()?.getName()

        if (gint.dbReport instanceof String || gint.dbReport instanceof GString) {
            gint.dbReport = gint.dbReport.trim()
            if (gint.helper.isNotBlank(gint.dbReport)) {
                if (gint.dbUser == null) {
                    gint.dbUser = System.getenv('GINT_DB_USER')
                }
                if (gint.dbPassword == null) {
                    gint.dbPassword = System.getenv('GINT_DB_PASSWORD')
                }
                if (gint.dbRunTable == null) {
                    gint.dbRunTable = System.getenv('GINT_DB_RUN_TABLE') // will default if not set
                }
                if (gint.dbLogTable == null) {
                    gint.dbLogTable = System.getenv('GINT_DB_LOG_TABLE') // optional, should not defaulted!!
                }
                try {
                    if (gint.dbReport.matches('.*?dbUrl *=.*')) {
                        dbReportConnection = gint.sqlHelper.getConnectionFromProfile(gint.dbReport)
                    } else { // not the most elegant solution, but get connection into one code path after this
                        def builder = new StringBuilder()
                        builder.append("dbUrl=").append(gint.dbReport) // report is the url
                        if (gint.dbUser != null) {
                            builder.append(" | ")
                            builder.append("dbUser=").append(gint.dbUser)
                        }
                        if (gint.dbPassword != null) {
                            builder.append(" | ")
                            builder.append("dbPassword=").append(gint.dbPassword)
                        }
                        dbReportConnection = gint.sqlHelper.getConnectionFromProfile(builder.toString())
                    }
                } catch (SQLException exception) {
                    gint.addFailedMessage('Problem preparing for the DB report. Exception: ' + exception.toString())
                    result = false
                }
                catch (Exception exception) {
                    gint.addFailedMessage('Problem with dbReport setup or configuration. Exception: ' + exception.toString())
                    result = false
                }
                //message 'debug', 'dbReportConnection: ' + dbReportConnection
            }
        }
        return result
    }

    /**
     * Prepare for cmd logging
     * @return
     */
    protected prepareForCmdLog() {
        def cmdLogName = helper.getParameterValue('cmdLog', null)
        if (cmdLogName != null) {
            cmdLogName = cmdLogName.trim()
            if (cmdLogName == '') { // cmdLog requested, but no name given
                cmdLogName = getOutputFile('log') // default name
            } else {
                cmdLog = new File(cmdLogName)
                if (cmdLog.isDirectory()) {
                    cmdLog = new File(cmdLogName + System.properties.'file.separator' + 'log.txt')
                }
            }
            try {
                cmdLog = new File(cmdLogName)
                cmdLog.write('')  // clear
            } catch (FileNotFoundException exception) {
                message 'warning', "Command logging disabled, problems with ${cmdLogName}. " + exception.toString()
                cmdLog = null
            }
        }
    }

    /**
     * Generate text report
     * @return
     */
    protected void generateTextReport() {
        //File file = GintUtils2.getAbsoluteFile(new File(gint.xmlReport), true); // make sure parents created
        if (textReport == true) {
            textReport = gint.getOutputDirectory() + System.properties.'file.separator' + getDefaultTextReportName() // default name
        } else if (textReport instanceof String) {
            def file = GintUtils2.getAbsoluteFile(textReport);
            if (file.isDirectory() || (!file.exists() && !textReport.contains('.'))) { // directory or already exists or name looks like a file name
                if (!file.exists()) {
                    file.mkdirs() // creates all directories needed for the path
                }
                textReport += System.properties.'file.separator' + getDefaultTextReportName()
            }
        }
        File file = GintUtils2.getAbsoluteFile(textReport, true); // make sure parents created
        //helper.log('text report', file.getAbsoluteFile())
        def outputStream = new FileOutputStream(file)
        def writer = new BufferedWriter(new OutputStreamWriter(outputStream, (String) gint.encoding ?: Charset.defaultCharset()))
        writer.write(headerLine)
        writer.write(Constants.EOL)
        writer.write(taskReport.toString())
        writer.write(Constants.EOL)
        writer.write(finalReport.toString())
        writer.write(Constants.EOL)
        writer.flush()
        writer.close()
    }

    /**
     * Generate and write an XML Report - JUnit like (http://ant.1045680.n5.nabble.com/schema-for-junit-xml-output-td1375274.html)
     * @return
     */
    protected void generateXmlReport(final File reportFile, final elapsedTime, final failures, final skipped, final total) {

        def xmlOutput = new FileOutputStream(reportFile)
        def writer = new OutputStreamWriter(xmlOutput, Constants.XML_ENCODING)
        writer.write '<?xml version="1.0"?>\n' // clear and write
        def builder = new MarkupBuilder(writer)

        builder.testsuite(time: elapsedTime, failures: "${failures}", errors: "0", skipped: "${skipped}",
        tests: "${total}", name: gint.getName()) {
            def filter = getReportFilter()
            if (filter != null) {  // default of null means no properties in report
                try {
                    def passwordFilter = helper.getPasswordFilter() // ALWAYS apply the password filter (not optional)
                    gint.project.ant.properties.each { entry ->
                        if (!filter(entry.key) && !passwordFilter(entry.key)) { // exclude some or all properties from the report file
                            builder.property(name: entry.key, value: entry.value)
                        }
                    }
                } catch (Exception exception) {
                    message 'warning', 'Ignoring problems adding properties to XML report.'
                    if (helper.isDebug()) {
                        exception.printStackTrace()
                    }
                }
            }
            gint.project.tasks.each() { entry ->
                if (entry instanceof GintTask) {
                    GintTask task = (GintTask) entry
                    TaskState state = task.getState()

                    if (task.type == TaskType.BASE) {  // only include base tasks in XML report
                        //println 'name: ' + task.name + ', success: ' + task.success + ', ignoreFailure: ' + task.ignoreFailure + ', skipped: ' + task.skipped + ', state: ' + state.skipped
                        if (state.executed && !state.skipped && !task.skipped) {  // only tasks that were run
                            if (task.ignoreFailure) { // mark ignored
                                // Still shows as successful in Bamboo
                                builder.testcase(name: task.name,  time: taskElapsedTime(task)/1000, status: 'ignored') // https://stackoverflow.com/questions/8157773/what-does-ignore-do-to-junit-xml-output
                            } else {
                                builder.testcase(name: task.name, time: taskElapsedTime(task)/1000) {
                                    if (!task.success) {
                                        builder.failure(message: task.failReason, type: 'junit.framework.ComparisonFailure', task.failDetail)
                                    }
                                }
                            }
                        } else { // if its not run, then need to consider it skipped from an XML report perspective
                            builder.testcase(name: task.name) { builder.skipped() }
                        }
                    }
                }
            }
            }
        writer << System.properties.'line.separator'
        writer.close()
    }

    protected long taskElapsedTime(final GintTask task) {
        return ((task?.endTime == null) || (task?.startTime == null)) ? 0 : task.endTime - task.startTime
    }

    protected boolean generateDbReport(final endTime, final elapsedTime, final Map counts) {

        boolean result = true
        //message 'debug', 'dbReport: ' + dbReport
        def runTable = gint.helper.getValueFromKeyValueString(gint.dbReport, 'runTable') ?: gint.dbRunTable
        if (runTable == null) {
            runTable = 'gint_runs'
        }

        if (helper.isNotBlank(runTable)) {

            def metadataColumnList = gint.sqlHelper.getColumns(dbReportConnection, runTable)

            java.sql.Timestamp startTimestamp = new java.sql.Timestamp(gint.startTime)

            def skippedLevelList = []
            gint.skippedLevels.each { level ->
                skippedLevelList << level.toString()  // this takes care of boolean for example
            }

            // check these variables are strings before use in case they are tasks (closures), otherwise use blank - avoids SQL update problems
            // allow parameter value to override a program setting
            def runRepository  = gint.helper.getParameterValue('runRepository', gint.runRepository)
            def runBranch      = gint.helper.getParameterValue('runBranch', gint.runBranch)
            def runCommit      = gint.helper.getParameterValue('runCommit', gint.runCommit)
            def runBuild       = gint.helper.getParameterValue('runBuild', gint.runBuild)
            def runVersion     = gint.helper.getParameterValue('runVersion', gint.runVersion)
            def runEnvironment = gint.helper.getParameterValue('runEnvironment', gint.runEnvironment)

            // CI defaulting support at least for Bitbucket
            def isPipeline = System.getenv('CI')  // Bitbucket and some other CI platforms have this set
            if (isPipeline && (isPipeline != 'false')) {
                // default to bitbucket values if not already defined
                // if not desired, user should set them to blank ahead of time
                if (runRepository == null) {
                    runRepository  = System.getenv('BITBUCKET_REPO_FULL_NAME')
                }
                if (runBranch == null) {
                    runBranch      = System.getenv('BITBUCKET_BRANCH')
                }
                if (runCommit == null) {
                    runCommit      = System.getenv('BITBUCKET_COMMIT')
                }
                if (runBuild == null) {
                    runBuild       = System.getenv('BITBUCKET_BUILD_NUMBER')
                }
                if (runEnvironment == null) {
                    runEnvironment = System.getenv('BITBUCKET_DEPLOYMENT_ENVIRONMENT')
                }
            }
            def parameters = [
                connection: dbReportConnection,
                table: runTable,
                logExceptions: gint.verbose,
                valueMap: [
                    test_name: gint.name,  // TODO
                    start_time: gint.startTime == null ? null : startTimestamp,
                    end_time: endTime == null ? null : new java.sql.Timestamp(endTime),
                    elapsed_seconds: elapsedTime, // in seconds?
                    gint_version: gint.getVersion(),
                    run_repository: runRepository instanceof String ? runRepository : '',
                    run_build: runBuild instanceof String ? runBuild : '',  // likely build key
                    run_version: runVersion instanceof String ? runVersion : '', // likely the version of the code being tested if available
                    run_environment: runEnvironment instanceof String ? runEnvironment : '', // other optional environment data like server instance for example

                    skipped_levels: helper.listToSeparatedString(list: skippedLevelList, separator: ','),
                    auto_retry: gint.autoRetry,

                    // Normal
                    successful:           counts.base.success,
                    failed:               counts.base.failed,
                    not_run:              counts.base.notRun,
                    skipped:              counts.base.skipped,
                    total:                counts.base.total,

                    // Retry
                    restarted_successful: counts.restarted.success,
                    restarted_failed:     counts.restarted.failed,

                    // Set up
                    setup_successful:     counts.setUp.success,
                    setup_failed:         counts.setUp.failed,
                    setup_not_run:        counts.setUp.notRun,
                    setup_skipped:        counts.setUp.skipped,
                    setup_total:          counts.setUp.total,

                    // Tear down
                    teardown_successful:  counts.tearDown.success,
                    teardown_failed:      counts.tearDown.failed,
                    teardown_not_run:     counts.tearDown.notRun,
                    teardown_skipped:     counts.tearDown.skipped,
                    teardown_total:       counts.tearDown.total,

                    // environment
                    work_directory: System.properties.'user.dir',
                    ip_address: java.net.InetAddress.getLocalHost().toString(),
                    os: System.properties.'os.name',
                    os_version: System.properties.'os.version',
                    java_version: System.properties.'java.runtime.version',
                ],
            ]

            // new fields added in 3.7.0, only use them if the table contains the columns
            if (metadataColumnList.contains('suite')) {
                parameters.valueMap.suite = gint.suite ?: ''
            }

            // new fields added in 3.6.0, only use them if the table contains the columns
            if (metadataColumnList.contains('run_branch')) {
                parameters.valueMap.run_branch = runBranch ?: ''
            }
            if (metadataColumnList.contains('run_commit')) {
                parameters.valueMap.run_commit = runCommit ?: ''
            }

            // new fields added in 3.7.0, only use them if the table contains the columns
            if (metadataColumnList.contains('quarantined')) {
                parameters.valueMap.quarantined = counts.base.quarantined
            }
            if (metadataColumnList.contains('setup_quarantined')) {
                parameters.valueMap.setup_quarantined = counts.setUp.quarantined
            }
            if (metadataColumnList.contains('quarantined_base_tasks')) {
                parameters.valueMap.quarantined_base_tasks = quarantinedBaseTasks
            }
            if (metadataColumnList.contains('quarantined_setup_tasks')) {
                parameters.valueMap.quarantined_setup_tasks = quarantinedSetUpTasks
            }
            if (metadataColumnList.contains('failed_base_tasks')) { // 3.7.1
                parameters.valueMap.failed_base_tasks = counts.failedList
            }
            if (metadataColumnList.contains('failed_setup_tasks')) { // 3.7.1
                parameters.valueMap.failed_setup_tasks = counts.failedSetUpList
            }

            try {
                gint.sqlHelper.runInsert(parameters)

                // find the id of the row we just added above
                def runId = gint.sqlHelper.getFirstColumn([
                    connection: dbReportConnection,
                    table: runTable,
                    verbose: false,
                    sql: "select id from ${runTable} where start_time = '${startTimestamp}'",
                ])

                message 'info', 'Run summary row added to DB report run table ' + runTable + ' with id ' + runId + '.'

                def count = addDbReportLogRows(runId)

            } catch (SQLException exception) {
                gint.addFailedMessage('Problem with the DB report run table. Exception: ' + exception.toString())
                result = false
            } finally {
                try {
                    dbReportConnection.close()  // free resources
                    dbReportConnection = null
                } catch (Exception ignore) {
                }
            }
        }
        return result
    }

    /**
     * Add a row for each task to the log table
     */
    protected int addDbReportLogRows(final runId) {

        int count = 0

        def logTable = helper.getValueFromKeyValueString(gint.dbReport, 'logTable') ?: gint.dbLogTable // not defaulted, example name would be gint_log
        if (helper.isNotBlank(logTable)) {

            try {
                // Only log tasks that were run and started (not failed before start)

                def metadataColumnList = gint.sqlHelper.getColumns(dbReportConnection, logTable)

                gint.project.tasks.matching { it instanceof GintTask }.all { GintTask task ->

                    if (task.startTime != null) {
                        // helper.logWithFormat('task', task)
                        // helper.logWithFormat('task name', task.name)

                        String cmd = task.cmd?.toString()
                        cmd = (helper.isBlank(cmd) ? null : cmd.trim())

                        def level = task.level
                        if (level instanceof Closure) {
                            try {
                                level = (level.getMaximumNumberOfParameters() > 0) ? level(task) : level()
                            } catch (Exception ignore) {
                                level = ''
                            }
                        }
                        // special case expected closure matches against result for true or false
                        def expectedAsInteger
                        try {
                            expectedAsInteger = getValueAsInteger(task.expected instanceof Closure ? task.expected(task.result) : task.expected)
                        } catch (Exception ignore) {
                            expectedAsInteger = 0
                        }
                        def parameters = [
                            connection: dbReportConnection,
                            table: logTable,
                            logExceptions: gint.verbose,
                            valueMap: [
                                run_id: runId,  // run summary record this is associated to
                                testcase_name: task.name,
                                cmd: cmd,
                                // inline: testcase.inline,
                                is_set_up: getValueAsBoolean(task.isSetUp),
                                is_tear_down: getValueAsBoolean(task.isTearDown),
                                cmd_log: task.cmdLog != false,
                                level: level,

                                result: getValueAsInteger(task.result),
                                expected: expectedAsInteger,
                                ignore_failure: getValueAsBoolean(task.ignoreFailure),
                                success: getValueAsBoolean(task.success),
                                restart_count: getValueAsInteger(task.restartCount),

                                start_time: task.startTime == null ? null : new java.sql.Timestamp(task.startTime),
                                end_time: task.endTime == null ? null : new java.sql.Timestamp(task.endTime),
                                elapsed_seconds: ((task?.endTime == null) || (task?.startTime == null)) ? null : (task.endTime - task.startTime)/1000,
                            ],
                        ]
                        // new fields added in 3.6.0, only use them if the table contains the columns
                        // note that outData is removed after task run, so is NOT available for the log
                        if (metadataColumnList.contains('fail_reason')) {
                            parameters.valueMap.fail_reason = task.failReason ?: ''
                        }
                        if (metadataColumnList.contains('fail_detail')) {
                            parameters.valueMap.fail_detail = task.failDetail ?: ''
                        }
                        if (metadataColumnList.contains('quarantined')) {
                            parameters.valueMap.quarantined = getValueAsBoolean(!task.isTearDown && task.ignoreFailure && task.failed)
                        }
                        count += gint.sqlHelper.runInsert(parameters)
                    }
                }

                message 'info', count + ' rows added to DB report log table ' + logTable + '.'

            } catch (SQLException exception) {
                gint.addFailedMessage('Problem with the DB report log table. Exception: ' + exception.toString())
                count = -1
            }
        }
        return count
    }

    protected boolean getValueAsBoolean(value) {
        //if (value instanceof Closure) { // TODO: looks like an error from old code
        //    value = value(task)
        //}
        if (value instanceof Boolean) {
            return value
        } else if (value instanceof Integer) {
            return value == 0 ? false : true
        }
        return (value == null ? false : true)
    }

    protected int getValueAsInteger(value) {
        //if (value instanceof Closure) { // TODO: looks like an error from old code
        //    value = value(task.result)
        //}
        if (value instanceof Integer) {
            return value
        } else if (value instanceof Boolean) {
            return (value ? 0 : -1)
        } else if (value instanceof Collection) {
            return 0
        } else if (value instanceof String || value instanceof GString) {
            return helper.getIntegerValue(value)
        }
        return 0
    }

    /**
     * Adds to the command log, user can customize using the cmdLogClosure
     * @param entry to be added to the log
     */
    public void addCmdLog(final entry) {
        if (cmdLog) {
            cmdLog << cmdLogClosure.call(entry)
        }
    }

    /**
     * Handle result message after the task has been run. put out a meaningful message on why a task failed.
     */
    public void handleResultMessage(final GintTask task, boolean ignoreFailure = false) {

        if (task.success) {
            if (gint.getVerbose()) {
                taskMessage 'success', task.name + (task.cmd ? ': ' + task.cmd.toString() : '')
            }
        } else { // task failed
            // go in reverse order of checking as the values are not set if that condition was not checked because of earlier failure

            if (task.failReason == null) {  // fail reason not already set
                task.failReason = getFailReason(task)
            }
            def messageLength = [
                task.failReason.size(),
                gint.getVerbose() ? Constants.DISPLAY_LIMIT : Constants.DISPLAY_LIMIT_SUMMARY
            ].min()
            taskMessage task.ignoreFailure || ignoreFailure ? (task.isTearDown ? 'teardown' : 'ignore') : 'failed', "${task.name}: ${task.failReason.substring(0, messageLength)}"
            if (task.cmd) {
                taskMessage 'reproduce', task.cmd.toString() // toString to handle cmd array
                if (gint.showFailDetail) {
                    task.failDetail = task.cmd.toString() + ((task.standardInput != null) ? '\n' + helper.getValueHandleClosure(task.standardInput) : '')
                }
            }
            if (!task.success && helper.isDebug()) {
                helper.log('task', task)
            }
        }
    }

    //    (task.closures.successResult == false ?
    //    //: (task.imResult?.after == false ? "IM after response indicated failure."
    //    //: (task.compareSuccess == false ? "File compare failed."
    //    : getOutputFailReason() ?: (task.notFound == false ? "Unexpected data ${helper.quoteString(helper.getValueHandleClosure(task.failData, task))} ${fragment}"
    //    : (task.found == false ? "Expected data ${helper.quoteString(helper.getValueHandleClosure(task.data, task))} not ${fragment}"
    //    // : (task.imResult?.before == false ? "IM before response indicated failure."
    //    : (!isExpectedResult(task.result, task.expected) ?
    //    "Expected ${(task.expected != null) ? task.expected.toString() : 0 } but got ${task.result}"
    //    : '' ) ) ) )
    protected String getFailReason(final GintTask task) {
        String reason = '' // sets the reason to something even if nothing
        if (task.closures.successResult == false) {
            reason = "Success closure failed."
            //} else if (task.imResult?.after == false) {
            //    reason = "IM after response indicated failure."
            //} else if (task.compareSuccess == false) {
            //    reason = "File compare failed."
        } else {
            String outputFailReason = getOutputFailReason(task, task.output)
            if (outputFailReason != null) {
                reason = outputFailReason
            } else {
                if (task.notFound == false) {
                    reason = "Unexpected data ${helper.quoteString(helper.getValueHandleClosure(task.failData, task))} found in output: ${task.outData}"
                } else if (task.found == false) {
                    reason = "Expected data ${helper.quoteString(helper.getValueHandleClosure(task.data, task))} not found in output: ${task.outData}"
                    //} else if (task.imResult?.before == false) {
                    //    reason = "IM before response indicated failure."
                } else if (!task.isExpectedResult(task.result, task.expected)) {
                    reason = "Expected ${(task.expected != null) ? task.expected.toString() : 0 } but got ${task.result}"
                }
            }
        }
        return reason?.size() > 200 ? reason.substring(0, 200) + ' ...' : reason
    }

    //    : ((task.output instanceof Map) && (task.output.exists == false) ? "Could not find expected output file ${task.output.file}"
    //        : ((task.output instanceof Map) && (task.output.notFound == false) ? "Unexpected data ${helper.quoteString(helper.getValueHandleClosure(task.output.failData, task))} found in file ${task.output.file}"
    //        : ((task.output instanceof Map) && (task.output.found == false) ? "Expected data ${helper.quoteString(helper.getValueHandleClosure(task.output.data, task))} not found in file ${task.output.file}"
    protected String getOutputFailReason(final GintTask task, final entry) {

        String reason = null
        if (entry instanceof Map) {
            if (entry.exists == false) {
                reason = "Could not find expected output file ${entry.file}"
            } else if (entry.notFound == false) {
                reason = "Unexpected data ${helper.quoteString(helper.getValueHandleClosure(entry.failData, task))} found in file ${entry.file}"
            } else if (entry.found == false) {
                reason = "Expected data ${helper.quoteString(helper.getValueHandleClosure(entry.data, task))} not found in file ${entry.file}"
            }
        } else if (entry instanceof List) {
            entry.each { map ->
                if (reason == null && (map instanceof Map)) { // quit on first Map that has an error message
                    reason = getOutputFailReason(task, (Map) map)
                }
            }
        }
        return reason?.size() > 200 ? reason.substring(0, 200) + ' ...' : reason
    }

    public logGintTaskGraph() {

        gint.logger.quiet 'Report on Gint tasks with depends on tasks:'

        def doneList = [
            Constants.TASK_FINALIZE,
            Constants.TASK_FAILED
        ]
        gint.project.tasks.each() { task ->
            //if ((task instanceof GintTask) && !(task.name in doneList)) {
            gint.logger.quiet(task.description == null ? '  {}' : '  {} - {}', task, task.description)
            logDependsOn(task, doneList)
            //task.dependsOn.forEach { entry ->
            //doneList << entry.name
            //    gint.logger.quiet "  - " + entry
            //}
            // }
        }
        gint.logger.quiet ''
    }

    protected logDependsOn(final task, final List<String> doneList) {
        task.dependsOn.forEach { entry ->
            if (entry instanceof Task) {
                doneList << entry.name
                gint.logger.quiet "  - " + entry
                logDependsOn(entry, doneList)
            } else if (entry instanceof String) {
                gint.logger.quiet "  - " + entry
            }
        }
    }

    /**
     * Set report filter to filter out properties in the standard XML junit report
     * <pre>
     * Filter closure should take one parameter and return true if value is to be filter (not logged).
     * Filter pattern will be used as a stringContains pattern
     * Filter string will be used as a case insensitive, literal pattern
     * </pre>
     * @param filter pattern or closure returning boolean - example: { text -> return helper.stringContains(text, ~/(?i)password/) }
     */
    public void setReportFilter(final filter) {
        reportFilter = (
                        (filter instanceof Closure) ? filter
                        : (filter instanceof Pattern  ? { text ->
                            this.helper.stringContains(text, filter) }
                        : (filter instanceof String   ? { text ->
                            this.helper.stringContains(text, filter, Pattern.CASE_INSENSITIVE | Pattern.LITERAL) }
                        : null)))
    }

    /**
     * Get the report filter
     * @return closure to filter out properties in the report
     */
    public Closure getReportFilter() {
        return reportFilter
    }

    protected String getOutputHeader() {
        // remember header for summary reporting so start date is right
        headerLine = '= = = = = =   ' + gint.name + ' started at   ' + new Date() + '   = = = = = =' + Constants.EOL
        return Constants.EOL + headerLine
    }
    protected String getOutputFooter() {
        return Constants.EOL + '= = = = = =   ' + gint.name + ' completed at ' + new Date() + '   = = = = = =' + Constants.EOL
    }
}
