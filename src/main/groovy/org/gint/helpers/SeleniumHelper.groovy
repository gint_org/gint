/*
 * Copyright (c) 2009, 2019 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.helpers

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.TimeoutException
import java.time.Duration

import org.gint.plugin.Gint
import org.gint.tasks.GintTask
import org.openqa.selenium.*
import org.openqa.selenium.support.ui.*
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.edge.EdgeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.ie.InternetExplorerOptions
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.safari.SafariDriver
import org.openqa.selenium.safari.SafariOptions
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode


@CompileStatic
@TypeChecked
public class SeleniumHelper extends BaseHelper {

    protected driver // WebDriver or RemoteWebDriver
    protected String browser
    protected String hub
    protected int defaultWait = helper.getIntegerParameterValue('wait', 10) // seconds
    protected Map findDefaultParameters = [wait: defaultWait]
    protected Integer delay // standard delay parameter to help view selenium scripts as the run
    public GintTask closeTask

    static protected int TABLE_DEFAULT_SLEEP = 1000 // milliseconds - some intermittent failures if things go too fast

    public SeleniumHelper(final Gint gint) {
        super(gint)
    }

    protected List driverList = [] // remember drivers for clean up
    public getDriver() {
        return driver
    }

    public void setBrowser(final String browser) {
        this.browser = browser
    }

    /**
     * Get browser parameter or the default
     */
    public String getBrowser() {
        if (browser == null) {
            browser = helper.getParameterValue('browser', 'chrome').toLowerCase()
        }
        return browser
    }

    public String getHub() {
        if (hub == null) {
            hub = helper.getParameterValue('seleniumHub', System.getenv('SELENIUM_HUB')) // hub url like https://hub.examplegear.com/wd/hub
        }
        return hub
    }

    public boolean isChrome() {
        return browser == 'chrome'
    }

    public boolean isFirefox() {
        return browser == 'firefox'
    }

    public boolean isSafari() {
        return browser == 'safari'
    }

    public boolean isIe() {
        return browser == 'ie'
    }

    public boolean isEdge() {
        return browser == 'edge'
    }

    /**
     * Replace selenium qualifier with browser name for script name to use as the selenium name
     *
     * @param script
     * @return
     */
    public String getScriptSeleniumName(script) {
        return script.getClass().getName().replaceFirst(~'(?i)selenium', getBrowser())
    }

    // set delay time in milli seconds
    public void setDelay(int delay) {
        this.delay = delay
    }

    // get delay time in milli seconds
    public int getDelay() {
        if (delay == null) {
            delay = helper.getIntegerParameterValue('delay', 0) * 1000  // default is parameter value in seconds
        }
        return delay
    }

    public void delay() {
        sleep(getDelay())
    }

    /**
     * Set default parameters like visible, present, wait, serverInfo for Atlassian server info
     * @param parameters
     */
    public void setFindDefaultParameters(Map parameters) {
        this.findDefaultParameters = parameters
    }

    public Map getFindDefaultParameters() {
        return findDefaultParameters
    }

    /**
     * If you don't want automatic driver close support and need to close it specifically
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public GintTask addCloseTask() {
        if (closeTask == null) {
            closeTask = gint.taskHelper.add([
                name: 'seleniumClose',
                inline: { gint.seleniumHelper.close() },
            ])
        }
        return closeTask
    }

    /**
     * Add initialize task. Note, no need for a close task as automatic cleanup will work better
     */
    public GintTask addInitializeTask(Map<String, Object> map = null) {
        gint.taskHelper.add(getInitializeTaskMap(map))
    }

    public GintTask addLoginTask(Map<String, Object> map = null) {
        gint.taskHelper.add(getLoginTaskMap(map))
    }

    /**
     * Default initialize selenium task map
     * @param browser
     * @param hub
     * @param capabilities - browser capabilities, recommend starting with getDefaultCapabilities() and then adding more
     * @param retry - number of retries if trouble with initialization
     * @param timeout - timeout
     * @return map to initialize selenium
     */
    public Map getInitializeTaskMap(Map<String, Object> map = null) {
        def browser = map?.browser ?: getBrowser() // gets from parameter or properties if not set
        def hub = map?.hub ?: getHub() // gets from parameter or properties if not set

        return [
            name: map?.name ?:'initializeSelenium', description: map?.description ?: 'Initialize to run browser selenium tasks',
            neededBy: map?.containsKey('neededBy') ? map.neededBy : 'gintBase', // default to all tasks needing this to be done first unless specified not to
            dependsOn: map?.dependsOn,
            mustRunBefore: map?.mustRunBefore,
            mustRunAfter: map?.mustRunAfter,
            inline: {
                def driver = initialize(browser: browser, hub: hub, capabilities: map?.capabilities) // default will be chrome
                if ((driver == null) && map?.stopOnFail != false) {  // Not explicitly requested to keep going on fail
                    gint.stopNow = true
                    return false
                }
                return true
            },
            data: map?.data,
            startClosure: map?.startClosure,
            resultClosure: map?.resultClosure,
            finalClosure: map?.finalClosure,
            retry: map?.retry ?: 2, // sometimes the driver doesn't initialize, especially have seen this with safari
        ] + (gint.timeout > 0 ? [:] : [timeout: map?.timeout ?: 120000]) // if timeout not explicitly set, then force a reasonable limit - 2 minutes
    }

    /**
     * Follow a standard login protocol
     * @param map with the following parameters
     * @param url - (required or via serverInfo)
     * @param userField - (optional) name of user input field, default will try to find the field
     * @param user - (optional) user name, default to binding user variable
     * @param passwordField - (optional) name of password input field, default will try to find the field
     * @param password - (optional) password, default to binding password variable
     * @param admin or adminLogin - force going to a admin url to get secure session login if enabled
     * @param adminUrl - specific admin url, otherwise defaults
     * @param formField - (optional) name of form field, defaults to the first form found
     * @param various findElement parameters for searching for the element that will indicate success when found on the result page
     * @return Map that can be used to create a task that implements the login sequence
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Map getLoginTaskMap(final Map map) {

        return [
            name: map?.name ?: 'login', description: map?.description ?: 'Login to site',
            neededBy: map?.containsKey('neededBy') ? map.neededBy : 'gintBase', // default to all tasks needing this to be done first unless specified not to
            dependsOn: map.dependsOn ?: ['initializeSelenium'],
            mustRunBefore: map?.mustRunBefore,
            mustRunAfter: map?.mustRunAfter,
            parameters: map,
            inline: { GintTask task ->
                boolean result = true
                try {
                    result = getLoginClosure().call(task?.map?.parameters)
                } catch (UnhandledAlertException uae) {
                    message 'info', 'Handling alerts on login: ' + uae.getMessage()
                    handleAlert(map)
                    if (handleAlert(map)) { // handle more alerts
                        if (handleAlert(map)) {
                            handleAlert(map)
                        }
                    }
                    // now retry
                    try {
                        result = getLoginClosure().call(task.map?.parameters)
                    } catch (UnhandledAlertException uae2) {  // handle a second alert
                        message 'info', 'More alerts found, handling 1 more before giving up'
                        if (handleAlert(map)) {  // retry if an alert was found and handled
                            result = getLoginClosure().call(task.map?.parameters)
                        } else {
                            result = false
                        }
                    }

                } catch(Exception | Error exception) {
                    //message 'debug', 'closure failed with exception: ' + exception.toString()
                    exception.printStackTrace()
                    result = false
                }
                if (!result && (map.stopOnFail != false)) { // not explicitly requested to keep going
                    gint.stopNow = true
                }
                return result
            },
            data: map?.data,
            finalClosure: map?.finalClosure ?: { GintTask task ->
                if (task.success == false) {
                    screenshot(name: task.name + '_failed')
                }
                handleAlert(map) // dismiss any alert that shows
            },
            startClosure: map?.startClosure,
            resultClosure: map?.resultClosure,
        ]
    }

    /**
     * Get a closure that runs a login scenario based on parameters
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Closure getLoginClosure() {

        return { Map inParameters ->
            Map parameters = inParameters ?: [:]
            helper.dLog('parameters', parameters)

            if (parameters.url == null) {
                def loginPath = getLoginPath(parameters)
                helper.dLog('loginPath', loginPath)

                def url = parameters.server ?: parameters.serverInfo?.url ?: getFindDefaultParameters()?.serverInfo?.url
                //helper.log('url', url)
                helper.dLog('url', url)

                if (url != null) {
                    parameters.url = url + (loginPath ?: '')
                    parameters.user = parameters.user ?: helper.getParameterValue('user')
                    parameters.password = parameters.password ?: helper.getParameterValue('password')
                    if ((parameters.adminLogin == true || parameters.admin == true) && (parameters.adminUrl == null)) {
                        parameters.adminUrl = url + (getAdminLoginPath(parameters) ?: '')
                        parameters.titleText = parameters.titleText ?: getAdminLoginDefaultResultTitleText(parameters)
                        // if we need a second choice for finding the result page
                        //parameters.text = parameters.text ?: getAdminLoginDefaultResultText(parameters)
                    } else {
                        parameters.titleText = parameters.titleText ?: getLoginDefaultResultTitleText(parameters)
                    }
                } else {
                    assert parameters.url // url parameter must be provided or server and application information
                }
            }

            assert parameters?.url // at least the url parameter must be provided or constructed
            if (!parameters.quiet) {  // TODO: should be log level based
                helper.log 'Log into ', parameters.url
            }
            gotoUrl(url: parameters.url, screenshot: parameters.screenshot ?: null)
            waitForPageLoad(getWait(parameters.wait))
            sleep(2000) // TODO: needed for testing with Atlassian, not sure what changed there
            delay()

            // look for input fields that look right
            def inputElements = findElements(tag: 'input', visible: true)
            assert inputElements instanceof List && inputElements?.size() > 1 // must be at least 2 input fields on the screen to login

            def userMap = getUserMap(parameters, inputElements)
            def formElement = getFormElement(parameters, inputElements)

            def user = parameters.user ?: helper.getParameterValueWithExtendedLookup('user')
            def password = parameters.password ?: helper.getParameterValueWithExtendedLookup('password')

            if (helper.isBlank(user) || helper.isBlank(password)) {
                message 'error', 'Both user and password parameters must be provided.'
                return false
            }

            //helper.log('format element', formElement)
            //helper.log('password', userMap.passwordElement)
            //helper.log('pw attributes', helper.getWebElementAttributes(userMap.passwordElement))
            //helper.log('password is displayed?', userMap.passwordElement.isDisplayed())

            // With Atlassian id, password is now hidden until email typed in, another delay getting to page
            // Some sites like Atlassian Cloud hide the password parameter until user is entered
            boolean isPasswordFieldDelayed = !userMap.passwordElement.isDisplayed()
            //helper.log 'isPasswordFieldDelayed', isPasswordFieldDelayed

            userMap.userElement.clear()  // just in case the browser is auto filling
            userMap.userElement.sendKeys(user)

            if (isPasswordFieldDelayed) {
                formElement.submit()  // submit Next to get the password field to display
                // wait for password field to be available
                for (int i = 0; i < 50; i++) {
                    if (userMap.passwordElement.isDisplayed()) { // this should be quick
                        break
                    }
                    sleep(200)
                }
            }
            userMap.passwordElement.clear()
            userMap.passwordElement.sendKeys(password)

            formElement.submit()  // submit works on any form element

            // Atlassian Cloud specific - TODO how can we handle this more generically
            if (isPasswordFieldDelayed) {
                for (int i = 0; i < 50; i++) {
                    if (!driver.getTitle().toLowerCase().contains("account")) {  // wait for Atlassian id screen to clear
                        break
                    }
                    sleep(200)
                }
            }
            // wait for page load if possible, although this may not work in all cases, so we also do a sleep :(
            waitForPageLoad(getWait(parameters.wait)) // tried to prevent Safari problems, usually not a problem with others
            sleep(2000) // TODO: needed for testing with Atlassian, not sure what changed there
            delay()

            // TODO: This is Atlassian specific, think about generalizing and moving to Atlassian plugin
            if (parameters.adminUrl != null) { // admin url provided in addition to regular url
                gotoUrl(url: parameters.adminUrl)
                parameters.adminLogin = true
            }
            // handle case where admin authentication is required for the installation
            if (parameters.adminLogin == true || parameters.admin == true) {
                handleAdminAccess(password: password)
            }

            //if (parameters.titleText != null) {
            //  helper.vLogWithFormat('titleText', driver.getTitle())
            //  isMatchOnTitle(parameters)
            //  assert driver.getTitle().toLowerCase().contains(parameters.titleText.toLowerCase())
            //} else {
            if (!isMatchOnTitle(parameters)) {
                //if (getByMethod(parameters) == null) {
                //message 'error', 'Title match failed or was not defined and parameters provided do not allow for other searching to determine if resulting content is expected.'
                message 'error', 'Title match failed or parameters provided do not allow for other searching to determine if resulting content is expected.'
                return false
                //}
                def element = findElement(parameters + [visible: true, screenshot: (parameters.screenshot ? 'login-result' : null)])
                if (element == null) { // give user a chance to cancel password dialog (safari) or observe error message
                    //sleep(5000)  // TODO: I don't think this is necessary so commented it out
                    element = findElement(parameters + [visible: true])
                }
                assert element // login submitted and this checks the result page contains what the user is looking for
            }
            return true
        }
    }

    /**
     * Get the login form
     * @param parameters
     * @param inputElements
     * @return form
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected WebElement getFormElement(Map parameters, inputElements) {

        // look for form field that looks right
        def formElements = findElements(tag: 'form')
        assert inputElements.size() > 0 // must be at least 1 form on the screen to login

        // look for first form element that looks like the login submit
        def formElement = null
        formElements.each { element ->
            if (formElement == null) {
                def attributes = helper.getWebElementAttributes(element)
                if (parameters.verbose) {
                    helper.log('form element attributes', attributes)
                }
                if (parameters.formField != null) {
                    if ((parameters.formField == attributes?.name) || (parameters.formField == attributes?.id)) {
                        formElement = element
                    }
                } else if ((attributes?.name?.contains('login') || attributes?.id?.contains('login'))) {
                    formElement = element
                }
            }
        }
        formElement = formElement ?: formElements[0] // take the first one

        assert formElement // could not find form field
        return formElement instanceof WebElement ? (WebElement) formElement : null
    }

    /**
     * Get the user and password fields from the form - assert the value are found
     * @param parameters
     * @param inputElements
     * @return map contain userElement and password element
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Map getUserMap(Map parameters, inputElements) {
        def map = [
            userElement: null,
            passwordElement: null,
        ]
        assert parameters != null // parameters are required
        inputElements.each { element ->
            def attributes = helper.getWebElementAttributes(element, driver)
            if (attributes.type != 'hidden') {
                if (parameters.verbose) {
                    helper.log('element attributes', attributes)
                }
                if (map.userElement == null) {
                    if (parameters.userField != null) {
                        if ((parameters.userField == attributes.name) || (parameters.userField == attributes.id)) {
                            map.userElement = element
                        }
                    } else if ((attributes.name?.contains('user') || attributes.id?.contains('user'))) {
                        map.userElement = element
                    }
                }
                if (map.passwordElement == null) {
                    if (parameters.passwordField != null) {
                        if ((parameters.passwordField == attributes.name) || (parameters.passwordField == attributes.id)) {
                            map.passwordElement = element
                        }
                    } else if ((attributes.name?.contains('password') || attributes.id?.contains('password'))) {
                        map.passwordElement = element
                    }
                }
            }
        }
        assert map.userElement // could not find a user field
        assert map.passwordElement // could not find a password field

        return map
    }

    /**
     * Does current page title corresponds to what is being looked for?
     * @param parameters - titleText, text, and exact are relevant
     * @return true if there is a match
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public boolean isMatchOnTitle(Map parameters) {
        def result = true
        def driver = parameters.driver ?: driver
        helper.dLog 'parameters', parameters
        //helper.log 'parameters', parameters

        def text

        if (parameters?.titleText || parameters?.text) { // parameters must be provided

            text = helper.getValueHandleClosure(parameters.titleText ?: parameters.text)

            //helper.log 'parameters', parameters
            //helper.log 'text', text
            //helper.log 'text class', text.class.name

            if (text instanceof java.util.regex.Pattern) {
                result = text.matcher(driver.getTitle()).matches()
            } else if (text instanceof String) {
                if (parameters.exact == true) {
                    result = driver.getTitle() == text
                } else {
                    result = driver.getTitle().toLowerCase().contains(text.toLowerCase())
                }
            }
            if (!result && (parameters.visible == true)) { // user wants to wait for title to appear
                def wait = getWait(parameters.wait)
                try {
                    def webDriverWait = getWebDriverWait(driver, wait)
                    webDriverWeb.until(ExpectedConditions.titleContains(parameters.text)) // this is ONLY case sensitive
                } catch (TimeoutException exception) {
                    result = false
                    if ((wait != null) && (wait > 0)) {
                        if (!quiet) {
                            message 'error', "Timeout with wait: ${wait} while looking for: ${parameters.text}"
                        }
                    }
                }
            }
            helper.log('Title', driver.getTitle())
        }
        if (!result || parameters.verbose) {
            helper.log('Current url', driver?.getCurrentUrl())
            helper.log('Title', driver.getTitle())
            helper.log('Title search', text)
        }
        return result
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return partial url
     */
    public getLoginPath(Map parameters = null) {
        return null
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return partial url
     */
    public getAdminLoginPath(Map parameters = null) {
        return null
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return text to find on result page
     */
    public getLoginDefaultResultTitleText(Map parameters = null) {
        return null
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return text to find on result page
     */
    public getAdminLoginDefaultResultTitleText(Map parameters = null) {
        return null
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return text to find on result page
     */
    public getLoginDefaultResultFindParameters(Map parameters = null) {
        return [:]  // override in server specific if needed
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return text to find on result page
     */
    public getAdminLoginDefaultResultFindParameters(Map parameters = null) {
        return [:]  // override if necessary
    }

    public void handleAdminAccess(Map parameters = null) {
    }

    /**
     * Initialize and save driver. Default to chrome as it is the most popular browser by far.
     * @param browser - one of chrome (default), firefox, safari, ie, edge.
     * @param hub - selenium grid hub url if desired
     * @return driver - either WebDriver or RemoteWebDriver
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    def initialize(Map<String, Object> map) {
        browser = map?.browser?.toString().toLowerCase() // set global variable

        if (map.capabilities != null && !(map.capabilities instanceof MutableCapabilities)) {
            throw new Exception('Class of capabilities parameter provided is not MutableCapabilities')
        }

        MutableCapabilities capabilities = (MutableCapabilities) map?.capabilities ?: getDefaultCapabilities(browser)

        if (helper.isBlank(map?.hub)) { // not using selenium hub
            switch (browser) {
                case 'firefox':
                    assert capabilities instanceof FirefoxOptions
                    driver = new FirefoxDriver((FirefoxOptions) capabilities)
                    break

                case 'safari':
                    assert capabilities instanceof SafariOptions
                    driver = new SafariDriver((SafariOptions) capabilities)
                    break

                case 'ie':
                    assert capabilities instanceof InternetExplorerOptions
                    driver = new InternetExplorerDriver((InternetExplorerOptions) capabilities)
                    break

                case 'edge':
                    assert capabilities instanceof EdgeOptions
                    driver = new EdgeDriver((EdgeOptions) capabilities)
                    break

                case 'chrome': // default
                default:
                    assert capabilities instanceof ChromeOptions
                    driver = new ChromeDriver((ChromeOptions) capabilities)
            }
        } else {
            message 'info', 'Using selenium hub: ' + map?.hub
            // deprecated in 4.x, warning message if used
            capabilities.setCapability("name", gint.name)  // zalenium name for video and live preview
            //capabilities.setCapability("screenResolution", "1280x720")
            driver = new RemoteWebDriver(new URL(map?.hub), capabilities)
            driver.setFileDetector(new LocalFileDetector())
        }
        driverList << driver // save for automatic cleanup at the end of the script
        return driver
    }

    public MutableCapabilities getDefaultCapabilities(browser) {
        MutableCapabilities capabilities
        switch (browser) {
            case 'firefox':
                capabilities = new FirefoxOptions()
                break

            case 'safari':
                capabilities = new SafariOptions()
                break

            case 'ie':
                capabilities = new InternetExplorerOptions() // needs Selenium 3.4
                break

            case 'edge':
                capabilities = new EdgeOptions()
                break

            case 'chrome':
            default:
                capabilities = new ChromeOptions()
        }
        return capabilities
    }

    /**
     * Cleanup - quit the driver, reset to null. Close is not needed and causes problems at least with firefox.
     * See https://stackoverflow.com/questions/15067107/difference-between-webdriver-dispose-close-and-quit
     */
    public void close(def inDriver = null) {
        def localDriver = inDriver ?: this.driver
        if (localDriver != null) {
            assert (localDriver instanceof WebDriver || localDriver instanceof RemoteWebDriver)
            localDriver.quit()
            if (localDriver == this.driver) {
                driver = null
            }
        }
    }

    /**
     * Find and get table body text as 2 dimensional list or null if not found or other problem accessing
     * - uses same parameters as findElement
     * - includeId - if true, then the row id attribute will be included
     * @return null or list of rows, each row being a list of column values
     */
    public List<List<String>> getTable(final Map parameters) {
        List list = null
        def tableElements = getTableElements(parameters + (parameters.includeId == true ? [includeRow: true] : [:]))
        if (tableElements != null) {
            list = []
            tableElements.each { row ->
                def rowResult = []
                for (int i = 0; i < row.size(); i++) {
                    if ((i == 0) && (parameters.includeId == true)) {
                        rowResult << row[i].getAttribute('id')
                    } else {
                        rowResult << (row[i].getText() ?: '')
                    }
                }
                //helper.log('rowResult', rowResult)
                list << rowResult
            }
        }

        return list

        /*
         def list = null
         def table = findElement(parameters) // Find the text input element by its name or id
         if (table != null) {
         def tbody = findElement(parameters + [base: table, tag: 'tbody'])
         assert tbody != null
         // prevent some intermittent failures when table is there but javascript may not be finished processing
         // only needed when we taking a copy of the data perhaps before it is totally complete
         sleep(parameters.sleep ?: DEFAULT_SLEEP) // milliseconds
         def rows = findElements(parameters + [base: tbody, tag: 'tr'])
         list = []
         rows.each { row ->
         def rowResult = []
         def columns = findElements(base: row, tag: 'th') + findElements(base: row, tag: 'td')
         if (columns && (parameters.includeId == true)) {
         def rowId = row.getAttribute('id')
         rowResult << (rowId ?: (columns.size > 0) && (columns[0].getTagName() == 'th') ? 'id' : '') // if headers, put id column heading
         }
         //helper.log("columns", columns)
         columns?.each { column ->
         rowResult << column.getText() ?: ''
         }
         list << rowResult
         }
         if (parameters.verbose) {
         helper.log('list', list)
         }
         }
         return list
         */
    }

    /**
     * Find and get table body text as 2 dimensional list or null if not found or other problem accessing
     * - uses same parameters as findElement
     * - includeId - if true, then the row id attribute will be included
     * @return null or list of rows, each row being a list of column values
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public List<List<WebElement>> getTableElements(final Map parameters) {
        List list = null
        def table = findElement(parameters) // Find the text input element by its name or id
        if (table != null) {
            def tbody = findElement(parameters + [base: table, tag: 'tbody'])
            assert tbody != null

            // prevent some intermittent failures when table is there but javascript may not be finished processing
            // only needed when we taking a copy of the data perhaps before it is totally complete
            sleep(parameters.sleep ?: TABLE_DEFAULT_SLEEP) // milliseconds
            def rows = findElements(parameters + [base: tbody, tag: 'tr'])
            list = []
            rows.each { row ->
                List<WebElement> columns = new ArrayList<Map<String, String>>()
                if (parameters.includeRow == true) {  // add row as the first column
                    columns << row
                }
                columns += findElements(base: row, tag: 'th') ?: []
                columns += findElements(base: row, tag: 'td') ?: []
                list << columns
            }
            if (parameters.verbose) {
                helper.log('list', list)
            }
        }
        return list
    }

    /**
     * Get list of elements with attribute map or null if not found or other problem accessing.
     * Filter list by providing a matchAttribute (default name) and regex pattern
     * @return null or list of rows, each row being a map of columns to values (see below)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    List<Map<String, String>> getElements(final Map parameters) {
        def list = []
        def pattern = parameters?.regex ? java.util.regex.Pattern.compile(parameters.regex.toString()) : null

        def elements = findElements(parameters)
        def matchAttribute = parameters.matchAttribute ?: 'name'
        elements?.each { element ->
            def attribute = element?.getAttribute(matchAttribute)
            if ((pattern == null) || ((element?.getAttribute(matchAttribute) != null) && pattern?.matcher(element?.getAttribute(matchAttribute))?.matches())) {
                list << helper.getWebElementAttributes(element, driver)
            }
        }
        if (parameters.verbose) {
            helper.log('elements', list)
        }
        return list
    }

    /**
     * Get select list text 2-dim array or null if not found or other problem accessing. If you want a map instead, use getElementsAsListOfMaps.
     * Filter list by providing a matchAttribute (default name) and regex pattern
     * @return null or list of rows, each row being a list of columns (see below)
     */
    /*
     List<List<String>> getElements(final Map parameters) {
     def list = []
     def pattern = parameters?.regex ? java.util.regex.Pattern.compile(parameters.regex) : null
     def elements = findElements(parameters)
     if (elements.size() > 0) {
     list << getElementAttributes(elements[0]).keySet()  // get the header row
     }
     def matchAttribute = parameters.matchAttribute ?: 'name'
     elements?.each { element ->
     if ((pattern == null) || pattern.matcher(element.getAttribute(matchAttribute))?.matches()) {
     list << getElementAttributes(element).values()
     }
     }
     if (parameters.verbose) {
     helper.log('list', list)
     }
     return list
     }
     public List<Map<String, String>> getElementsAsListOfMaps(final Map parameters) {
     return helper.convertRowListToMapList(getElements(parameters))
     }
     */

    /**
     * Find multiple elements - same as findElement
     */
    public def findElements(final Map parameters) {
        return findElement((parameters ?: [:]) + [multiple: true])
    }

    /**
     * Find element(s) by name or id or tag
     * parameters:
     * - base - base element - this or driver is required!!!
     * - driver - web driver is required when visible is specified and base is not a web driver
     * - name, tag, or id
     * - clickable - include logic to wait for element to be visible AND clickable
     * - visible - include logic to wait for element to become visible
     * - present - include logic to wait for element to present
     * - multiple - set to true to get multiple elements
     * - attribute - for sub setting by attribute value
     * - attributeValue - value that the element attribute must match to be selected
     * - screenshot - take a screenshot when the find fails
     * - verbose - more messages
     * - quiet - fail quietly, used when verifying an element is not available
     * @return WebElement, List<WebElement> or null
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def findElement(final Map inParameters) {

        def parameters = findDefaultParameters + (inParameters ?: [:])
        helper.dLog("find element parameters", parameters)

        def element = null
        def byMethod = getByMethod(parameters)
        boolean success = byMethod != null
        boolean quiet = (parameters.quiet == true)

        if (success) {
            def base = helper.getValueHandleClosure(parameters.base ?: parameters.driver ?: driver, parameters)
            assert base != null

            if ((parameters.visible == true) || (parameters.present == true) || (parameters.clickable == true)) { // element must be visible or present within the timeout
                def myDriver = helper.getValueHandleClosure(parameters.driver ?: driver ?: parameters.base, parameters)
                assert myDriver instanceof WebDriver  // need driver in order to handle visibility waiting
                def wait = getWait(parameters.wait)
                try {
                    def webDriverWait = getWebDriverWait(myDriver, wait)
                    try {
                        def startTime = System.currentTimeMillis()
                        waitUntil(parameters + [wait: webDriverWait, byMethod: byMethod])
                        if (parameters.verbose == true) {
                            message 'info', 'Actual wait time (ms): ' + (System.currentTimeMillis() - startTime)
                        }
                    } catch (UnhandledAlertException uae) {
                        if (parameters.acceptAlert || parameters.dismissAlert) {
                            message 'info', 'Handling an alert as requested'
                            if (handleAlert(parameters)) {  // retry if an alert was found and handled
                                waitUntil(parameters + [wait: webDriverWait, byMethod: byMethod])
                            }
                        } else {
                            message 'error', 'An unhandled alert is present. Consider using acceptAlert or dismissAlert parameters for automatic handling'
                            success = false
                        }
                    }
                } catch (TimeoutException exception) {
                    if (wait > 0) {
                        if (!quiet) {
                            message 'error', "Timeout with wait: ${wait} while looking for: ${parameters}"
                        }
                    }
                    success = false
                }
            }
            if (success) {
                try {
                    if (parameters.multiple == true) {
                        try {
                            element = base?.findElements(byMethod) // multiple elements
                        } catch (UnhandledAlertException uae) {
                            if (parameters.acceptAlert || parameters.dismissAlert) {
                                message 'info', 'Handling an alert as requested'
                                if (handleAlert(parameters)) {  // retry if an alert was found and handled
                                    element = base?.findElements(byMethod) // multiple elements
                                }
                            } else {
                                message 'error', 'An unhandle alert is present. Consider using acceptAlert or dismissAlert parameters for automatic handling'
                                success = false
                            }
                        }
                        // Subset the list by attribute setting if requested
                        if ((parameters.attributeValue != null) && (parameters.attribute != null)) {
                            def list = []as List<WebElement>
                            element.each { entry ->
                                def attributes = helper.getWebElementAttributes(entry, driver)
                                if (attributes != null) {
                                    def value = attributes[parameters.attribute]
                                    if (value != null) {
                                        if (parameters.exact == true) {
                                            if (value.equals(parameters.attributeValue)) {
                                                list.add(entry)
                                            }
                                        } else if (value?.toLowerCase().contains(parameters.attributeValue?.toLowerCase())) {
                                            list.add(entry)
                                        }
                                    }
                                }
                            }
                            element = list // now a subset of elements matching the attribute conditions
                        }
                    } else {
                        try {
                            element = base?.findElement(byMethod) // one element
                        } catch (UnhandledAlertException uae) {
                            if (parameters.acceptAlert || parameters.dismissAlert) {
                                message 'info', 'Handling an alert as requested'
                                if (handleAlert(parameters)) {  // retry if an alert was found and handled
                                    element = base?.findElement(byMethod) // one element
                                }
                            } else {
                                message 'error', 'An unhandle alert is present. Consider using acceptAlert or dismissAlert parameters for automatic handling'
                                success = false
                            }
                        }
                    }
                    if ((parameters.debug == true) && (parameters.multiple != true)) {
                        helper.log 'found element', [element: element, tag: element?.getTagName(), text: element?.getText(), isDisplayed: element.isDisplayed(), isEnabled: element.isEnabled(), isSelected: element.isSelected()]
                    }
                } catch (NoSuchElementException exception) {
                    if (!quiet) {
                        message 'error', getFindElementErrorMessage(parameters)
                    }

                } catch (StaleElementReferenceException exception) {
                    if (!quiet) {
                        message 'error', 'Stale element reference in findElement processing: ' + exception
                        gint.printStackTrace(exception)
                    }
                } catch (Exception exception) {
                    if (!quiet) {
                        message 'error', 'Unexpected findElement exception: ' + exception
                        gint.printStackTrace(exception)
                    }
                }
            }
        }
        // Refresh support to refresh screen and retry finding element
        // Primary use case is Atlassian Cloud often shows blank pages that sometime can be fixed by a refresh. We see this both for Confluence and Jira :(
        if (success && (element == null)) {
            Integer refresh = helper.getInteger(parameters?.refresh, 0)

            if (refresh > 0) {
                Map tempParameters = parameters
                tempParameters.remove('refresh')
                for (int i = 0; (i < refresh) && (element == null); i++) {
                    helper.log('Refreshing url to find element', driver.getCurrentUrl())
                    try {
                        refreshCurrentUrl()
                        element = findElement(tempParameters)
                    } catch (Exception ignore) {
                        helper.dLog 'Ignore refresh exception', ignore.toString()
                        break
                    }
                }
            }
        }
        if (!quiet && ((element == null) || parameters.verbose)) {
            helper.log('parameters', parameters) // log more in debug mode
            helper.log('ByMethod', byMethod)
        }
        if ((!quiet && !success && parameters.screenshot) || (parameters.screenshot == true)) {
            screenshot(parameters)
        }
        return element
    }

    /**
     * Refresh the current url for the default driver
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void refreshCurrentUrl() {
        driver?.navigate()?.refresh()
    }

    /**
     * Handle the waitUntil logic so we can easily retry
     * @param parameters
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected void waitUntil(final Map parameters) {
        assert parameters?.wait  // wait is required for waitUntil
        assert parameters?.byMethod  // byMethod is required for waitUntil
        try {
            if (parameters.clickable == true) {
                parameters.wait.until(ExpectedConditions.elementToBeClickable(parameters.byMethod)) // includes visible
            } else if (parameters.visible == true) {
                parameters.wait.until(ExpectedConditions.visibilityOfElementLocated(parameters.byMethod))
            } else {
                parameters.wait.until(ExpectedConditions.presenceOfElementLocated(parameters.byMethod))
            }
        } catch (org.openqa.selenium.TimeoutException exception) {
            // This is expected - let normal process deal with it
            // throw exception
        } catch (org.openqa.selenium.UnsupportedCommandException exception) {
            //exception.printStackTrace()
            message 'warn', 'org.openqa.selenium.UnsupportedCommandException doing waitUntil'
        } catch (WebDriverException exception) {
            message 'warn', 'Unexpected exception doing waitUntil: ' + exception.toString()
        }
    }

    /**
     * Handle an alert (if present) - either accept if accept=true or dismiss=null
     * @param acceptAlert - true to accept the alert dialog
     * @param dismissAlert - true to dismiss the alert dialog
     * @param verbose - for some info messages
     * @return true if an alert was found, false if no alert
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected boolean handleAlert(final Map parameters) {

        def alert = null
        try {
            alert = getDriver()?.switchTo()?.alert()
        } catch (NoAlertPresentException ignore) {
            if (parameters?.verbose) {
                message 'info', 'No alert found during handle alert processing'
                alert = null
            }
        } catch (Exception exceptiong) {
            if (parameters?.verbose == true) {
                message 'warn', 'Ignore exception: ' + exception.toString()
            }
        }
        //helper.log 'alert', alert
        if (alert != null) {
            message 'info', 'Alert found: ' + alert.getText()
            delay() // briefly show alerts if delay set
            try {
                if ((parameters?.acceptAlert == true) || (parameters?.dismissAlert == null)) { // accept overrides dismiss
                    alert.accept()
                    if (parameters?.verbose == true) {
                        message 'info', 'Alert accepted'
                    }
                } else if (parameters?.dismissAlert == true){
                    alert.dismiss()  // dismiss is the default handling for any alert interfering with operation
                    if (parameters?.verbose == true) {
                        message 'info', 'Alert dismissed'
                    }
                } else {
                    message 'warn', 'Alert not handled since dismissAlert was specified but was not true.'
                }
            } catch (NoAlertPresentException ignore) {
                // should not happen
                helper.dLog('exception', ignore)
            }
            delay()
        }
        return alert != null // indicates an alert was found and handled
    }

    /**
     * @param parameters
     * @return integer wait either from parameter or defaulted
     */
    protected int getWait(wait) {
        wait = wait ?: getFindDefaultParameters().wait // make sure we use any modified wait explicitly set
        return wait != null && wait instanceof Integer ? wait.intValue() : defaultWait
    }

    /**
     * Get the by method to use based on the parameters provided. Valid lookup in priority order are:
     * - tag
     * - linkText
     * - linkHref (via xpath)
     * - text (via xpath) - contains match by default unless exact = true
     * - xpath (general)
     * - id
     * - name
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def getByMethod(final Map parameters) {
        if (parameters?.tag != null) {
            return By.tagName(helper.getValueHandleClosure(parameters.tag))
        }
        if (parameters?.linkText != null) {
            return By.linkText(helper.getValueHandleClosure(parameters.linkText))
        }
        if (parameters?.linkHref != null) { // looking for a link: <a href ...
            def value = helper.quoteString(helper.getValueHandleClosure(parameters.linkHref))
            return By.xpath("//a[@href[contains(., ${value})]]")
        }
        if (parameters?.text != null) {
            def value = helper.quoteString(helper.getValueHandleClosure(parameters.text))
            return By.xpath("//*[text()${parameters.exact == true ? '=' + value : '[contains(., ' + value + ')]'} ]")
        }
        if (parameters?.cssSelector != null) {  // see http://saucelabs.com/resources/selenium/css-selectors
            return By.cssSelector(helper.getValueHandleClosure(parameters.cssSelector))
        }
        if (parameters?.xpath != null) {
            return By.xpath(helper.getValueHandleClosure(parameters.xpath))
        }
        if (parameters?.className != null) {
            return By.className(helper.getValueHandleClosure(parameters.className))
        }
        if (parameters?.id != null) {
            return By.id(helper.getValueHandleClosure(helper.getValueHandleClosure(parameters.id))) // extra recurse on closure evaluation
        }
        if (parameters?.name != null) {
            return By.name(helper.getValueHandleClosure(parameters.name))
        }
        if (parameters.verbose) {
            message 'warning', 'No by method could be determined from the parameters provided: ' + parameters + '. Use one of: tag, linkText, linkHref, text, xpath, className, id, name.'
        }
        return null
    }

    /**
     * Same as find. Assert element is found. Click the element. Delay according to configuration.
     * @param parameters - contains find parameters like normal
     * @param childText - a secondary item to find and click (think of a menu item)
     * @return element found and clicked and optionally a child element as well
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def findAndClickElement(final Map parameters) {
        def element = findElement(parameters)
        if (element == null) {
            if (!parameters.quiet) {
                helper.log('findAndClickElement parameters', parameters)
            }
            assert element // findAndClickElement
        }
        if (parameters.verbose) {
            message 'info', 'Click on ' + element
        }
        if (parameters.childText == null) {
            elementClick(element)
        } else {
            findAndClickElement(base: element, text: parameters.childText, visible: true, childText: parameters.childChildText, exact: parameters.exact, verbose: parameters.verbose)
        }
        delay() // put in a pause for watching if delay is configured
        return element // the main element
    }

    /**
     * Element click with handling of element not clickable situations (behind some message box or similar) - https://stackoverflow.com/questions/11908249/debugging-element-is-not-clickable-at-point-error
     * @param element
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def elementClick(element) {
        try {
            element.click()
        } catch (WebDriverException exception) {
            if (exception.getMessage().contains("is not clickable")) { // reposition and try again, hopefully avoiding some overlay that was in the way
                def actions = new Actions(driver)
                actions.moveToElement(element).click().perform()
                //screenshot(name: '_after_click')
            } else {
                throw exception
            }
        }
    }

    /**
     * Same as find. Assert element is found. Click the element. Then send text to the element
     * @param parameters
     * @return element found and clicked
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def findAndSendElement(final Map parameters) {
        def element = findElement(parameters)
        if (element == null) {
            if (!parameters.quiet) {
                helper.log('findAndSendElement parameters', parameters)
            }
            assert element // findAndClickElement
        }
        elementClick(element)
        if (parameters.clear == true) {
            element.clear()
        }
        element.sendKeys(parameters.send ?: '')
        delay() // put in a pause for watching if delay is configured
        if (parameters.submit == true) {
            element.submit() // submit can be done on any form element
        }
        return element
    }

    /**
     * Same parameters as find for looking for the main element.
     * Click on main element and then find on subElements by subText or subId.
     * Assert element is found.
     * @param parameters
     * @return subElement
     */
    public def findSubElement(final Map parameters) {
        def element = findElement(parameters)
        assert element // find main element in findSubElement function
        elementClick(element)
        delay() // put in a pause for watching if delay is configured
        def subElement = findElement(base: element, text: parameters.subText, id: parameters.subId, visible: true)
        assert subElement // find subelement in findSubElement function
        return subElement
    }

    /**
     * Get error message when element not found
     * @param parameters - similar to findElement
     * @return message
     */
    public def getFindElementErrorMessage(Map parameters) {
        Map map = getByMethodNameAndValue(parameters)
        return "Element not found by ${map.name} with value ${helper.quoteString(map.value)}. Base: ${parameters?.base ?: parameters?.driver ?: driver}."
    }

    /**
     * Get the name and value of the by method used to help with reporting errors when elements are not found
     * @param parameters - similar to findElement
     * @return map of name and value
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, String> getByMethodNameAndValue(final Map parameters) {
        if (parameters?.tag != null) {
            return [name: 'tag', value: helper.getValueHandleClosure(parameters.tag)]
        }
        if (parameters?.linkText != null) {
            return [name: 'linkText', value: helper.getValueHandleClosure(parameters.linkText)]
        }
        if (parameters?.linkHref != null) { // looking for a link: <a href ...
            return [name: 'linkHref(xpath)', value: helper.getValueHandleClosure(parameters.linkHref)]
        }
        if (parameters?.text != null) {
            return [name: 'text(xpath)', value: helper.getValueHandleClosure(parameters.text)]
        }
        if (parameters?.cssSelector != null) {
            return [name: 'cssSelector', value: helper.getValueHandleClosure(parameters.cssSelector)]
        }
        if (parameters?.xpath != null) {
            return [name: 'xpath', value: helper.getValueHandleClosure(parameters.xpath)]
        }
        if (parameters?.className != null) {
            return [name: 'className', value: helper.getValueHandleClosure(parameters.className)]
        }
        if (parameters?.id != null) {
            return [name: 'id', value: helper.getValueHandleClosure(parameters.id)]
        }
        if (parameters?.name != null) {
            return [name: 'name', value: helper.getValueHandleClosure(parameters.name)]
        }
        return [name: 'NO_BY_METHOD']
    }

    /**
     * Go to the URL specified by parameters and optionally take a screenshot
     * @param parameters including driver or base, url, and optionally screenshot
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void gotoUrl(final Map parameters) {

        def myDriver = helper.getValueHandleClosure(parameters?.driver ?: driver ?: parameters?.base)
        assert myDriver // driver must not be null on gotoUrl
        assert !helper.isBlank(parameters?.url) // url must not be blank or null on gotoUrl
        try {
            myDriver.get(parameters.url)
        } catch (Exception exception) {
            message 'error', 'Error connecting to url: ' + parameters.url + ". Exception was: " + exception.toString()
            assert false // exception
        }
        if (parameters.screenshot) {
            screenshot(parameters)
        }
        if (handleAlert(parameters)) {  // alert found
            handleAlert(parameters)  // handle 1 or 2 alerts
            if (parameters.screenshot) {
                screenshot(parameters) // take a new screen shot without alert
            }
        }
        return // nothing
    }

    //    /**
    //     * Goto a JIRA issue - move to seleniumHelperForJira ???
    //     * @param parameters including driver or base, issue key, and optionally screenshot
    //     */
    //    public void gotoIssue(final Map parameters) {
    //        assert parameters?.issue
    //        def issue = helper.getValueHandleClosure(parameters.issue)
    //        def url = parameters.baseUrl ?: gint.getServerInfo().url  // base URL
    //        url += "/browse/${issue}"
    //
    //        gotoUrl(driver: parameters.driver ?: driver, url: url, screenshot: parameters.screenshot, name: parameters?.name ?: issue)
    //    }


    /**
     * Wait for page load. DO NOT use unless necessary.
     * Normally a findElement wait with visible: true is the preferred waiting technique.
     * @return true if page loaded, false if timeout or other error occurred
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public boolean waitForPageLoad(wait = defaultWait) {
        def expectation = new ExpectedCondition<Boolean>() {
                            public Boolean apply(WebDriver driver) {
                                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete")
                            }
                        }
        def webDriverWait = getWebDriverWait(driver, getWait(wait))
        try {
            if (isSafari()) { // even with the wait below, safari has issues
                sleep(1000) // this is probably more than needed
            }
            helper.vLogTimestamp('page load started  ')
            webDriverWait.until(expectation)
            helper.vLogTimestamp('page load finished ')
            return true
        } catch (Throwable error) {
            message 'error', error.getMessage()
            message 'error', 'Error waiting for page load with wait: ' + getWait(wait)
            return false
        }
    }

    /**
     * Take a screen shot and copy to the location identified by the screenshot parameter or name parameter, or default name of screenshot.
     * png will be appended to a name that does not have an extension
     * Parameters
     * - driver optional
     * - name - used to generate file name
     * Example: finalClosure: { testcase -> screenshot(name: testcase.name)},
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public def screenshot(final Map parameters = null) {
        if (parameters?.screenshot != false) {
            try {
                def myDriver = helper.getValueHandleClosure(parameters?.driver ?: driver ?: parameters?.base)
                assert myDriver // driver is null for screenshot
                def name = parameters?.screenshot == true ? parameters?.name : (parameters?.screenshot ?: parameters?.name)
                name = name ?: 'screenshot'  // default name
                def postfix = helper.getExtension(name) == '' ? '.png' : '' //
                def toFile = gint?.getOutputFile(name, postfix) ?: name + postfix
                toFile = helper.generateNextFileName(toFile)

                Path screenshotPath = ((TakesScreenshot) myDriver).getScreenshotAs(OutputType.FILE).toPath()
                Path toFilePath = FileSystems.getDefault().getPath(toFile)
                Files.copy(screenshotPath, toFilePath)

                message 'info', "Screenshot: ${toFilePath.toAbsolutePath()}"
            } catch (UnhandledAlertException uae) {
                message 'warning', "Unable to take screenshot due to: ${uae.getMessage()}."
            }
        }
    }

    /**
     * Normally will get a list of menu item text, but really it will work on any element and get each line of text.
     * @param element
     * @return list of text elements based on the text attribute splitting on \n
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public List getMenuList(Map parameters) {
        assert parameters // parameters must be provided
        def element = parameters.element
        if (element == null) {  // no element, so we have to search
            element = findAndClickElement(parameters + [visible: true])
        }
        assert element // we need an element defined either directly or via search
        // TODO: does not handle names with blanks in them !
        return helper.getWebElementAttributes(element, driver)?.text?.split() // expect a list of select elements
    }

    /**
     * Scroll top, bottom, up, down, left, right. By pixels. Does not work on some pages (Confluence pages for example). Respects screenshot parameter.
     * @param parameters
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void scroll(Map parameters) {
        assert parameters // parameters must be provided

        if (handleAlert(parameters)) {
            handleAlert(parameters)    // handle 1 or 2 alerts
        }

        def horizontal = parameters.right ?: (parameters.left ? 0 - parameters.left : 0)
        def vertical = parameters.bottom ? 99999 : parameters.top ? -99999 : parameters.down ?: parameters.down ? 0 - parameters.up : 0
        def script = "javascript:window.scrollBy(${horizontal}, ${vertical})"

        ((JavascriptExecutor) driver).executeScript(script)
        if (parameters.screenshot) {
            screenshot(parameters)
        }
    }

    public WebElement getParent(WebElement element) {
        return element.findElement(By.xpath('..'))
    }

    public List<WebElement> getAllChildElements(WebElement element) {
        return element.findElements(By.xpath('.//*'))
    }

    /**
     * Switch to frame (if it exists) and synchronously run code with proper handling of switchback. This provides a way to run arbitrary code for server or connect.
     * @param task - if connect, must contain a previously set frame key in the parameters
     * @param closure - closure with task parameter
     * @return true if successful
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public boolean switchAndRunClosure(final GintTask task, final Closure closure, final String closureResultKey = null, final String closureExceptionKey = null) {
        try {
            if (task.map?.frame != null) {
                getDriver()?.switchTo().frame(task.map.frame)
                delay() // make sure we pause after the iframe content is rendered if the delay parameter is set
                return gint.closureHelper.runSynchronizedClosure(closure, task, closureResultKey, closureExceptionKey)
            } else {
                return gint.closureHelper.runClosure(closure, task, closureResultKey, closureExceptionKey)
            }
        } finally {
            if (task.map?.frame != null) {
                //message 'debug', 'switch back to regular content'
                getDriver().switchTo().defaultContent()
            }
        }
    }

    /**
     * Close any open drivers
     */
    public void closeDrivers() {
        driverList.each { entry ->
            if (entry instanceof WebDriver || entry instanceof RemoteWebDriver) {
                try {
                    entry.quit()
                    helper.vLog 'Driver closed', entry
                } catch (Exception ignore) {
                }
                entry = null
            }
        }
    }

    // Helper functions - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - especially to help avoid Class loader conflicts with test scripts

    public WebDriverWait getWebDriverWait(Long wait) {
        return getWebDriverWait(getDriver(), wait)
    }

     /**
     * Determine at runtime how to get a web driver wait due to differences between 3.14 and 4.x
     * https://www.selenium.dev/documentation/webdriver/getting_started/upgrade_to_selenium_4/
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public WebDriverWait getWebDriverWait(driver, Long wait) {
        def webDriverWait
        try {
            webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(getWait(wait)))  // 4.x
        } catch (GroovyRuntimeException e) {
            webDriverWait = new WebDriverWait(driver, new Long(getWait(wait)))            // 3.14
        }
        return webDriverWait
    }

    /**
     * Get javascript executor for current driver
     * @return executor
     */
    public JavascriptExecutor getJavascriptExecutor() {
        return (JavascriptExecutor) getDriver()
    }

    /**
     * Get javascript executor for any driver
     * @return executor
     */
    public JavascriptExecutor getJavascriptExecutor(WebDriver driver) {
        return (JavascriptExecutor) driver
    }

    /**
     * Get select element or null for if not a select element
     * @return select web element or null
     */
    public Select getSelect(WebElement element) {
        return element == null ? null : element.tagName.equals('select') ? new Select(element) : null
    }

    /**
     * Get a boolean ExpectedCondition
     * Example usage:
     *   gint.seleniumHelper.getWebDriverWait(10).until(
     *       getBooleanExpectedCondition(string, { driver, s ->
     *          driver.getTitle().toLowerCase().startsWith(s)
     *       })
     *   )
     */
    public ExpectedCondition getBooleanExpectedCondition(object, Closure logic) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return logic(driver, object)
            }
        }
    }
}
